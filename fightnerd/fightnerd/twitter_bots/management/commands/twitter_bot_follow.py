from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.core.management.base import BaseCommand
from fightnerd.twitter_bots.models import Bot
from fightnerd.twitter_bots.models import Follow
from fightnerd.twitter_bots.models import User
from django.core.mail import mail_admins
from django.utils import timezone
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('screen_name')
        parser.add_argument('--clear', action='store_true', dest='do_clear', default=False)
        parser.add_argument('--notest', action='store_false', dest='is_test_mode', default=True)

    def handle(self, *args, **options):
        start_time = timezone.now()
        bot = Bot.objects.get(screen_name=options.get('screen_name'))
        if options.get('do_clear'):
            Follow.objects.all().delete()
            User.objects.all().delete()

        follows = list()
        num_tries = 0
        for i in range(1):
            num_tries = i + 1
            try:
                follows = bot.auto_follow(is_test_mode=options.get('is_test_mode'))
                break
            except:
                logger.exception('')

        end_time = timezone.now()
        duration = end_time - start_time

        if not bot.are_email_notifications_enabled:
            return

        # Send log
        message = """
    Bot: %s
    Number of follows: %d
    Number of tries: %d

    Start time: %s
    End time: %s
    Duration: %.2f seconds

    API Rate Limit: %d / %d
    API Reset Time: %d seconds
    """ % (bot.screen_name, len(follows), num_tries, start_time.isoformat(), end_time.isoformat(), duration.seconds, bot.get_hourly_limit('friends', 'list') - bot.get_remaining_hits('friends', 'list'), bot.get_hourly_limit('friends', 'list'), (bot.get_api_reset_time('friends', 'list') - timezone.now()).seconds)
        if not options.get('is_test_mode'):
            mail_admins(subject='Twitter follow task completed by %s' % bot.screen_name, message=message)
        logger.info(message)

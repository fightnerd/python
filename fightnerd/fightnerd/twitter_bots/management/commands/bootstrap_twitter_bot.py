from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from fightnerd.twitter_bots.models import Bot


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('screen_name')

    def handle(self, *args, **options):
        bot = Bot.objects.get(
            screen_name=options.get('screen_name'),
        )
        bot.bootstrap_follows()

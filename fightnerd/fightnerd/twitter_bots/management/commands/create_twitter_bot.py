from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from fightnerd.twitter_bots.models import Bot
from fightnerd.twitter_bots.models import SpeedType
from fabric.operations import prompt
import webbrowser
from requests_oauthlib import OAuth1Session

REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token'
AUTHORIZATION_URL = 'https://api.twitter.com/oauth/authorize'
SIGNIN_URL = 'https://api.twitter.com/oauth/authenticate'


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        pass

    @staticmethod
    def get_access_token(consumer_key, consumer_secret):
        oauth_client = OAuth1Session(consumer_key, client_secret=consumer_secret)

        print('Requesting temp token from Twitter')

        try:
            resp = oauth_client.fetch_request_token(REQUEST_TOKEN_URL)
        except ValueError, e:
            print('Invalid respond from Twitter requesting temp token: %s' % e)
            return
        url = oauth_client.authorization_url(AUTHORIZATION_URL)

        print('')
        print('I will try to start a browser to visit the following Twitter page')
        print('if a browser will not start, copy the URL to your browser')
        print('and retrieve the pincode to be used')
        print('in the next step to obtaining an Authentication Token:')
        print('')
        print(url)
        print('')

        webbrowser.open(url)
        pincode = raw_input('Pincode? ')

        print('')
        print('Generating and signing request for an access token')
        print('')

        oauth_client = OAuth1Session(consumer_key, client_secret=consumer_secret,
                                     resource_owner_key=resp.get('oauth_token'),
                                     resource_owner_secret=resp.get('oauth_token_secret'),
                                     verifier=pincode
        )
        try:
            resp = oauth_client.fetch_access_token(ACCESS_TOKEN_URL)
        except ValueError, e:
            print('Invalid respond from Twitter requesting access token: %s' % e)
            return

        print('Your Twitter Access Token key: %s' % resp.get('oauth_token'))
        print('          Access Token secret: %s' % resp.get('oauth_token_secret'))
        print('')
        return resp.get('oauth_token'), resp.get('oauth_token_secret')

    def handle(self, *args, **options):
        screen_name = prompt('Screen Name: ')
        password = prompt('Password: ')
        consumer_key = prompt('Consumer Key: ')
        consumer_secret = prompt('Consumer Secret: ')

        # Get access token
        access_token, access_token_secret = self.get_access_token(consumer_key, consumer_secret)

        Bot.objects.filter(screen_name=screen_name).delete()
        bot = Bot.objects.create(
            screen_name=screen_name,
            password=password,
            consumer_key=consumer_key,
            consumer_secret=consumer_secret,
            access_token=access_token,
            access_token_secret=access_token_secret,
            speed=SpeedType.FAST,
        )
        bot.bootstrap_follows()

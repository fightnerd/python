from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib import admin
from fightnerd.twitter_bots.models import Bot
from fightnerd.twitter_bots.models import FollowSourceType
from django.core.cache import caches

cache = caches['twitter_bots']


@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    list_display = (
        'screen_name',
        'speed',
        # 'num_followers',
        # 'num_followings',
        'is_follow_enabled',
        'is_reply_enabled'
    )
    # readonly_fields = (
    #     'num_followers',
    #     'num_followings',
    # )
    fieldsets = (
        ('Login Credentials', {
            'fields': (
                'screen_name',
                'password',
            )
        }),
        ('API Credentials', {
            'fields': (
                'consumer_key',
                'consumer_secret',
                'access_token',
                'access_token_secret'
            )
        }),
        ('Configuration', {
            'fields': (
                'is_enabled',
                'are_email_notifications_enabled',
                'speed',
            )
        }),
        ('Reply', {
            'fields': (
                'is_reply_enabled',
                'search_terms',
                'replies',
                'reply_timeout'
            )
        }),
        ('Follow', {
            'fields': (
                'is_follow_enabled',
                'screen_names',
                'follow_source',
                'unfollow_timeout'
            )
        }),
        ('Geocode', {
            'fields': (
                'latitude',
                'longitude',
                'radius',
            )
        }),
    )

    def num_followers(self, obj):
        key = '%s:%s' % ('NumFollowers', obj.screen_name)

        # Try the cache
        num_followers = cache.get(key, None)
        if not num_followers:
            num_followers = sum(1 for _ in obj.generate_follows([obj.screen_name], FollowSourceType.FOLLOWERS))
            cache.set(key, num_followers, timeout=300)

        return num_followers

    def num_followings(self, obj):
        key = '%s:%s' % ('NumFollowings', obj.screen_name)

        # Try the cache
        num_followings = cache.get(key, None)
        if not num_followings:
            num_followings = sum(1 for _ in obj.generate_follows([obj.screen_name], FollowSourceType.FRIENDS))
            cache.set(key, num_followings, timeout=300)

        return num_followings


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import hashlib
import random
import time
from datetime import datetime
from datetime import timedelta

import pytz
import re
import simplejson
from celery.utils.log import get_task_logger
from django.contrib.postgres.fields import ArrayField
from django.core.cache import caches
from django.db import models
from django.utils import timezone
from django_enumfield import enum
from fightnerd.twitter_bots.utils import Api
from twitter.error import TwitterError
from twitter.user import User as TwitterUser

cache = caches['twitter_bots']
logger = get_task_logger(__name__)


class FollowSourceType(enum.Enum):
    FOLLOWERS = 0
    FRIENDS = 1

    labels = {
        FOLLOWERS: 'Followers of usernames',
        FRIENDS: 'Friends of usernames',
    }


class SpeedType(enum.Enum):
    SLOW = 0
    NORMAL = 1
    FAST = 2

    labels = {
        SLOW: 'Slow',
        NORMAL: 'Normal',
        FAST: 'Fast',
    }

    # Per hour
    reply = {
        SLOW: 14,
        NORMAL: 20,
        FAST: 26,
    }

    # Per hour
    follow = {
        SLOW: 14,
        NORMAL: 20,
        FAST: 26,
    }
    unfollow = {
        SLOW: 10,
        NORMAL: 15,
        FAST: 20,
    }


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(BaseModel):
    screen_name = models.CharField(max_length=100)
    twitter_id = models.CharField(max_length=100)

    def __unicode__(self):
        return self.screen_name


class Follow(BaseModel):
    bot = models.ForeignKey('Bot')
    user = models.ForeignKey(User)
    is_unfollow = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s %s %s' % (self.bot, 'unfollowed' if self.is_unfollow else 'followed', self.user)


class Reply(BaseModel):
    status_id = models.CharField(max_length=100)
    in_reply_to_status_id = models.CharField(max_length=100)
    user = models.ForeignKey(User)
    bot = models.ForeignKey('Bot')
    status = models.TextField()


class Bot(BaseModel):
    screen_name = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    consumer_key = models.CharField(max_length=100)
    consumer_secret = models.CharField(max_length=100)
    access_token = models.CharField(max_length=100)
    access_token_secret = models.CharField(max_length=100)
    speed = enum.EnumField(SpeedType, blank=False, default=SpeedType.SLOW, verbose_name='activity speed')

    is_enabled = models.BooleanField(default=True)
    are_email_notifications_enabled = models.BooleanField(default=True)

    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    radius = models.FloatField(blank=True, null=True, verbose_name='Radius (mi)')  # In miles

    search_terms = ArrayField(models.CharField(max_length=100), default=list(), blank=True)
    replies = ArrayField(models.CharField(max_length=100), default=list(), blank=True)
    reply_timeout = models.IntegerField(default=172800, verbose_name='Reply timeout (s)')
    is_reply_enabled = models.BooleanField(default=False)

    screen_names = ArrayField(models.CharField(max_length=100), default=list(), blank=True)
    follow_source = enum.EnumField(FollowSourceType, blank=False, default=FollowSourceType.FRIENDS, verbose_name='follow source')
    unfollow_timeout = models.IntegerField(default=129600, verbose_name='Unfollow timeout (s)')
    is_follow_enabled = models.BooleanField(default=False)

    _api = None

    def __unicode__(self):
        return self.screen_name

    def clean(self):
        def clean_terms(terms):
            cleaned_terms = list()
            for term in terms:
                for t in re.split(r'[,]', term):
                    if t:
                        cleaned_terms.append(t.strip())
            return cleaned_terms

        # Clean terms
        self.search_terms = clean_terms(list(self.search_terms))
        self.replies = clean_terms(list(self.replies))
        self.screen_names = clean_terms(list(self.screen_names))

    @property
    def has_geocode(self):
        return self.latitude is not None and self.longitude is not None and self.radius is not None

    @property
    def hourly_limit(self):
        return self.api.GetRateLimitStatus(resource_families='search').get('resources').get('search').get('/search/tweets').get('limit')

    @property
    def remaining_hits(self):
        return self.api.GetRateLimitStatus(resource_families='search').get('resources').get('search').get('/search/tweets').get('remaining')

    def get_hourly_limit(self, resource_family, action):
        return self.api.GetRateLimitStatus(resource_families=resource_family).get('resources').get(resource_family).get('/%s/%s' % (resource_family, action)).get('limit')

    def get_remaining_hits(self, resource_family, action):
        return self.api.GetRateLimitStatus(resource_families=resource_family).get('resources').get(resource_family).get('/%s/%s' % (resource_family, action)).get('remaining')

    def get_api_reset_time(self, resource_family, action):
        reset_time = self.api.GetRateLimitStatus(resource_families=resource_family).get('resources').get(resource_family).get('/%s/%s' % (resource_family, action)).get('reset')
        timezone = pytz.timezone('UTC')
        return timezone.localize(datetime.fromtimestamp(reset_time))

    @property
    def api(self):
        if not self._api:
            self._api = Api(
                consumer_key=self.consumer_key,
                consumer_secret=self.consumer_secret,
                access_token_key=self.access_token,
                access_token_secret=self.access_token_secret
            )
        return self._api

    def auto_unfollow(self, is_test_mode):
        # Get list of followings (people I'm following)
        followings = list()
        for twitter_user in self.generate_follows([self.screen_name], FollowSourceType.FRIENDS):
            if twitter_user.screen_name == self.screen_name:
                continue
            user, is_created = User.objects.get_or_create(screen_name=twitter_user.screen_name, twitter_id=twitter_user.id)
            following, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            if is_created:
                logger.warn('Unlogged following found (%s)' % user.screen_name)

            # Determine if we should unfollow based on timeout
            unfollow_timeout = timedelta(seconds=self.unfollow_timeout)
            if timezone.now() - unfollow_timeout < following.date_created:
                logger.info('Skipping %s due to timeout' % user.screen_name)
                continue
            followings.append(following)

        # Get list of followers (people who follow me)
        followers = list()
        for twitter_user in self.generate_follows([self.screen_name], FollowSourceType.FOLLOWERS):
            followers.append(twitter_user)

        # Let's unfollow people who don't follow back...jerks...
        unfollows = list()
        for following in followings:
            user = following.user
            if [f for f in followers if f.screen_name == user.screen_name]:
                logger.info('%s is already following %s' % (user.screen_name, self.screen_name))
                continue

            if not is_test_mode:
                self.api.DestroyFriendship(screen_name=user.screen_name)
            unfollow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=True)
            unfollows.append(unfollow)
            logger.info('Unfollowing %s' % user.screen_name)

            # Limit yourself
            if len(unfollows) >= SpeedType.unfollow.get(self.speed):
                break

            # Sleep
            if not is_test_mode:
                sleep_duration = random.randrange(10, 20)
                logger.info('Sleeping for %d seconds to look more human' % sleep_duration)
                time.sleep(sleep_duration)
        return unfollows

    def auto_follow(self, is_test_mode):
        # Find some potential follower/ings
        follows = list()
        for twitter_user in self.generate_follows(self.screen_names, self.follow_source):
            if not self.is_twitter_user_valid(twitter_user):
                logger.info('Ignoring %s - Not enough posts' % twitter_user.screen_name)
                continue

            user, is_created = User.objects.get_or_create(screen_name=twitter_user.screen_name, twitter_id=twitter_user.id)
            if Follow.objects.filter(user=user).exists():
                logger.info('Ignoring %s - Already being followed' % user)
                continue

            # We aren't following this person!
            if not is_test_mode:
                try:
                    self.api.CreateFriendship(screen_name=user.screen_name)
                except TwitterError:
                    logger.exception('')
                    continue
            follow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            follows.append(follow)
            logger.info(follow)

            # Limit yourself
            if len(follows) >= SpeedType.follow.get(self.speed):
                break

            if not is_test_mode:
                sleep_duration = random.randrange(10, 20)
                logger.info('Sleeping for %d seconds to look more human' % sleep_duration)
                time.sleep(sleep_duration)
        return follows

    def bootstrap_follows(self):
        # Get list of followers (people I'm following)
        follows = list()
        for twitter_user in self.generate_follows([self.screen_name], FollowSourceType.FRIENDS):
            if twitter_user.screen_name == self.screen_name:
                continue
            user, is_created = User.objects.get_or_create(screen_name=twitter_user.screen_name, twitter_id=twitter_user.id)
            follow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            follows.append(follow)
            logger.info(follow)
        return follows

    def generate_follows(self, usernames, follow_source, timeout=timedelta(seconds=600)):
        api = self.api
        for username in usernames:
            next_cursor = -1
            while next_cursor != 0:
                cache_key = '%s:%s:%d' % ('GetFollowersPaged' if follow_source == FollowSourceType.FOLLOWERS else 'GetFriendsPaged', username, next_cursor)

                # Try the cache
                users = cache.get(cache_key, None)
                if users:
                    logger.info('HIT!!!!')
                    data = simplejson.loads(users)
                    users = [TwitterUser.NewFromJsonDict(x) for x in data['users']]
                    next_cursor = data['next_cursor']
                else:
                    logger.info('MISS!!!!')
                    # Make the API Request
                    kwargs = {
                        'screen_name': username,
                        'cursor': next_cursor,
                        'include_user_entities': True
                    }
                    if follow_source == FollowSourceType.FOLLOWERS:
                        next_cursor, previous_cursor, data = api.GetFollowersPaged(**kwargs)
                    else:
                        next_cursor, previous_cursor, data = api.GetFriendsPaged(**kwargs)
                    users = [TwitterUser.NewFromJsonDict(x) for x in data['users']]

                    # Cache it
                    cache.set(cache_key, simplejson.dumps(data), timeout=timeout.total_seconds())

                # Yield users
                for user in users:
                    yield user
        return

    def auto_reply(self, is_test_mode):
        replies = list()
        api = self.api

        for search_term in self.search_terms:
            # Get kwargs
            kwargs = {
                'term': search_term,
                'count': 100
            }
            if self.has_geocode:
                kwargs.update({'geocode': (self.latitude, self.longitude, '%fmi' % self.radius)})

            # Search for tweets
            statuses = api.GetSearch(**kwargs)
            sorted(statuses, key=lambda s: s.created_at_in_seconds)
            for status in statuses:
                reply = '@%s %s' % (status.user.screen_name, random.choice(self.replies))
                user, is_created = User.objects.get_or_create(screen_name=status.user.screen_name, twitter_id=status.user.id)

                # See if we have replied to this user within the timeout
                cutoff_date = timezone.now() - timedelta(seconds=self.reply_timeout)

                if Reply.objects.filter(user=user, bot=self, date_created__gte=cutoff_date).exists():
                    logger.info('Skipping %s due to reply timeout' % user.screen_name)
                    continue

                # Post a new status!
                reply_status = None
                if not is_test_mode:
                    try:
                        reply_status = api.PostUpdate(reply, in_reply_to_status_id=status.id)
                        logger.info('REPLY: %s' % reply)
                    except TwitterError:
                        logger.exception('Failed to post update')
                        continue
                reply = Reply.objects.create(
                    status_id=reply_status.id if reply_status else random.randint(1, 9000),
                    in_reply_to_status_id=status.id,
                    user=user,
                    bot=self,
                    status=reply
                )
                replies.append(reply)

                if len(replies) >= SpeedType.reply.get(self.speed):
                    return replies

                # Try to look more human
                if not is_test_mode:
                    sleep_time = random.randint(20, 30)
                    logger.info('Sleeping for %d seconds to look more human...' % sleep_time)
                    time.sleep(sleep_time)

        return replies

    def is_twitter_user_valid(self, twitter_user):
        return twitter_user.statuses_count > 200

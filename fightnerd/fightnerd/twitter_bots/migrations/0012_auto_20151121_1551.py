# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0011_bot_username'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='password',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='bot',
            name='username',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('username', models.CharField(unique=True, max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('speed', models.IntegerField(default=0, verbose_name=b'activity speed')),
                ('is_enabled', models.BooleanField(default=True)),
                ('are_email_notifications_enabled', models.BooleanField(default=True)),
                ('search_terms', django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(max_length=100), size=None)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('username', models.CharField(max_length=100)),
                ('user_id', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

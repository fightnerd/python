# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0013_auto_20151123_2218'),
    ]

    operations = [
        migrations.CreateModel(
            name='Follow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('is_unfollow', models.BooleanField(default=False)),
                ('bot', models.ForeignKey(to='twitter_bots.Bot')),
                ('user', models.ForeignKey(to='twitter_bots.User')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

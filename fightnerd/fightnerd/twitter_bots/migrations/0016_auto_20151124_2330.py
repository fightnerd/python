# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0015_auto_20151124_2328'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bot',
            old_name='username',
            new_name='screen_name',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0003_auto_20151120_0032'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='latitude',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='bot',
            name='longitude',
            field=models.FloatField(null=True),
        ),
        migrations.AddField(
            model_name='bot',
            name='radius',
            field=models.FloatField(null=True, verbose_name=b'Radius (mi)'),
        ),
    ]

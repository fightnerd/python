# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0012_auto_20151121_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='latitude',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bot',
            name='longitude',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bot',
            name='radius',
            field=models.FloatField(null=True, verbose_name=b'Radius (mi)', blank=True),
        ),
    ]

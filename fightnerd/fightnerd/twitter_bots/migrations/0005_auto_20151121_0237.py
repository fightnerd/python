# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0004_auto_20151121_0214'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reply',
            old_name='content',
            new_name='status',
        ),
        migrations.RenameField(
            model_name='reply',
            old_name='tweet_id',
            new_name='status_id',
        ),
        migrations.AddField(
            model_name='reply',
            name='in_reply_to_status_id',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]

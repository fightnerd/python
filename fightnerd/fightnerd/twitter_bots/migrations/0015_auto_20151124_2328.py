# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0014_follow'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='follow_source',
            field=models.IntegerField(default=1, verbose_name=b'follow source'),
        ),
        migrations.AddField(
            model_name='bot',
            name='screen_names',
            field=django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(max_length=100), size=None),
        ),
        migrations.AddField(
            model_name='bot',
            name='unfollow_timeout',
            field=models.IntegerField(default=129600, verbose_name=b'Unfollow timeout (s)'),
        ),
    ]

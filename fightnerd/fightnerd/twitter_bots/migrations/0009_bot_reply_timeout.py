# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0008_auto_20151121_0252'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='reply_timeout',
            field=models.IntegerField(default=0, verbose_name=b'Reply timeout (s)'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0002_reply'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='access_token',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bot',
            name='access_token_secret',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bot',
            name='consumer_key',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bot',
            name='consumer_secret',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
    ]

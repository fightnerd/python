from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from twitter.api import Api as TwitterApi

from twitter import (TwitterError)


class Api(TwitterApi):
    def GetFriendsPaged(self,
                        user_id=None,
                        screen_name=None,
                        cursor=-1,
                        count=200,
                        skip_status=False,
                        include_user_entities=False):
        '''Make a cursor driven call to return the list of all followers

        The caller is responsible for handling the cursor value and looping
        to gather all of the data

        Args:
          user_id:
            The twitter id of the user whose followers you are fetching.
            If not specified, defaults to the authenticated user. [Optional]
          screen_name:
            The twitter name of the user whose followers you are fetching.
            If not specified, defaults to the authenticated user. [Optional]
          cursor:
            Should be set to -1 for the initial call and then is used to
            control what result page Twitter returns.
          count:
            The number of users to return per page, up to a maximum of 200.
            Defaults to 200. [Optional]
          skip_status:
            If True the statuses will not be returned in the user items.
            [Optional]
          include_user_entities:
            When True, the user entities will be included. [Optional]

        Returns:
          next_cursor, previous_cursor, data sequence of twitter.User instances, one for each follower
        '''
        url = '%s/friends/list.json' % self.base_url
        result = []
        parameters = {}
        if user_id is not None:
            parameters['user_id'] = user_id
        if screen_name is not None:
            parameters['screen_name'] = screen_name
        try:
            parameters['count'] = int(count)
        except ValueError:
            raise TwitterError({'message': "count must be an integer"})
        if skip_status:
            parameters['skip_status'] = True
        if include_user_entities:
            parameters['include_user_entities'] = True
        parameters['cursor'] = cursor

        json = self._RequestUrl(url, 'GET', data=parameters)
        data = self._ParseAndCheckTwitter(json.content)

        if 'next_cursor' in data:
            next_cursor = data['next_cursor']
        else:
            next_cursor = 0
        if 'previous_cursor' in data:
            previous_cursor = data['previous_cursor']
        else:
            previous_cursor = 0

        return next_cursor, previous_cursor, data

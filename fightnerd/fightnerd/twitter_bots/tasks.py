from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from fightnerd.celeryapp import app
from fightnerd.twitter_bots.models import Bot
from django.core import management

logger = logging.getLogger(__name__)


class TaskType(object):
    REPLY = 0
    FOLLOW = 1
    UNFOLLOW = 2


@app.task
def run_follow_bots():
    for bot in Bot.objects.filter(is_enabled=True, is_follow_enabled=True):
        run_bot.delay(bot.id, TaskType.FOLLOW)


@app.task
def run_unfollow_bots():
    for bot in Bot.objects.filter(is_enabled=True, is_follow_enabled=True):
        run_bot.delay(bot.id, TaskType.UNFOLLOW)


@app.task
def run_reply_bots():
    for bot in Bot.objects.filter(is_enabled=True, is_reply_enabled=True):
        run_bot.delay(bot.id, TaskType.REPLY)


@app.task
def run_bot(bot_id, task):
    bot = Bot.objects.get(id=bot_id)
    if task == TaskType.REPLY:
        management.call_command('twitter_bot_reply', '--notest', bot.screen_name)
    elif task == TaskType.FOLLOW:
        management.call_command('twitter_bot_follow', '--notest', bot.screen_name)
    elif task == TaskType.UNFOLLOW:
        management.call_command('twitter_bot_unfollow', '--notest', bot.screen_name)
    else:
        logger.error('Unknown task type (%d)')

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


class TwitterBotCacheKeys(object):
    prefix = 'fightnerd.twitter_bots.cache_keys.'

    @staticmethod
    def get_key(suffix):
        return '%s%s' % (TwitterBotCacheKeys.prefix, suffix)

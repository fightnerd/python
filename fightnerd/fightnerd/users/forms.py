from __future__ import absolute_import

from django.contrib.auth import forms as auth_forms
from fightnerd.users.models import User
from django.utils.translation import ugettext_lazy as _
from django import forms

PASSWORD_MIN_LENGTH = 8


def add_class(attributes, class_name):
    if "class" in attributes:
        attributes["class"] = attributes["class"] + " " + class_name
    else:
        attributes["class"] = class_name


def get_widget_styles(initial_widget_styles, classes=None):
    attributes = {}
    for widget_style in initial_widget_styles:
        attributes.update(widget_style)
    if classes:
        if (isinstance(classes, list)):
            for class_name in classes:
                add_class(attributes, class_name)
    return attributes


DATE_WIDGET_STYLES = {"placeholder": "Choose Date", "data-parsley-trigger": "change"}
WIDGET_STYLES = {"class": "form-control form-widget"}
REQUIRED_WIDGET_STYLES = {"data-parsley-required": "true"}
REQUIRED_WIDGET_STYLES.update(WIDGET_STYLES)


class RequiredPassword2Input(forms.PasswordInput):
    def render(self, name, value, attrs=None):
        attrs["id"] = "password2"
        attrs["placeholder"] = "Confirm password"
        attrs["data-parsley-equalto"] = "#password1"
        attrs["data-parsley-equalto-message"] = "Your passwords don't match. Please try again."
        attrs.update(REQUIRED_WIDGET_STYLES)
        return super(RequiredPassword2Input, self).render(name, value, attrs)


class RequiredPassword1Input(forms.PasswordInput):
    def render(self, name, value, attrs=None):
        attrs["id"] = "password1"
        attrs["placeholder"] = "Enter password"
        attrs.update(REQUIRED_WIDGET_STYLES)
        return super(RequiredPassword1Input, self).render(name, value, attrs)


class RequiredFieldMixin(object):
    def add_required_widget_styles(self, attrs):
        if self.placeholder:
            attrs.update(get_widget_styles([{"placeholder": self.placeholder}, REQUIRED_WIDGET_STYLES], None))
        else:
            attrs.update(get_widget_styles([REQUIRED_WIDGET_STYLES], None))
        return attrs


class RequiredCharField(RequiredFieldMixin, forms.CharField):
    placeholder = None

    def __init__(self, placeholder=None, *args, **kwargs):
        self.placeholder = placeholder
        super(RequiredCharField, self).__init__(*args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super(RequiredCharField, self).widget_attrs(widget)
        return self.add_required_widget_styles(attrs)


class RequiredEmailField(RequiredFieldMixin, forms.EmailField):
    placeholder = None

    def __init__(self, placeholder=None, *args, **kwargs):
        self.placeholder = placeholder
        super(RequiredEmailField, self).__init__(*args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super(RequiredEmailField, self).widget_attrs(widget)
        return self.add_required_widget_styles(attrs)


class FromDateField(forms.DateField):
    def widget_attrs(self, widget):
        attrs = super(FromDateField, self).widget_attrs(widget)
        attrs.update(get_widget_styles([DATE_WIDGET_STYLES, REQUIRED_WIDGET_STYLES], ["start-datepicker"]))
        return attrs


class ToDateField(forms.DateField):
    def widget_attrs(self, widget):
        attrs = super(ToDateField, self).widget_attrs(widget)
        attrs.update(get_widget_styles([DATE_WIDGET_STYLES, REQUIRED_WIDGET_STYLES], ["end-datepicker"]))
        return attrs


class TimeField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        super(TimeField, self).__init__(*args, **kwargs)
        if not self.choices:
            self.choices = []
            isHalfHour = False
            for i in range(0, 1440, 30):
                hour = i / 60
                minute, isHalfHour = (30, not isHalfHour) if isHalfHour else (0, not isHalfHour)
                choiceTime = time(hour=hour, minute=minute)
                if hour == 0 and minute == 0:
                    timeString = "Midnight"
                elif hour == 12 and minute == 0:
                    timeString = "Noon"
                else:
                    timeString = choiceTime.strftime("%I:%M %p")
                    timeString = timeString.lstrip("0")
                self.choices.append((i, timeString))

    def widget_attrs(self, widget):
        attrs = super(TimeField, self).widget_attrs(widget)
        attrs.update(WIDGET_STYLES)
        return attrs


class NumberedChoicesField(forms.ChoiceField):
    def __init__(self, min=0, max=10, *args, **kwargs):
        super(NumberedChoicesField, self).__init__(*args, **kwargs)
        self.choices = self.choices if self.choices else [(i, str(i)) for i in range(min, max + 1)]

    def widget_attrs(self, widget):
        attrs = super(NumberedChoicesField, self).widget_attrs(widget)
        attrs.update(WIDGET_STYLES)
        return attrs


class NumberOfRoomsField(NumberedChoicesField):
    def __init__(self, *args, **kwargs):
        super(NumberOfRoomsField, self).__init__(*args, **kwargs)
        if self.choices:
            lastIndex, lastChoice = self.choices[-1]
            lastChoice += '+'
            self.choices[-1] = lastIndex, lastChoice


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given email and password.
    """

    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"), widget=forms.PasswordInput, help_text=_("Enter the same password as above, for verification."))

    def clean_password2(self):
        password1 = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = ('email_address', 'full_name', 'password')


class UserChangeForm(auth_forms.UserChangeForm):
    """
    A form for updating users. Includes all the fields on the user, but replaces the password field with admin"s
    password hash display field.
    """

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        self.fields.pop('username', None)

    class Meta:
        model = User
        fields = ("email_address", "full_name")


class RegistrationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields["email_address"].widget.attrs.update(get_widget_styles([{"placeholder": _("Email address")}, REQUIRED_WIDGET_STYLES], None))
        self.fields["full_name"].widget.attrs.update(get_widget_styles([{"placeholder": _("Full name")}, REQUIRED_WIDGET_STYLES], None))
        self.fields["password1"].widget.attrs.update(get_widget_styles([{"placeholder": _("Password")}, REQUIRED_WIDGET_STYLES], None))
        self.fields["password2"].widget.attrs.update(get_widget_styles([{"placeholder": _("Password confirmation")}, REQUIRED_WIDGET_STYLES], None))

    class Meta:
        model = User
        fields = ["email_address", "full_name", "password", "password2"]


class LoginForm(auth_forms.AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields["username"].label = _("Email address")
        self.fields["username"].widget.attrs.update(get_widget_styles([{"placeholder": _("Email address")}, REQUIRED_WIDGET_STYLES], None))
        self.fields["password"].widget.attrs.update(get_widget_styles([{"placeholder": _("Password")}, REQUIRED_WIDGET_STYLES], None))


class UserSetPasswordForm(auth_forms.SetPasswordForm):
    error_messages = {
        "password_mismatch": _("The two password fields didn't match."),
        "password_too_short": _("The password must be at least 8 characters long."),
        "password_needs_numeric_characters": _("The password must contain at least one number or special character."),
        "password_needs_alpha_characters": _("The password must contain at least one letter."),
    }
    new_password1 = forms.CharField(label=_("New Password"),
                                    widget=RequiredPassword1Input)
    new_password2 = forms.CharField(label=_("Re-enter Password"),
                                    widget=RequiredPassword2Input)

    def validate_password(self, password):
        errors = []
        if not password:
            errors.append(forms.ValidationError(self.error_messages["password_too_short"], code="password_too_short"))
        else:
            if len(password) < PASSWORD_MIN_LENGTH:
                errors.append(forms.ValidationError(self.error_messages["password_too_short"], code="password_too_short"))
            if all([c.isalpha() for c in password]):
                errors.append(forms.ValidationError(self.error_messages["password_needs_numeric_characters"], code="password_needs_numeric_characters"))
            if all([not c.isalpha() for c in password]):
                errors.append(forms.ValidationError(self.error_messages["password_needs_alpha_characters"], code="password_needs_alpha_characters"))
        if errors:
            raise forms.ValidationError(errors)

    def clean(self):
        cleaned_data = super(UserSetPasswordForm, self).clean()
        self.validate_password(cleaned_data.get("new_password1"))
        self.validate_password(cleaned_data.get("new_password2"))


class UserResetPasswordForm(forms.Form):
    email_address = RequiredEmailField(label=_("Email address"),
                                       placeholder=_("Enter email address"))

    def clean_email_address(self):
        email_address = self.cleaned_data['email_address']
        if not User.objects.filter(email_address=email_address).exists():
            raise forms.ValidationError("Looks like this email doesn't have a password. Please try another email.")
        return email_address


class UserRegistrationForm(forms.ModelForm):
    def save(self, commit=True):
        user = super(UserRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = ['email_address', 'full_name', 'password']
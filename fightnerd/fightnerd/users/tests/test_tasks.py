from __future__ import absolute_import

from django import test
from unittest import skip
from fightnerd.users.models import User
from fightnerd.users.models import LoginInfo
# from fightnerd.users.tasks import geocode_login_info
from django.conf import settings

# MAX_LOGIN_INFOS = getattr(settings, 'MAX_LOGIN_INFOS')

# @skip('F this...')
# class GeocodeLoginInfoTestCase(test.TestCase):
#     def setUp(self):
#         User.objects.all().delete()
#         LoginInfo.objects.all().delete()
#         self.user = User.objects.create_user('success+sterlingarcher@simulator.amazonses.com', 'Chukwuemeka Ezekwe', 'sterlingarcher123')
#         self.login_info = LoginInfo.objects.create(user=self.user, ip_address='71.244.248.228')
#
#     def test_geocode_login_info(self):
#         # geocode_login_info(self.login_info.id)
#
#         login_info = LoginInfo.objects.get(id=self.login_info.id)
#
#         self.assertTrue(login_info.location)
#         self.assertTrue(login_info.country_code)
#         self.assertTrue(login_info.country_code3)
#         self.assertTrue(login_info.continent_code)
#         self.assertTrue(login_info.country_name)
#         self.assertTrue(login_info.charset)
#
#     def test_login_info_pruning(self):
#         login_info = None
#         for i in range(MAX_LOGIN_INFOS * 30):
#             login_info = LoginInfo.objects.create(user=self.user, ip_address='71.244.248.228')
#         # geocode_login_info(login_info.id)
#         self.assertEqual(LoginInfo.objects.filter(user=self.user).count(), MAX_LOGIN_INFOS)

from __future__ import absolute_import

from fightnerd.users.models import User
from rest_framework import serializers


class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('full_name',)

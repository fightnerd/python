from __future__ import absolute_import

# from celery import shared_task
# from django.core import mail
# from fightnerd.users.models import LoginInfo
# from django.contrib.gis.geoip import GeoIP
# from fightnerd.celeryapp import app
# from django.conf import settings
#
# MAX_LOGIN_INFOS = getattr(settings, 'MAX_LOGIN_INFOS')
#
#
# @app.task
# def geocode_login_info(login_info_id):
#     login_info = LoginInfo.objects.get(pk=login_info_id)
#     ip_address = login_info.ip_address
#     if not ip_address:
#         return None
#
#     geo_ip = GeoIP()
#     data = geo_ip.city(ip_address)
#     latitude = data.pop('latitude', None)
#     longitude = data.pop('longitude', None)
#
#     if latitude and longitude:
#         data['location'] = 'POINT (%s %s)' % (longitude, latitude)
#
#     # Remove nulls...
#     keys_to_remove = list()
#     for key in data:
#         if data[key] is None:
#             keys_to_remove.append(key)
#     for key in keys_to_remove:
#         data.pop(key, None)
#
#     LoginInfo.objects.filter(pk=login_info_id).update(**data)
#
#     # Do some pruning
#     for l in LoginInfo.objects.filter(user=login_info.user).order_by('-date_created')[MAX_LOGIN_INFOS:]:
#         l.delete()
#
#     return login_info

from __future__ import absolute_import

from django.contrib.gis.db import models
from django.utils import timezone
from django.core.mail import send_mail
from django.utils.http import urlquote
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.conf import settings
import logging
from ipware.ip import get_ip

logger = logging.getLogger(__name__)
PASSWORD_MIN_LENGTH = 8
IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')

def validate_password(password):
    if len(password) < PASSWORD_MIN_LENGTH:
        raise ValidationError({"password": _("Your password must be at least %d character long." % PASSWORD_MIN_LENGTH)})
        # if all([c.isalpha() for c in password]):
        #     raise ValidationError({"password": _("Your password must contain at least one number or special character.")})
        # if all([not c.isalpha() for c in password]):
        #     raise ValidationError({"password": _("Your password must contain at least one letter.")})


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserManager(BaseUserManager):
    def _create_user(self, email_address, full_name, password, is_staff, is_superuser, **extra_fields):
        if not email_address:
            raise ValueError("The given email must be set")
        if not full_name:
            raise ValueError("The given full name must be set")
        user = self.model(email_address=self.normalize_email(email_address),
                          full_name=full_name,
                          is_staff=is_staff,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=timezone.now(),
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email_address, full_name, password=None, **extra_fields):
        user = self._create_user(email_address, full_name, password, False, False, **extra_fields)
        # group, created = Group.objects.get_or_create(name="Clients")
        # user.groups.add(group.id)
        return user

    def create_superuser(self, email_address, full_name, password, **extra_fields):
        return self._create_user(email_address, full_name, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    email_address = models.EmailField(_("email address"), max_length=256, unique=True, error_messages={'unique': 'Profile with this email address already exists.'})
    full_name = models.CharField(_("full name"), max_length=256, blank=False)
    is_staff = models.BooleanField(_("staff status"), default=False, help_text=_("Designates whether the user can log into this admin site."))
    is_active = models.BooleanField(_("active"), default=True, help_text=_("Designates whether this user should be treated as active. Unselect this instead of deleting accounts."))
    date_joined = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = "email_address"
    REQUIRED_FIELDS = ["full_name"]

    def get_absolute_url(self):
        return "/users/%d" % urlquote(self.pk)

    def get_short_name(self):
        return self.full_name

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    @property
    def first_name(self):
        toks = self.full_name.split(' ')
        return toks[0] if len(toks) > 1 else self.full_name

    @property
    def last_name(self):
        toks = self.full_name.split(' ')
        return toks[0] if len(toks) <= 1 else ' '.join(toks[1:])

    def clean(self):
        # Check password
        validate_password(self.password)

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")


class LoginInfo(BaseModel):
    user = models.ForeignKey(User)
    ip_address = models.GenericIPAddressField(blank=True, null=True)

    area_code = models.CharField(max_length=200, blank=True)
    city = models.CharField(max_length=200, blank=True)
    country_code = models.CharField(max_length=200, blank=True)
    country_code3 = models.CharField(max_length=200, blank=True, verbose_name='Country')
    continent_code = models.CharField(max_length=200, blank=True, verbose_name='Continent')
    country_name = models.CharField(max_length=200, blank=True)
    dma_code = models.CharField(max_length=200, blank=True)
    postal_code = models.CharField(max_length=200, blank=True)
    region = models.CharField(max_length=200, blank=True)
    charset = models.CharField(max_length=200, blank=True)

    location = models.PointField(srid=4326, blank=True, null=True)

    objects = models.GeoManager()


# @receiver(user_logged_in)
# def handle_user_logged_in(sender, request, user, **kwargs):
#     # Grab IP address
#     ip_address = get_ip(request)
#     if not ip_address and not IS_TEST_MODE:
#         logger.error('Failed to get IP address!', extra={'request': request})
#
#     # Create login info
#     login_info = LoginInfo.objects.create(user=user, ip_address=ip_address)
#
#     # Geocode that shit...
#     from fightnerd.users.tasks import geocode_login_info
#     geocode_login_info.delay(login_info.id)

from django import template

register = template.Library()

@register.filter
def first_name(full_name):
    if hasattr(full_name, 'full_name'):
        full_name = full_name.full_name
    toks = full_name.split(' ')
    return toks[0] if len(toks) > 1 else full_name
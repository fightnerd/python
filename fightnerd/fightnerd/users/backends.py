from django.contrib.auth import backends
from fightnerd.profiles.tokens import VerifyEmailAddressTokenGenerator
from django.contrib.auth import get_user_model


class VerificationEmailTokenBackend(backends.ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """
    def authenticate(self, token=None, username=None, **kwargs):
        UserModel = get_user_model()
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)
        if not username or not token:
            return None

        user = UserModel._default_manager.get(email_address=username)
        if VerifyEmailAddressTokenGenerator().check_token(user, token):
            return user

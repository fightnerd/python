from __future__ import absolute_import

from django.contrib import admin
from django.contrib import auth
from django.utils.translation import ugettext_lazy as _
from fightnerd.users.models import User
from fightnerd.users.forms import UserChangeForm, UserCreationForm


class UserAdmin(auth.admin.UserAdmin):
    readonly_fields = ("date_joined",)
    fieldsets = (
        (None, {"fields": ("email_address", "full_name", "password")}),
        (_("Personal info"), {"fields": ("full_name",)}),
        (_("Permissions"), {"fields": ("is_active", "is_staff", "is_superuser",
                                       "groups", "user_permissions")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email_address', 'full_name', 'password', 'password2')
        }),
    )
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ("email_address", "full_name", "is_staff")
    search_fields = ("email_address", "full_name")
    ordering = ("email_address",)


admin.site.register(User, UserAdmin)

from __future__ import absolute_import
from optparse import make_option

from django.core.management.base import BaseCommand
from fightnerd.users import tasks
from django.conf import settings


DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
IS_LOCAL = getattr(settings, 'IS_LOCAL')


def send_mail(subject,
              message,
              recipients,
              from_email=DEFAULT_FROM_EMAIL,
              fail_silently=False,
              auth_user=None,
              auth_password=None,
              connection=None,
              html_message=None):
    if not subject:
        raise ValueError('subject must not be null')
    if not message:
        raise ValueError('message must not be null')
    if not recipients:
        raise ValueError('recipient_list must not be null')

    if IS_LOCAL:
        tasks.send_mail.apply(args=[subject, message, from_email, recipients], kwargs={
            'fail_silently': fail_silently,
            'auth_user': auth_user,
            'auth_password': auth_password,
            'connection': connection,
            'html_message': html_message
        })
    else:
        tasks.send_mail.apply_async(args=[subject, message, from_email, recipients], kwargs={
            'fail_silently': fail_silently,
            'auth_user': auth_user,
            'auth_password': auth_password,
            'connection': connection,
            'html_message': html_message
        })
    return True


class Command(BaseCommand):
    base_options = (
        make_option(
            '-s', '--subject', action='store', dest='subject',
            type='str',
            help='Subject of email'
        ),
        make_option(
            '-f', '--from', action='store', dest='from_email',
            type='str',
            help='Sender\'s email address'
        ),
        make_option(
            '-m', '--message', action='store', dest='message',
            type='str',
            help='Body of email.'
        ),
        make_option(
            '-r', '--recipient', action='append', dest='recipients',
            help='Email address of recipient'
        ),
    )
    option_list = BaseCommand.option_list + base_options

    def run_from_argv(self, argv):
        self._argv = argv
        return super(Command, self).run_from_argv(argv)

    def handle(self, *args, **options):
        send_mail(options['subject'],
                  options['message'],
                  options['recipients'],
                  from_email=options['from_email'])

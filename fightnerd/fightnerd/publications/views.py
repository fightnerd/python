from __future__ import absolute_import

import logging
from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from django.views.generic import DetailView
from django.conf import settings
from fightnerd.views import NeverCacheMixin
from django.http import HttpResponse

logger = logging.getLogger(__name__)


class ArticleDetailView(DetailView):
    model = Article
    template_name_field = 'article'
    slug_field = 'guid'
    slug_url_kwarg = 'guid'


class VideoDetailView(DetailView):
    model = Video
    template_name_field = 'video'
    slug_field = 'guid'
    slug_url_kwarg = 'guid'

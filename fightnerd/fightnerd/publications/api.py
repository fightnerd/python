from fightnerd.utils.models import UserActionType
from fightnerd.utils.models import UserAction
from fightnerd.publications.models import Article
from fightnerd.publications.models import Publication
from fightnerd.publications.models import Author
from fightnerd.publications.models import Video
from fightnerd.publications.models import Channel
from fightnerd.publications.serializers import ArticleSerializer
from fightnerd.publications.serializers import PublicationSerializer
from fightnerd.publications.serializers import VideoSerializer
from fightnerd.publications.serializers import ChannelSerializer
from fightnerd.publications.filters import ArticleFilter
from fightnerd.publications.filters import VideoFilter
from fightnerd.api import HaystackFilter
from rest_framework import viewsets
from rest_framework import filters
from fightnerd.pagination import LimitOffsetPagination
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework import mixins
import django_filters


class ArticleViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = Article.objects.all()
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend)
    filter_class = ArticleFilter
    ordering = ('-date',)
    ordering_fields = ('date',)
    pagination_class = LimitOffsetPagination
    lookup_field = 'guid'
    document_uid_field = 'guid'

    @detail_route(methods=['post'])
    def view(self, request, guid=None):
        article = self.get_object()
        UserAction.objects.create(type=UserActionType.VIEW, content_object=article)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def like(self, request, guid=None):
        article = self.get_object()
        UserAction.objects.create(type=UserActionType.LIKE, content_object=article)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def unlike(self, request, guid=None):
        article = self.get_object()
        UserAction.objects.create(type=UserActionType.UNLIKE, content_object=article)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def save(self, request, guid=None):
        article = self.get_object()
        UserAction.objects.create(type=UserActionType.SAVE, content_object=article)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def unsave(self, request, guid=None):
        article = self.get_object()
        UserAction.objects.create(type=UserActionType.UNSAVE, content_object=article)
        return Response({'status': 'success'})

    def get_serializer_class(self):
        return ArticleSerializer


class VideoViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    queryset = Video.objects.all()
    filter_backends = (filters.OrderingFilter, filters.DjangoFilterBackend)
    filter_class = VideoFilter
    ordering_fields = ('date',)
    ordering = ('-date',)
    lookup_field = 'guid'
    document_uid_field = 'guid'

    @detail_route(methods=['post'])
    def view(self, request, guid=None):
        video = self.get_object()
        UserAction.objects.create(type=UserActionType.VIEW, content_object=video)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def like(self, request, guid=None):
        video = self.get_object()
        UserAction.objects.create(type=UserActionType.LIKE, content_object=video)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def unlike(self, request, guid=None):
        video = self.get_object()
        UserAction.objects.create(type=UserActionType.UNLIKE, content_object=video)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def save(self, request, guid=None):
        video = self.get_object()
        UserAction.objects.create(type=UserActionType.SAVE, content_object=video)
        return Response({'status': 'success'})

    @detail_route(methods=['post'])
    def unsave(self, request, guid=None):
        video = self.get_object()
        UserAction.objects.create(type=UserActionType.UNSAVE, content_object=video)
        return Response({'status': 'success'})

    def get_serializer_class(self):
        return VideoSerializer


class PublicationViewSet(mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         viewsets.GenericViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer


class ChannelViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer


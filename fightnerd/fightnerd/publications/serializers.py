from __future__ import absolute_import

from rest_framework import serializers

from fightnerd.publications.models import Article
from fightnerd.publications.models import Channel
from fightnerd.publications.models import Publication
from fightnerd.publications.models import Video


class ChannelSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Channel
        fields = [
            'name'
        ]


class PublicationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Publication
        fields = [
            'name'
        ]


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    content_url = serializers.HyperlinkedIdentityField(
        view_name='publications:video_detail',
        lookup_field='guid'
    )
    channel = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()

    def get_channel(self, obj):
        return obj.channel.name if obj.channel else None

    def get_author(self, obj):
        return obj.author.name if obj.author else None

    class Meta:
        model = Video
        fields = [
            'title',
            'image_url',
            'date',
            'source_url',
            'author',
            'guid',
            'channel',
            'url',
            'content_url'
        ]
        extra_kwargs = {
            'url': {
                'view_name': 'api_publications:video_detail',
                'lookup_field': 'guid'
            }
        }


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    content_url = serializers.HyperlinkedIdentityField(
        view_name='publications:article_detail',
        lookup_field='guid'
    )
    publication = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()

    def get_publication(self, obj):
        return obj.publication.name

    def get_author(self, obj):
        return obj.author.name if obj.author else None

    class Meta:
        model = Article
        fields = [
            'title',
            'date',
            'image_url',
            'publication',
            'author',
            'source_url',
            'guid',
            'url',
            'content_url'
        ]
        extra_kwargs = {
            'url': {
                'view_name': 'api_publications:article_detail',
                'lookup_field': 'guid'
            }
        }


class ArticleAppleNotificationSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api_publications:article_detail',
        lookup_field='guid'
    )

    class Meta:
        model = Article
        fields = [
            'url'
        ]


class VideoAppleNotificationSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api_publications:video_detail',
        lookup_field='guid'
    )

    class Meta:
        model = Video
        fields = [
            'url'
        ]

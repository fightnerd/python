from datetime import timedelta
import random
import string

from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from fightnerd.publications.models import Publication
from fightnerd.publications.models import Author
from fightnerd.publications.models import Channel
from fightnerd.utils import get_lorem_text, get_ascii_text
from django.utils import timezone


def create_publications(num_publications):
    publications = list()
    for i in range(num_publications):
        publication = Publication.objects.create(
            name=get_ascii_text().upper(),
        )
        publications.append(publication)
    return publications


def create_authors(num_authors):
    authors = list()
    for i in range(num_authors):
        is_unique = False
        while not is_unique:
            first_name = get_lorem_text(1, 'w').capitalize()
            last_name = get_lorem_text(1, 'w').capitalize()
            is_unique = not Author.objects.filter(name='%s %s' % (first_name, last_name)).exists()
        author = Author.objects.create(
            name='%s %s' % (first_name, last_name)
        )
        authors.append(author)
    return authors


def create_channels(num_channels):
    channels = list()
    for i in range(num_channels):
        name = get_lorem_text(5, 'w').capitalize()
        channel = Channel.objects.create(name=name)
        channels.append(channel)
    return channels


def create_videos(num_videos=20, num_authors=5, num_channels=5):
    videos = list()
    authors = create_authors(num_authors)
    channels = create_channels(num_channels)
    for i in range(num_videos):
        video = Video.objects.create(
            title=get_lorem_text(random.randint(1, 5), 'w').capitalize(),
            author=random.choice(authors),
            channel=random.choice(channels),
            description=get_lorem_text(random.randint(1, 10), 'b'),
            source_url='https://you.tube/%s' % get_ascii_text(),
            image_url='https://you.tube/%s' % get_ascii_text(),
            date=timezone.now() - timedelta(days=random.randint(0, 5))
        )
        videos.append(video)

    return videos


def create_articles(num_articles=20, num_publications=5, num_authors=5):
    articles = list()
    publications = create_publications(num_publications)
    authors = create_authors(num_authors)
    ascii_characters = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
    for i in range(num_articles):
        publication = random.choice(publications)
        author = random.choice(authors)
        content = get_lorem_text(random.randint(1, 10), 'b')
        title = get_lorem_text(random.randint(1, 5), 'w').capitalize()
        url = 'https://example.com/%s' % get_ascii_text()
        image_url = 'https://example.com/%s' % get_ascii_text()
        date = timezone.now() - timedelta(days=random.randint(0, 5))
        article = Article.objects.create(
            content=content,
            title=title,
            image_url=image_url,
            source_url=url,
            date=date,
            author=author,
            publication=publication
        )
        articles.append(article)
    return articles


def tear_down_articles():
    Publication.objects.all().delete()
    Article.objects.all().delete()
    Video.objects.all().delete()
    Author.objects.all().delete()
    Channel.objects.all().delete()


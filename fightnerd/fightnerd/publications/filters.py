from __future__ import absolute_import

import operator
from itertools import chain

import django_filters
from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from drf_haystack import filters
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils import six


class ArticleFilter(django_filters.rest_framework.FilterSet):
    q = django_filters.CharFilter(method='filter_by_query')
    publication = django_filters.CharFilter(name='publication__name')

    def filter_by_query(self, queryset, name, value):
        return queryset.filter(title__icontains=value)

    class Meta:
        model = Article
        fields = {
            'date': ['lte', 'gte']
        }


class VideoFilter(django_filters.rest_framework.FilterSet):
    q = django_filters.CharFilter(method='filter_by_query')
    channel = django_filters.CharFilter(name='channel__name')
    author = django_filters.CharFilter(name='author__name')

    def filter_by_query(self, queryset, name, value):
        return queryset.filter(title__icontains=value)

    class Meta:
        model = Video
        fields = {
            'date': ['lte', 'gte']
        }

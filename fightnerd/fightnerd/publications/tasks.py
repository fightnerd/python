from fightnerd.celeryapp import app
from fightnerd.publications.models import Publication
from fightnerd.publications.models import Article
from fightnerd.publications.models import Author
from fightnerd.publications.models import Video
import os
from dateutil import parser as dateparser
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core import management
from django.utils import encoding
from django.contrib.sites.models import Site
from django.template import loader
from selenium.webdriver.phantomjs.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime, timedelta

BASE_DIR = getattr(settings, 'BASE_DIR')
logger = get_task_logger(__name__)
TZD = getattr(settings, 'TZD')


@app.task()
def bootstrap_publications():
    spiders = [
        'bleacher_report_news',
        'five_ounces_of_pain_news',
        'sbnation_news',
        'sherdog_news',
        'mma_junkie_news',
    ]
    manage_script = os.path.join(BASE_DIR, 'manage.py')
    for spider in spiders:
        try:
            os.system('python %s scrapy \'crawl %s\'' % (manage_script, spider))
        except Exception as e:
            logger.exception('Hmmm')

    # Get Youtube videos
    management.call_command('crawl_youtube', limit=9000)


@app.task()
def crawl_publications():
    spiders = [
        'bleacher_report_xml_news',
        'cage_potato_xml_news',
        'five_ounces_of_pain_xml_news',
        'sbnation_xml_news',
        'sherdog_xml_news',
        'mma_junkie_xml_news',
    ]
    manage_script = os.path.join(BASE_DIR, 'manage.py')
    for spider in spiders:
        try:
            os.system('python %s scrapy \'crawl %s\'' % (manage_script, spider))
        except Exception as e:
            logger.exception('Hmmm')

    # Get Youtube videos
    management.call_command('crawl_youtube')


@app.task()
def process_publication(publication_item):
    publication, did_create = Publication.objects.get_or_create(name=publication_item['name'])
    articles = list()
    for article_item in publication_item['articles']:
        source_url = article_item['source_url']

        # Defaults
        defaults = dict()
        defaults['publication'] = publication

        # Process author
        if 'author' in article_item and article_item['author']:
            author_item = article_item['author']
            author, did_create = Author.objects.get_or_create(name=author_item['name'])
            defaults['author'] = author

        defaults['content'] = article_item['content']
        defaults['title'] = article_item['title']
        defaults['image_url'] = article_item['image_url'] if article_item['image_url'] is not None else ''
        defaults['video_url'] = article_item['video_url'] if article_item['video_url'] is not None else ''
        defaults['date'] = dateparser.parse(article_item['date'], tzinfos=TZD) if article_item['date'] else None

        article, did_create = Article.objects.update_or_create(source_url=source_url, defaults=defaults)

        # Render
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'article': article
        })
        articles.append(article)
        # try:
        #     logger.info('\nProcessed "%s" for %s\nImage: %s' % (article, publication, article.image_url))
        # except UnicodeDecodeError:
        #     pass
    # Check imagery
    # articles_with_images = [a for a in articles if a.image_url is not '']
    # percent_articles_with_images = float(len(articles_with_images)) / float(len(articles))
    # if percent_articles_with_images < 0.5:
    #     logger.error('Found too few images for %s' % publication)
    # else:
    #     logger.info('Image rate is %f' % percent_articles_with_images)
    return publication

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('content', models.TextField(blank=True)),
                ('title', models.CharField(db_index=True, max_length=600, blank=True)),
                ('image_url', models.URLField(db_index=True, max_length=600, blank=True)),
                ('video_url', models.URLField(db_index=True, max_length=600, blank=True)),
                ('source_url', models.URLField(max_length=600, db_index=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('guid', models.CharField(db_index=True, unique=True, max_length=50, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('guid', models.CharField(unique=True, max_length=50, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('guid', models.CharField(unique=True, max_length=50, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(db_index=True, max_length=50, blank=True)),
                ('guid', models.CharField(db_index=True, unique=True, max_length=50, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('title', models.CharField(db_index=True, max_length=600, blank=True)),
                ('description', models.TextField(blank=True)),
                ('image_url', models.URLField(db_index=True, max_length=600, blank=True)),
                ('source_url', models.URLField(db_index=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('guid', models.CharField(db_index=True, unique=True, max_length=50, blank=True)),
                ('author', models.ForeignKey(related_name='videos', to='publications.Author', null=True)),
                ('channel', models.ForeignKey(related_name='videos', to='publications.Channel', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='article',
            name='author',
            field=models.ForeignKey(to='publications.Author', null=True),
        ),
        migrations.AddField(
            model_name='article',
            name='publication',
            field=models.ForeignKey(related_name='articles', to='publications.Publication'),
        ),
    ]

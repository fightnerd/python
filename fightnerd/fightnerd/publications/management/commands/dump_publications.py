from __future__ import absolute_import

from django.core.management.base import BaseCommand
from celery.utils.log import get_task_logger
from django.conf import settings
from googleapiclient import discovery
from dateutil.parser import parse as parse_date
import json
from fightnerd.publications.models import Publication
from fightnerd.publications.search_indexes import ArticleIndex
from fightnerd.publications.serializers import ArticleDetailSerializer
from fightnerd.publications.serializers import PublicationSerializer
from haystack.query import SearchQuerySet
from djangorestframework_camel_case.render import CamelCaseJSONRenderer
import simplejson


logger = get_task_logger(__name__)


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('output_file_name')

    def handle(self, *args, **options):
        sqs = SearchQuerySet().models(Publication).order_by('date')
        serializer = PublicationSerializer(sqs, many=True)
        rendered_data = CamelCaseJSONRenderer().render(serializer.data)
        with open(options.get('output_file_name'), 'w') as f:
            f.write(rendered_data)

from __future__ import absolute_import

from django.core.management.base import BaseCommand
from celery.utils.log import get_task_logger
from django.conf import settings
from googleapiclient import discovery
from fightnerd.publications.models import Video
from fightnerd.publications.models import Author
from fightnerd.publications.models import Channel
from dateutil.parser import parse as parse_date
import json


logger = get_task_logger(__name__)
GOOGLE_API_KEY = getattr(settings, 'GOOGLE_API_KEY')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('--limit', dest='limit', type=int, default=50)

    def get_channel(self, user, title):
        channels = {
            'MMAFightingOnSBN': 'MMA Fighting',
            'UFC': 'UFC',
            'BellatorMMA': 'Bellator MMA',
        }
        if 'Free Fight' in title:
            name = 'Free Fights'
        else:
            name = channels.get(user)
        channel, is_created = Channel.objects.get_or_create(name=name)
        return channel

    def handle(self, *args, **options):
        youtube_service = discovery.build('youtube', 'v3', developerKey=GOOGLE_API_KEY)
        users = [
            'MMAFightingOnSBN',
            'UFC',
            'BellatorMMA'
        ]
        for user in users:
            # Get channel ID
            response = youtube_service.channels().list(part='id', forUsername=user).execute()
            channel_id = response.get('items')[0].get('id')

            # Get videos for channel
            next_page_token = None
            is_first_request = True
            videos = list()
            while next_page_token or is_first_request:
                is_first_request = False
                kwargs = {
                    'type': 'video',
                    'part': 'id, snippet',
                    'order': 'date',
                    'channelId': channel_id,
                    'maxResults': 50
                }
                if next_page_token:
                    kwargs.update({'pageToken': next_page_token})

                # Query Google
                response = youtube_service.search().list(**kwargs).execute()

                # Process items in response
                for item in response.get('items'):
                    defaults = dict()
                    defaults['title'] = item.get('snippet').get('title')
                    defaults['description'] = item.get('snippet').get('description')
                    defaults['image_url'] = item.get('snippet').get('thumbnails').get('high').get('url')
                    defaults['author'], is_created = Author.objects.get_or_create(name=user)
                    defaults['channel'] = self.get_channel(user, defaults['title'])
                    url = 'https://www.youtube.com/watch?v=%s' % item.get('id').get('videoId')
                    defaults['date'] = parse_date(item.get('snippet').get('publishedAt'))
                    video, is_created = Video.objects.update_or_create(source_url=url, defaults=defaults)
                    videos.append(video)
                    logger.info('Processed %s' % video)
                next_page_token = response.get('nextPageToken', None) if len(videos) < options.get('limit') else None

from __future__ import absolute_import

from django.core.management.base import BaseCommand
from celery.utils.log import get_task_logger
from django.conf import settings
from googleapiclient import discovery
from fightnerd.publications.tasks import crawl_publications
from dateutil.parser import parse as parse_date
import json


logger = get_task_logger(__name__)
GOOGLE_API_KEY = getattr(settings, 'GOOGLE_API_KEY')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('--limit', dest='limit', type=int, default=50)

    def handle(self, *args, **options):
        crawl_publications.delay()

from __future__ import absolute_import

import os
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core import management
from django.core.management.base import BaseCommand

BASE_DIR = getattr(settings, 'BASE_DIR')
logger = get_task_logger(__name__)
TZD = getattr(settings, 'TZD')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def handle(self, *args, **options):
        spiders = [
            'bleacher_report_news',
            'five_ounces_of_pain_news',
            'sbnation_news',
            'sherdog_news',
            'mma_junkie_news',
        ]
        manage_script = os.path.join(BASE_DIR, 'manage.py')
        for spider in spiders:
            try:
                os.system('python %s scrapy \'crawl %s\'' % (manage_script, spider))
            except Exception as e:
                logger.exception('Hmmm')

        # Get Youtube videos
        management.call_command('crawl_youtube', '--limit', '9000')

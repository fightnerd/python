from django.contrib import admin
from fightnerd.publications.models import Publication
from fightnerd.publications.models import Article


class PublicationAdmin(admin.ModelAdmin):
    pass


class ArticleAdmin(admin.ModelAdmin):
    raw_id_fields = ('publication',)
    ordering = ('date',)


admin.site.register(Publication, PublicationAdmin)
admin.site.register(Article, ArticleAdmin)

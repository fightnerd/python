# this should be at the top of your custom template tags file
import datetime
import time

from django import template


register = template.Library()

# custom template filter - place this in your custom template tags file


@register.filter
def only_hours(value):
    """
    Filter - removes the minutes, seconds, and milliseconds from a datetime

    Example usage in template:

    {{ my_datetime|only_hours|timesince }}

    This would show the hours in my_datetime without showing the minutes or seconds.
    """
    # replace returns a new object instead of modifying in place
    return value.replace(minute=0, second=0, microsecond=0)


@register.filter
def timesince_ago(value):
    """
    Filter - removes the minutes, seconds, and milliseconds from a datetime

    Example usage in template:

    {{ my_datetime|only_hours|timesince }}

    This would show the hours in my_datetime without showing the minutes or seconds.
    """
    now_time = datetime.datetime.utcnow()
    delta = now_time - value
    days = abs(int(delta.days))
    if days >= 1:
        if days == 1:
            return "%d day ago" % (days)
        else:
            return "%d days ago" % (days)
    hours = abs(int(delta.seconds / 3600))
    if hours >= 1:
        if hours == 1:
            return "%d hour ago" % (hours)
        else:
            return "%d hours ago" % (hours)
    minutes = abs(int(delta.seconds / 60))
    if minutes >= 1:
        if minutes == 1:
            return "%d minute ago" % (minutes)
        else:
            return "%d minutes ago" % (minutes)
    seconds = abs(int(delta.seconds))
    if seconds == 1:
        return "%d second ago" % (seconds)
    else:
        return "%d seconds ago" % (seconds)


@register.filter
def epoch(value):
    try:
        return int(time.mktime(value.timetuple()) * 1000)
    except AttributeError:
        return ''

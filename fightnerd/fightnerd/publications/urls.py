from django.conf.urls import patterns, url
from fightnerd.publications.views import ArticleDetailView
from fightnerd.publications.views import VideoDetailView
from django.views.decorators.cache import cache_page, never_cache

urlpatterns = patterns('',
    url(r'^article/(?P<guid>[0-9a-zA-Z]+)/$', cache_page(60 * 60 * 24)(ArticleDetailView.as_view()), name='article_detail'),
    url(r'^video/(?P<guid>[0-9a-zA-Z]+)/$', cache_page(60 * 60 * 24)(VideoDetailView.as_view()), name='video_detail'),
)

import hashlib

import re
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.db import models
from django.db.models.signals import pre_save
from django.http import HttpRequest
from django.template import loader
from django.utils import timezone
from djangorestframework_camel_case.render import CamelCaseJSONRenderer
from rest_framework.request import Request
from django.conf import settings


IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')
API_DOMAIN = getattr(settings, 'API_DOMAIN')
API_PROTOCOL = getattr(settings, 'API_PROTOCOL')


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class Author(BaseModel):
    name = models.CharField(max_length=100, blank=True)
    guid = models.CharField(max_length=50, blank=True, unique=True)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5(instance.name).hexdigest()

    def __unicode__(self):
        return self.name


class Channel(BaseModel):
    name = models.CharField(max_length=100, blank=True)
    guid = models.CharField(max_length=50, blank=True, unique=True)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5(instance.name).hexdigest()

    def __unicode__(self):
        return self.name


class Article(BaseModel):
    content = models.TextField(blank=True)
    title = models.CharField(max_length=600, blank=True, db_index=True)
    image_url = models.URLField(blank=True, db_index=True, max_length=600)
    video_url = models.URLField(blank=True, db_index=True, max_length=600)
    source_url = models.URLField(db_index=True, max_length=600)
    date = models.DateTimeField(null=True, blank=True)
    guid = models.CharField(max_length=50, blank=True, db_index=True, unique=True)

    author = models.ForeignKey('Author', null=True)
    publication = models.ForeignKey('Publication', related_name='articles')

    def fire_notification(self):
        from fightnerd.notifications.models import Notification
        from fightnerd.publications.serializers import ArticleAppleNotificationSerializer

        # Payload
        http_request = HttpRequest()
        http_request.META['HTTP_HOST'] = API_DOMAIN
        serializer = ArticleAppleNotificationSerializer(instance=self, context={'request': Request(http_request)})
        rendered_data = CamelCaseJSONRenderer().render({'article': serializer.data})

        notification = Notification.objects.create(
            fire_date=timezone.now(),
            title=self.title,
            ticker=self.title,
            payload=rendered_data,
            content_object=self,
            object_id=self.id,
            content_type=ContentType.objects.get_for_model(self)
        )
        notification.fire()
        return notification

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5(instance.source_url).hexdigest()

    @property
    def do_use_iframe(self):
        if not self.video_url:
            return False
        patterns = [
            'youtube',
            'springboardplatform',
            'nbcsports'
        ]
        for p in patterns:
            if p in self.video_url:
                return True
        return False

    def __unicode__(self):
        return self.title


class Publication(BaseModel):
    name = models.CharField(max_length=50, blank=True, db_index=True)
    guid = models.CharField(max_length=50, blank=True, db_index=True, unique=True)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5(instance.name).hexdigest()

    @property
    def image_url(self):
        qs = self.articles.exclude(image_url='').order_by('date')
        return qs[0].image_url if qs.exists() else None

    def __unicode__(self):
        return self.name


class Video(BaseModel):
    title = models.CharField(max_length=600, blank=True, db_index=True)
    description = models.TextField(blank=True)
    image_url = models.URLField(blank=True, db_index=True, max_length=600)
    source_url = models.URLField(blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True)
    guid = models.CharField(max_length=50, blank=True, db_index=True, unique=True)

    author = models.ForeignKey('Author', null=True, related_name='videos')
    channel = models.ForeignKey('Channel', null=True, related_name='videos')

    def fire_notification(self):
        from fightnerd.notifications.models import Notification
        from fightnerd.publications.serializers import VideoAppleNotificationSerializer

        # Payload
        http_request = HttpRequest()
        http_request.META['HTTP_HOST'] = API_DOMAIN
        serializer = VideoAppleNotificationSerializer(instance=self, context={'request': Request(http_request)})
        rendered_data = CamelCaseJSONRenderer().render({'video': serializer.data})

        alert = 'VIDEO: %s' % self.title
        notification = Notification.objects.create(
            fire_date=timezone.now(),
            title=alert,
            ticker=alert,
            payload=rendered_data,
            content_object=self,
            object_id=self.id,
            content_type=ContentType.objects.get_for_model(self)
        )
        notification.fire()
        return notification

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5(instance.source_url).hexdigest()

    def __unicode__(self):
        return self.title

    @property
    def embed_url(self):
        toks = re.split(r'embed/|\?', self.source_url.__str__())
        if len(toks) > 1:
            embed_url = 'https://www.youtube.com/embed/%s' % toks[1]
            embed_url = embed_url.replace('v=', '')
            return embed_url
        return None

    @property
    def detail_page(self):
        # Render
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'video': self
        })
        return loader.render_to_string('publications/video_detail.html', context)


pre_save.connect(Author.pre_save, Author, dispatch_uid='fightnerd.fighters.models.Author')
pre_save.connect(Article.pre_save, Article, dispatch_uid='fightnerd.fighters.models.Article')
pre_save.connect(Publication.pre_save, Publication, dispatch_uid='fightnerd.fighters.models.Publication')
pre_save.connect(Video.pre_save, Video, dispatch_uid='fightnerd.fighters.models.Video')
pre_save.connect(Channel.pre_save, Channel, dispatch_uid='fightnerd.fighters.models.Channel')

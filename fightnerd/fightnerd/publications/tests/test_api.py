import urllib

from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from fightnerd.publications.utils import create_articles
from django.core.management import call_command
from fightnerd.utils.models import UserActionType
from fightnerd.utils.models import UserAction
from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from fightnerd.publications.models import Channel
from fightnerd.publications.models import Author
from fightnerd.publications.models import Publication
from fightnerd.publications.utils import create_articles
from fightnerd.publications.utils import create_videos
from fightnerd.publications.utils import tear_down_articles
from dateutil.parser import parse as parse_date
from haystack.query import SearchQuerySet
import simplejson
from django.contrib.contenttypes.models import ContentType


def reverse_with_query(view_name, query_params):
    url = reverse(view_name)
    if query_params:
        url += '?' + urllib.urlencode(query_params)
    return url


class BaseTestCases(object):
    class PublicationAPITestCase(APITestCase):
        list_view_name = None

        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.PublicationAPITestCase, cls).setUpClass()
            create_articles(500)
            create_videos(500)

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.PublicationAPITestCase, cls).tearDownClass()
            tear_down_articles()

        def test_cache_headers(self):
            url = reverse(self.list_view_name)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertIn('Last-Modified', response)
            self.assertIn('Expires', response)
            self.assertIn('Cache-Control', response)
            self.assertIn('max-age=3600', response['Cache-Control'])


class PublicationRetrievalAPITestCase(BaseTestCases.PublicationAPITestCase):
    list_view_name = 'api_publications:publication_list'

    def test_publication_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)


class ChannelRetrievalAPITestCase(BaseTestCases.PublicationAPITestCase):
    list_view_name = 'api_publications:channel_list'

    def test_channel_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)


class VideoUpdateAPITestCase(BaseTestCases.PublicationAPITestCase):
    def tearDown(self):
        super(VideoUpdateAPITestCase, self).tearDown()
        UserAction.objects.all().delete()

    def test_cache_headers(self):
        pass

    def test_read_video(self):
        video = Video.objects.all()[0]
        url = reverse('api_publications:video_view', kwargs={'guid': video.guid})
        response = self.client.post(url)
        video = Video.objects.get(guid=video.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(video)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.VIEW, object_id=video.id, content_type=content_type).count(), 1)

    def test_like_video(self):
        video = Video.objects.all()[0]
        url = reverse('api_publications:video_like', kwargs={'guid': video.guid})
        response = self.client.post(url)
        video = Video.objects.get(guid=video.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(video)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.LIKE, object_id=video.id, content_type=content_type).count(), 1)

    def test_unlike_video(self):
        video = Video.objects.all()[0]
        url = reverse('api_publications:video_unlike', kwargs={'guid': video.guid})
        response = self.client.post(url)
        video = Video.objects.get(guid=video.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(video)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.UNLIKE, object_id=video.id, content_type=content_type).count(), 1)

    def test_save_video(self):
        video = Video.objects.all()[0]
        url = reverse('api_publications:video_save', kwargs={'guid': video.guid})
        response = self.client.post(url)

        video = Video.objects.get(guid=video.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(video)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.SAVE, object_id=video.id, content_type=content_type).count(), 1)

    def test_unsave_video(self):
        video = Video.objects.all()[0]
        url = reverse('api_publications:video_unsave', kwargs={'guid': video.guid})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        video = Video.objects.get(guid=video.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(video)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.UNSAVE, object_id=video.id, content_type=content_type).count(), 1)


class VideoRetrievalAPITestCase(BaseTestCases.PublicationAPITestCase):
    list_view_name = 'api_publications:video_list'
    detail_view_name = 'api_publications:video_detail'

    def test_pagination(self):
        url = reverse(self.list_view_name)

        last_results = None
        for i in range(3):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertGreater(len(response.data.get('results')), 0)
            self.assertTrue('next' in response.data)
            if last_results:
                self.assertNotEqual(last_results, response.data.get('results'))
            last_results = response.data.get('results')
            url = response.data.get('next')

    # def test_video_detail(self):
    #     video = Video.objects.all()[0]
    #     url = reverse(self.detail_view_name, kwargs={'guid': video.guid})
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #
    #     video = response.rendered_content
    #     video = simplejson.loads(video)
    #     self.assertTrue('title' in video)
    #     self.assertTrue('text' not in video)
    #     self.assertTrue('description' not in video)
    #     self.assertTrue('sourceUrl' in video)
    #     self.assertTrue('author' in video)
    #     self.assertTrue('channel' in video)
    #     self.assertTrue('guid' in video)
    #     self.assertTrue('imageUrl' in video)
    #     self.assertTrue('detailPage' in video)
    #     self.assertIsNotNone(video.get('url'))
    #     self.assertTrue('http://' in video.get('url'))

    def test_video_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for video in response.data.get('results'):
            self.assertTrue('title' in video)
            self.assertTrue('text' not in video)
            self.assertTrue('description' not in video)
            self.assertTrue('sourceUrl' in video)
            self.assertTrue('author' in video)
            self.assertTrue('channel' in video)
            self.assertTrue('guid' in video)
            self.assertTrue('imageUrl' in video)
            self.assertTrue('contentUrl' in video)
            self.assertTrue('url' in video)

    def test_order_by_date(self):
        data = {
            'ordering': '-date',
        }
        url = reverse_with_query(self.list_view_name, data)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        last_video_date = None
        for video in response.data.get('results'):
            video_date = parse_date(video.get('date'))
            if last_video_date:
                self.assertGreaterEqual(last_video_date, video_date)
            last_video_date = video_date

    def test_filter_by_author(self):
        author = Author.objects.exclude(videos=None)[0]
        query = {
            'author': author.name,
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertGreater(len(response.data.get('results')), 0)
        for video in response.data.get('results'):
            self.assertEqual(video.get('author'), author.name)

    def test_filter_by_channel(self):
        channel = Channel.objects.exclude(videos=None)[0]
        query = {
            'channel': channel.name,
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertGreater(len(response.data.get('results')), 0)
        for video in response.data.get('results'):
            self.assertEqual(video.get('channel'), channel.name)

    def test_filter_by_query(self):
        query = Video.objects.all()[0].title
        query = query[:int(len(query) / 2)]
        url = reverse_with_query(self.list_view_name, {'q': query})
        response = self.client.get(url)
        videos = response.data.get('results')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(videos), 0)
        for video in videos:
            self.assertTrue(query.lower() in video.get('title').lower())


class ArticleUpdateAPITestCase(BaseTestCases.PublicationAPITestCase):
    def tearDown(self):
        super(ArticleUpdateAPITestCase, self).tearDown()
        UserAction.objects.all().delete()

    def test_cache_headers(self):
        pass

    def test_read_article(self):
        article = Article.objects.all()[0]
        url = reverse('api_publications:article_view', kwargs={'guid': article.guid})
        response = self.client.post(url)
        article = Article.objects.get(guid=article.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(article)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.VIEW, object_id=article.id, content_type=content_type).count(), 1)

    def test_like_article(self):
        article = Article.objects.all()[0]
        url = reverse('api_publications:article_like', kwargs={'guid': article.guid})
        response = self.client.post(url)
        article = Article.objects.get(guid=article.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(article)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.LIKE, object_id=article.id, content_type=content_type).count(), 1)

    def test_unlike_article(self):
        article = Article.objects.all()[0]
        url = reverse('api_publications:article_unlike', kwargs={'guid': article.guid})
        response = self.client.post(url)
        article = Article.objects.get(guid=article.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(article)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.UNLIKE, object_id=article.id, content_type=content_type).count(), 1)

    def test_save_article(self):
        article = Article.objects.all()[0]
        url = reverse('api_publications:article_save', kwargs={'guid': article.guid})
        response = self.client.post(url)

        article = Article.objects.get(guid=article.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(article)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.SAVE, object_id=article.id, content_type=content_type).count(), 1)

    def test_unsave_article(self):
        article = Article.objects.all()[0]
        url = reverse('api_publications:article_unsave', kwargs={'guid': article.guid})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        article = Article.objects.get(guid=article.guid)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content_type = ContentType.objects.get_for_model(article)
        self.assertEqual(UserAction.objects.filter(type=UserActionType.UNSAVE, object_id=article.id, content_type=content_type).count(), 1)


class ArticleRetrievalAPITestCase(BaseTestCases.PublicationAPITestCase):
    list_view_name = 'api_publications:article_list'
    detail_view_name = 'api_publications:article_detail'

    def test_pagination(self):
        url = reverse(self.list_view_name)

        last_results = None
        for i in range(3):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertGreater(len(response.data.get('results')), 0)
            self.assertTrue('next' in response.data)
            if last_results:
                self.assertNotEqual(last_results, response.data.get('results'))
            last_results = response.data.get('results')
            url = response.data.get('next')

    def test_article_detail(self):
        article = Article.objects.all()[0]
        url = reverse(self.detail_view_name, kwargs={'guid': article.guid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        article = response.rendered_content
        article = simplejson.loads(article)
        self.assertTrue('text' not in article)
        self.assertTrue('contentUrl' in article)
        self.assertTrue('sourceUrl' in article)
        self.assertTrue('title' in article)
        self.assertTrue('author' in article)
        self.assertTrue('guid' in article)
        self.assertTrue('publication' in article)
        self.assertTrue('imageUrl' in article)
        self.assertTrue('url' in article)
        self.assertIsNotNone(article.get('url'))
        self.assertTrue('http://' in article.get('url'))

    def test_article_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertGreater(len(response.data.get('results')), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for article in response.data.get('results'):
            self.assertTrue('text' not in article)
            self.assertTrue('detailPage' not in article)
            self.assertTrue('guid' in article)
            self.assertTrue('sourceUrl' in article)
            self.assertTrue('title' in article)
            self.assertTrue('author' in article)
            self.assertTrue('publication' in article)
            self.assertTrue('imageUrl' in article)
            self.assertTrue('url' in article)
            self.assertTrue('contentUrl' in article)
            self.assertIsNotNone(article.get('contentUrl'))
            self.assertTrue('http://' in article.get('contentUrl'))

    def test_filter_by_publication(self):
        publication = Publication.objects.exclude(articles=None)[0]
        query = {
            'publication': publication.name,
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertGreater(len(response.data.get('results')), 0)
        for article in response.data.get('results'):
            self.assertTrue('detailPage' not in article)
            self.assertEqual(article.get('publication'), publication.name)

    def test_filter_by_date(self):
        qs = Article.objects.order_by('-date')
        date = qs[qs.count() / 2].date
        query = {
            'date__lte': date.isoformat()
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertGreater(len(response.data.get('results')), 0)
        for article in response.data.get('results'):
            self.assertTrue('detailPage' not in article)
            self.assertLessEqual(parse_date(article.get('date')), date)

    def test_article_list_order_by_date(self):
        data = {}
        url = reverse_with_query(self.list_view_name, data)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        last_article_date = None
        self.assertGreater(len(response.data.get('results')), 0)
        for article in response.data.get('results'):
            article_date = parse_date(article.get('date'))
            if last_article_date:
                self.assertGreaterEqual(last_article_date, article_date)
            last_article_date = article_date
            self.assertTrue('detailPage' not in article)

    def test_filter_by_query(self):
        query = Article.objects.all()[0].title
        query = query[:int(len(query) / 2)]
        url = reverse_with_query(self.list_view_name, {'q': query})
        response = self.client.get(url)
        articles = response.data.get('results')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(articles), 0)
        for article in articles:
            self.assertTrue(query.lower() in article.get('title').lower())

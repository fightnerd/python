from django.core.management import call_command
from django.core.urlresolvers import reverse
from django_webtest import WebTest
from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from fightnerd.publications.utils import create_articles
from fightnerd.publications.utils import create_videos
from fightnerd.publications.utils import tear_down_articles
from rest_framework import status


class BaseTestCases(object):
    class ViewTestCase(WebTest):
        def get_url(self):
            raise NotImplementedError()

        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.ViewTestCase, cls).setUpClass()
            create_articles(10)
            create_videos(10)

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.ViewTestCase, cls).tearDownClass()
            tear_down_articles()

        def test_cache_headers(self):
            url = self.get_url()
            response = self.app.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertIn('Last-Modified', response.headers)
            self.assertIn('Expires', response.headers)
            self.assertIn('Cache-Control', response.headers)
            self.assertIn('max-age=86400', response.headers['Cache-Control'])


class ArticleDetailViewTestCase(BaseTestCases.ViewTestCase):
    def setUp(self):
        super(ArticleDetailViewTestCase, self).setUp()
        self.article = Article.objects.all()[0]

    def get_url(self):
        return reverse('publications:article_detail', kwargs={'guid': self.article.guid})

    def test_page(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class VideoDetailViewTestCase(BaseTestCases.ViewTestCase):
    def setUp(self):
        super(VideoDetailViewTestCase, self).setUp()
        self.video = Video.objects.all()[0]

    def get_url(self):
        return reverse('publications:video_detail', kwargs={'guid': self.video.guid})

    def test_page(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

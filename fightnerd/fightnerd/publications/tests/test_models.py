from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


from django.test import TestCase
from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from fightnerd.notifications.models import Notification
from fightnerd.publications.utils import create_articles
from fightnerd.publications.utils import tear_down_articles
from fightnerd.publications.utils import create_videos
from fightnerd.publications.utils import tear_down_articles
from django.utils import timezone
from fightnerd.utils import get_lorem_text
from datetime import timedelta
from django.core.management import call_command
from django_webtest import WebTest
import simplejson
from rest_framework import status


class ArticleTestCase(WebTest):
    def setUp(self):
        create_articles(num_articles=1)

    def tearDown(self):
        tear_down_articles()

    def test_fire_notification(self):
        article = Article.objects.all()[0]
        notification = article.fire_notification()
        self.assertIsNotNone(notification)
        self.assertTrue(notification.did_fire)
        self.assertEqual(article.id, notification.object_id)
        self.assertEqual(Notification.objects.all().count(), 1)

        # Check payload
        payload = simplejson.loads(notification.payload)
        url = payload.get('article').get('url')
        self.assertIsNotNone(url)
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class VideoTestCase(WebTest):
    def setUp(self):
        create_videos(num_videos=1)

    def tearDown(self):
        tear_down_articles()

    def test_fire_notification(self):
        video = Video.objects.all()[0]
        notification = video.fire_notification()
        self.assertIsNotNone(notification)
        self.assertTrue(notification.did_fire)
        self.assertEqual(video.id, notification.object_id)
        self.assertEqual(Notification.objects.all().count(), 1)

        # Check payload
        payload = simplejson.loads(notification.payload)
        url = payload.get('video').get('url')
        self.assertIsNotNone(url)
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

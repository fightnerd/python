# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0002_remove_user_user_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='num_followers',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='bot',
            name='num_followings',
            field=models.IntegerField(default=0),
        ),
    ]

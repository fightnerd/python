from django.test import TestCase

from fightnerd.instagram_bots.models import Bot
from fightnerd.instagram_bots.models import SpeedType


class BotTestCase(TestCase):
    def setUp(self):
        self.bot = Bot.objects.create(
            username='BOTTER9000',
            password='adsfsajdsajlkasji;l243443',
            speed=SpeedType.SLOW
        )

    def test_init(self):
        Bot()

    def _test_speed_limit(self, speed):
        self.bot.speed = speed
        self.bot.save()
        follows = range(SpeedType.follow.get(speed) - 1)
        self.assertTrue(self.bot.is_under_follow_speed_limit(len(follows)))

    def test_slow_speed_limit(self):
        self._test_speed_limit(SpeedType.SLOW)

    def test_normal_speed_limit(self):
        self._test_speed_limit(SpeedType.NORMAL)

    def test_fast_speed_limit(self):
        self._test_speed_limit(SpeedType.FAST)

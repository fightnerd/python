from __future__ import absolute_import

import logging

from django.core.management.base import BaseCommand
from fightnerd.instagram_bots.models import Bot
from fightnerd.instagram_bots.models import Follow
from fightnerd.instagram_bots.models import User
from django.core.mail import mail_admins
from django.utils import timezone
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('username')
        parser.add_argument('--clear', action='store_true', dest='do_clear', default=False)
        parser.add_argument('--notest', action='store_false', dest='is_test_mode', default=True)

    def handle(self, *args, **options):
        start_time = timezone.now()
        bot = Bot.objects.get(username=options.get('username'))
        if options.get('do_clear'):
            Follow.objects.all().delete()
            User.objects.all().delete()

        follows = list()
        num_tries = 0
        for i in range(3):
            num_tries = i + 1
            try:
                follows = bot.auto_follow(is_test_mode=options.get('is_test_mode'))
                break
            except:
                logger.exception('')

        end_time = timezone.now()
        duration = end_time - start_time

        if not bot.are_email_notifications_enabled:
            return

        # Send log
        message = """
    Bot: %s
    Number of follows: %d
    Number of tries: %d

    Start time: %s
    End time: %s
    Duration: %.2f seconds""" % (bot.username, len(follows), num_tries, start_time.isoformat(), end_time.isoformat(), duration.seconds)
        mail_admins(subject='Follow task completed by %s' % bot.username, message=message)


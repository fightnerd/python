from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from fightnerd.instagram_bots.models import Bot
from fightnerd.instagram_bots.models import SpeedType
from fightnerd.instagram_bots.models import FollowSourceType


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('username')
        parser.add_argument('password')
        parser.add_argument('-u', dest='usernames', action='append')

    def handle(self, *args, **options):
        bot = Bot.objects.create(
            username=options.get('username'),
            password=options.get('password'),
            speed=SpeedType.FAST,
            follow_source=FollowSourceType.FOLLOWINGS,
            unfollow_timeout=129600,
            usernames=options.get('usernames'),
            do_ignore_private_users=False
        )
        bot.bootstrap_follows()

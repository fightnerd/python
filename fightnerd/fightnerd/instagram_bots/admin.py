from __future__ import absolute_import

from django.contrib import admin
from fightnerd.instagram_bots.models import Bot


@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    list_display = (
        'username',
        'speed',
        'follow_source',
        'num_followers',
        'num_followings'
    )
    # readonly_fields = (
    #     'client_id',
    #     'client_secret',
    # )

    fieldsets = (
        ('Login Credentials', {
            'fields': (
                'username',
                'password',
            )
        }),
        ('Configuration', {
            'fields': (
                'is_enabled',
                'are_email_notifications_enabled',
                'speed',
                'do_ignore_private_users',
            )
        }),
        ('Follow', {
            'fields': (
                'usernames',
                'follow_source',
                'unfollow_timeout',
            )
        }),
    )

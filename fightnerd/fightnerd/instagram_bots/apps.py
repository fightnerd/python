from __future__ import absolute_import

from django.apps import AppConfig


class InstagramBotsAppConfig(AppConfig):
    name = 'fightnerd.instagram_bots'
    verbose_name = 'Instagram Bots'

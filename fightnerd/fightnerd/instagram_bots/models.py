from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
from selenium.webdriver.phantomjs.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from django.db import models
from django.conf import settings
from fabric.operations import prompt
from django.contrib.postgres.fields import ArrayField
from django_enumfield import enum
import re
import logging
import random
import requests
import time
from urlparse import urljoin
from django.utils import timezone
from datetime import timedelta
from django.core.mail import mail_admins
from celery.utils.log import get_task_logger
from instagram.client import InstagramAPI
from django.core.cache import caches
import hashlib
import simplejson
from instagram.models import User as InstagramUser
from scrapy.selector import Selector
from scrapy.http import HtmlResponse
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


cache = caches['instagram_bots']
logger = get_task_logger(__name__)

WEBSTA_BASE_URL = 'http://websta.me/n/'


class FollowSourceType(enum.Enum):
    FOLLOWERS = 0
    FOLLOWINGS = 1

    labels = {
        FOLLOWERS: 'Followers of usernames',
        FOLLOWINGS: 'Followings of usernames',
    }


class SpeedType(enum.Enum):
    SLOW = 0
    NORMAL = 1
    FAST = 2

    labels = {
        SLOW: 'Slow',
        NORMAL: 'Normal',
        FAST: 'Fast',
    }

    # Per hour
    follow = {
        SLOW: 14,
        NORMAL: 20,
        FAST: 26,
    }
    unfollow = {
        SLOW: 10,
        NORMAL: 15,
        FAST: 20,
    }


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(BaseModel):
    username = models.CharField(max_length=100)


class Follow(BaseModel):
    bot = models.ForeignKey('Bot')
    user = models.ForeignKey(User)
    is_unfollow = models.BooleanField(default=False)


class Bot(BaseModel):
    username = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    speed = enum.EnumField(SpeedType, blank=False, default=SpeedType.FAST, verbose_name='activity speed')

    is_enabled = models.BooleanField(default=True)
    are_email_notifications_enabled = models.BooleanField(default=True)

    num_followers = models.IntegerField(default=0)
    num_followings = models.IntegerField(default=0)

    usernames = ArrayField(models.CharField(max_length=100), default=list())
    follow_source = enum.EnumField(FollowSourceType, blank=False, default=FollowSourceType.FOLLOWERS, verbose_name='follow source')
    do_ignore_private_users = models.BooleanField(default=True)
    unfollow_timeout = models.IntegerField(default=129600, verbose_name='Unfollow timeout (s)')

    web_driver = None

    def clean(self):
        # Clean usernames
        usernames = list()
        for username in self.usernames:
            for t in re.split(r'[\s,]', username):
                if t:
                    usernames.append(t.strip())
        self.usernames = usernames

    def is_under_follow_speed_limit(self, num_follows):
        # cutoff_time = timezone.now() - timedelta(hours=1)
        # cutoff_time = cutoff_time + timedelta(minutes=7)
        # Follow.objects.filter(bot=self, is_unfollow=False, date_created__gte=cutoff_time).count()
        return num_follows < SpeedType.follow.get(self.speed)

    def is_under_unfollow_speed_limit(self, num_unfollows):
        return num_unfollows < SpeedType.unfollow.get(self.speed)

    def generate_follows(self, usernames, follow_source, timeout=timedelta(seconds=86400)):
        # Did webstagram_browser login...?
        self.web_driver = self._login()

        # Find the users
        for username in usernames:
            # Load profile page
            self.web_driver.get('http://websta.me/n/%s' % username)
            logger.info('Loaded %s...' % self.web_driver.current_url)
            time.sleep(2)

            # cache_key = '%s:%s:last_url' % ('user_follows' if follow_source == FollowSourceType.FOLLOWERS else 'user_followed_by', username)
            # last_url = cache.get(cache_key, None)

            # Click follow source link
            if follow_source == FollowSourceType.FOLLOWERS:
                next_url = self.web_driver.find_element_by_xpath('//*[@id="main"]/div[2]/div[3]/div/div[1]/div[1]/div/ul/li[2]/a').get_attribute('href')
            else:
                next_url = self.web_driver.find_element_by_xpath('//*[@id="main"]/div[2]/div[3]/div/div[1]/div[1]/div/ul/li[3]/a').get_attribute('href')

            # Start scrapin =D
            try:
                while next_url:
                    logger.info('Loading %s...' % next_url)
                    # Check cache before loading page
                    key = '%s:%s:%s' % ('user_follwers' if follow_source == FollowSourceType.FOLLOWERS else 'user_followings', username, next_url)
                    payload = cache.get(key, None)

                    if not payload:  # If names are not in cache for this url...
                        logger.info('Cache miss @ %s' % key)
                        self.web_driver.get(next_url)
                        logger.info('Loaded %s...' % self.web_driver.current_url)
                        time.sleep(2)

                        # Scrape names
                        names = list()
                        for li in self.web_driver.find_elements_by_xpath('//div[@id="followed"]/ul[@class="userlist"]/li'):
                            href = li.find_element_by_xpath('.//a').get_attribute('href')
                            name = href.split('/')[-1]
                            names.append(name)

                        # Get next url
                        try:
                            next_url = self.web_driver.find_element_by_xpath('//*[@id="followed"]/ul[2]/li[2]/a').get_attribute('href')
                        except:
                            next_url = None

                        # Save to cache
                        payload = {
                            'names': names,
                            'next_url': next_url
                        }
                        cache.set(key, simplejson.dumps(payload), timeout=timeout.total_seconds())
                    else:
                        logger.info('Cache hit @ %s' % key)
                        payload = simplejson.loads(payload)

                    # Yield...
                    for name in payload.get('names'):
                        # logger.info('Generating %s' % name)
                        yield name
                    next_url = payload.get('next_url', None)
            except GeneratorExit:
                pass
            except Exception:
                logger.exception('')

        return

    def bootstrap_follows(self):
        self.web_driver = None

        # Get list of followers (people who follow me)
        follows = list()
        for username in self.generate_follows([self.username], FollowSourceType.FOLLOWERS, timeout=timedelta(minutes=5)):
            if username == self.username:
                continue
            user, is_created = User.objects.get_or_create(username=username)
            follow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            follows.append(follow)
        self.num_followers = len(follows)
        self.save()
        if self.web_driver:
            self.web_driver.close()
        return follows

    def auto_unfollow(self, is_test_mode):
        self.web_driver = self._login()

        # Get list of followings (people who I follow)
        followings = list()
        for username in self.generate_follows([self.username], FollowSourceType.FOLLOWINGS, timeout=timedelta(minutes=5)):
            if username == self.username:
                continue
            user, is_created = User.objects.get_or_create(username=username)
            follow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            if is_created:
                logger.warn('Unlogged following found (%d, %s)!' % (follow.id, follow.user.username))

            # Determine if we should unfollow based on timeout
            unfollow_timeout = timedelta(seconds=self.unfollow_timeout)
            if timezone.now() - unfollow_timeout < follow.date_created:
                logger.info('Skipping %s due to timeout' % follow.user.username)
                continue
            followings.append(follow)
        self.num_followings = len(followings)

        # Get list of followers (people who follow me)
        followers = list()
        for username in self.generate_follows([self.username], FollowSourceType.FOLLOWERS, timeout=timedelta(minutes=5)):
            followers.append(username)
        self.num_followers = len(followers)
        self.save()

        # Start unfollowing people who don't follow back...
        unfollows = list()
        for follow in followings:
            user = follow.user
            # See if user is following this account
            if [f for f in followers if f == follow.user.username]:
                logger.info('%s is already following %s' % (user.username, self.username))
                continue

            # Load user's page
            logger.info('Loading page for %s' % follow.user.username)
            if not is_test_mode:
                self.web_driver.get(urljoin(WEBSTA_BASE_URL, follow.user.username))
                time.sleep(5)
                logger.info('Loaded %s' % self.web_driver.current_url)

                # Click unfollow...
                unfollow_button = self.web_driver.find_element_by_xpath('//*[@id="follow_btn_wrapper"]/button')
                if unfollow_button.lower() == 'following':
                    logger.info('Clicking <%s>' % unfollow_button.text)
                    unfollow_button.click()
                else:
                    logger.error('We are not following %s (<%s>)' % (follow.user.username, unfollow_button.text))

            unfollow = Follow.objects.create(bot=self, user=user, is_unfollow=True)
            unfollows.append(unfollow)
            logger.info('Unfollowing %s' % unfollow.user.username)

            if not self.is_under_unfollow_speed_limit(len(unfollows)):
                break

            if not is_test_mode:
                sleep_duration = random.randrange(10, 20)
                logger.info('Sleeping for %d seconds to look more human' % sleep_duration)
                time.sleep(sleep_duration)
        self.web_driver.quit()
        return unfollows

    def auto_follow(self, is_test_mode):
        self.web_driver = self._login()

        # Visit each follow's page
        follows = list()
        for username in self.generate_follows(self.usernames, self.follow_source):
            if username == self.username:
                continue

            if Follow.objects.filter(user__username=username).exists():  # Make sure this follow is unique
                logger.info('Ignoring %s' % username)
                continue

            # Load user's page
            is_new_follow = True
            is_follow_requested = False
            try:
                logger.info('Loading page for %s' % username)
                if not is_test_mode:
                    self.web_driver.get(urljoin(WEBSTA_BASE_URL, username))
                    time.sleep(5)
                    logger.info('Loaded %s' % self.web_driver.current_url)

                    # Click follow...
                    follow_button = self.web_driver.find_element_by_xpath('//*[@id="follow_btn_wrapper"]/button')
                    is_new_follow = follow_button.text.lower() == 'follow'
                    is_follow_requested = follow_button.text.lower() == 'requested'
                    if is_new_follow:
                        logger.info('Clicking <%s>' % follow_button.text)
                        follow_button.click()
                    elif is_follow_requested:
                        logger.info('Already requested a follow for %s' % username)
                    else:
                        logger.warning('We are already following %s (%s)' % (username, follow_button.text))
                        continue
            except:
                logger.exception('Failed to process page for %s' % username)
                continue

            user, is_created = User.objects.get_or_create(username=username)
            follow, is_created = Follow.objects.get_or_create(bot=self, user=user, is_unfollow=False)
            if is_new_follow:
                follows.append(follow)
            logger.info('Following %s' % follow.user.username)

            if not self.is_under_follow_speed_limit(len(follows)):
                break

            if not is_test_mode:
                sleep_duration = random.randrange(10, 20)
                logger.info('Sleeping for %d seconds to look more human' % sleep_duration)
                if is_new_follow:
                    time.sleep(sleep_duration)

        self.web_driver.quit()
        logger.info('Followed %d users!' % len(follows))
        return follows

    def _login(self):
        if not self.web_driver:
            desired_capabilities = DesiredCapabilities.PHANTOMJS.copy()
            # desired_capabilities['phantomjs.page.settings.userAgent'] = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0'
            web_driver = WebDriver(desired_capabilities=desired_capabilities)
            web_driver.get('http://websta.me/login')
            logger.info('Loaded %s...' % web_driver.current_url)
            time.sleep(5)

            web_driver.find_element_by_xpath('//*[@id="id_username"]').send_keys(self.username)
            web_driver.find_element_by_xpath('//*[@id="id_password"]').send_keys(self.password)
            web_driver.find_element_by_xpath('//*[@id="login-form"]/p[3]/input').click()
            time.sleep(2)
            logger.info('Loaded %s...' % web_driver.current_url)
            self.web_driver = web_driver
        return self.web_driver

    @staticmethod
    def get_follow_source_label(follow_source):
        return 'following' if follow_source == FollowSourceType.FOLLOWINGS else 'follower'

    @property
    def follow_source_label(self):
        return 'following' if self.follow_source == FollowSourceType.FOLLOWINGS else 'follower'

from __future__ import absolute_import

import logging

from fightnerd.celeryapp import app
from fightnerd.instagram_bots.models import Bot
from django.core import management

logger = logging.getLogger(__name__)


class TaskType(object):
    FOLLOW = 0
    UNFOLLOW = 1


@app.task
def run_bot(bot_id, task):
    bot = Bot.objects.get(id=bot_id)
    if task == TaskType.FOLLOW:
        management.call_command('instagram_bot_follow', '--notest', bot.username)
    elif task == TaskType.UNFOLLOW:
        management.call_command('instagram_bot_unfollow', '--notest', bot.username)


@app.task
def run_follow_bots():
    for bot in Bot.objects.filter(is_enabled=True):
        run_bot.delay(bot.id, TaskType.FOLLOW)


@app.task
def run_unfollow_bots():
    for bot in Bot.objects.filter(is_enabled=True):
        run_bot.delay(bot.id, TaskType.UNFOLLOW)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppleDevice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('token', models.CharField(unique=True, max_length=200)),
            ],
            options={
                'verbose_name': 'Apple Device',
                'verbose_name_plural': 'Apple Devices',
            },
        ),
        migrations.CreateModel(
            name='GoogleCloudMessagingProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('registration_id', models.CharField(unique=True, max_length=200)),
            ],
            options={
                'verbose_name': 'GCM Profile',
                'verbose_name_plural': 'GCM Profiles',
            },
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('fire_date', models.DateTimeField()),
                ('did_fire', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=200)),
                ('body', models.TextField()),
                ('ticker', models.CharField(max_length=200)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

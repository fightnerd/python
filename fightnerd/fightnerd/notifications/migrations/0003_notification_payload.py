# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0002_auto_20160119_2326'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='payload',
            field=models.TextField(default=''),
        ),
    ]

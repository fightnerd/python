from fightnerd.notifications.models import AppleDevice
from rest_framework import mixins
from rest_framework import viewsets
from rest_framework import serializers


class AppleDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppleDevice
        fields = ('token',)


class AppleDeviceViewSet(mixins.CreateModelMixin,
                         viewsets.GenericViewSet):
    queryset = AppleDevice.objects.all()
    serializer_class = AppleDeviceSerializer

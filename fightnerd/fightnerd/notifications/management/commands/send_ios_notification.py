from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from apns import Payload, APNs
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.management.base import BaseCommand
from fightnerd.notifications.models import AppleDevice


logger = get_task_logger(__name__)
APNS_CERTIFICATE_FILE_PATH = getattr(settings, 'APNS_CERTIFICATE_FILE_PATH')
APNS_CERTIFICATE_AUTHORITY_FILE_PATH = getattr(settings, 'APNS_CERTIFICATE_AUTHORITY_FILE_PATH')
APNS_PRIVATE_KEY_FILE_PATH = getattr(settings, 'APNS_PRIVATE_KEY_FILE_PATH')
APNS_USE_SANDBOX = getattr(settings, 'APNS_USE_SANDBOX')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('-t', '--device_token', dest='device_tokens', action='append')
        parser.add_argument('alert')

    def handle(self, *args, **options):
        device_tokens = options.get('device_tokens')
        if not device_tokens:
            device_tokens = [a.token for a in AppleDevice.objects.all()]

        def response_listener(error_response):
            logger.debug("client get error-response: " + str(error_response))

        apns = APNs(use_sandbox=APNS_USE_SANDBOX, cert_file=APNS_CERTIFICATE_FILE_PATH, key_file=APNS_PRIVATE_KEY_FILE_PATH, enhanced=True)
        apns.gateway_server.register_response_listener(response_listener)
        payload = Payload(alert=options.get('alert'), sound="default", badge=1)
        for device_token in device_tokens:
            apns.gateway_server.send_notification(device_token, payload)

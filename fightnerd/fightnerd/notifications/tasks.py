from fightnerd.notifications.models import Notification
from fightnerd.celeryapp import app
from django.utils import timezone


@app.task()
def fire_notifications():
    for notification in Notification.objects.filter(did_fire=False):
        if notification.fire_date < timezone.now():
            notification.fire()

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from django.db import models
import requests
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import HStoreField
from apns import APNs, Frame, Payload
import simplejson
import time
from django.conf import settings
import random

logger = logging.getLogger(__name__)


# Yay Google...?
GOOGLE_CLOUD_MESSAGING_SENDER_ID = '27161842841'
GOGGLE_CLOUD_MESSAGING_API_KEY = 'AIzaSyAANiWfpvq-5PG-fnctbkIfr46ZItzIugA'
GOGGLE_CLOUD_MESSAGING_MAX_RECIPIENTS = 1000
GOOGLE_CLOUD_MESSAGING_ENDPOINT = 'https://android.googleapis.com/gcm/send'
APNS_CERTIFICATE_FILE_PATH = getattr(settings, 'APNS_CERTIFICATE_FILE_PATH')
APNS_CERTIFICATE_AUTHORITY_FILE_PATH = getattr(settings, 'APNS_CERTIFICATE_AUTHORITY_FILE_PATH')
APNS_PRIVATE_KEY_FILE_PATH = getattr(settings, 'APNS_PRIVATE_KEY_FILE_PATH')
APNS_USE_SANDBOX = getattr(settings, 'APNS_USE_SANDBOX')


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class AppleDevice(BaseModel):
    token = models.CharField(max_length=200, unique=True, blank=False)

    class Meta:
        verbose_name = 'Apple Device'
        verbose_name_plural = 'Apple Devices'


class GoogleCloudMessagingProfile(BaseModel):
    registration_id = models.CharField(max_length=200, unique=True, blank=False)

    def __unicode__(self):
        return self.registration_id

    def save(self, *args, **kwargs):
        super(GoogleCloudMessagingProfile, self).full_clean()
        super(GoogleCloudMessagingProfile, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'GCM Profile'
        verbose_name_plural = 'GCM Profiles'


class Notification(BaseModel):
    fire_date = models.DateTimeField(null=False, blank=False)
    did_fire = models.BooleanField(default=False)
    title = models.CharField(max_length=200)
    body = models.TextField()
    ticker = models.CharField(max_length=200)
    payload = models.TextField(default='')

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return '%s <%s>' % (self.fire_date, self.did_fire)

    def fire_apple_notification(self):
        def response_listener(error_response):
            logger.debug('client get error-response: ' + str(error_response))

        apns = APNs(use_sandbox=APNS_USE_SANDBOX, cert_file=APNS_CERTIFICATE_FILE_PATH, key_file=APNS_PRIVATE_KEY_FILE_PATH, enhanced=True)
        apns.gateway_server.register_response_listener(response_listener)

        custom_payload = simplejson.loads(self.payload) if self.payload and len(self.payload) < 2000 else {}
        payload = Payload(
            alert=self.ticker,
            sound='default',
            badge=1,
            custom=custom_payload
        )

        device_tokens = [a.token for a in AppleDevice.objects.all()]
        for device_token in device_tokens:
            apns.gateway_server.send_notification(
                token_hex=device_token,
                payload=payload,
                identifier=random.getrandbits(32)
            )

    def fire(self):
        self.fire_apple_notification()
        self.did_fire = True
        self.save()

    def fire_android_notification(self):
        all_recipients = []
        for gcm_profile in GoogleCloudMessagingProfile.objects.all():
            all_recipients.append(gcm_profile.registration_id)

        gcm_profiles_to_remove = []
        canonical_ids = []
        headers = {'Content-Type': 'application/json',
                   'Authorization': 'key=%s' % GOGGLE_CLOUD_MESSAGING_API_KEY}
        payload = {}
        for recipients in (all_recipients[i:i + GOGGLE_CLOUD_MESSAGING_MAX_RECIPIENTS] for i in
                           xrange(0, len(all_recipients), GOGGLE_CLOUD_MESSAGING_MAX_RECIPIENTS)):
            payload['registration_ids'] = recipients
            payload['dry_run'] = True
            payload['data'] = {}
            payload['data']['title'] = self.title
            payload['data']['body'] = self.body
            payload['data']['ticker'] = self.ticker
            response = requests.post(GOOGLE_CLOUD_MESSAGING_ENDPOINT, data=json.dumps(payload), headers=headers)

            response_json = response.json()
            if response_json:
                logger.info(response_json)
                if response_json['canonical_ids'] > 0:
                    for recipient, result in zip(recipients, response_json['results']):
                        if 'registration_id' in result:
                            canonical_ids.append(result['registration_id'])
                            try:
                                gcm_profile = GoogleCloudMessagingProfile.objects.get(registrationId=recipient)
                                gcm_profiles_to_remove.append(gcm_profile)
                            except:
                                pass

            if response.status_code != requests.codes.get('ok') or response_json['success'] <= 0:
                logger.error('Failed to POST notification to GCM!')
                raise RuntimeError('Failed to POST notification to GCM!')

        # Clean up duplicates
        for gcm_profile in gcm_profiles_to_remove:
            logger.info('Removing %s' % gcm_profile)
            gcm_profile.delete()
        for canonical_id in canonical_ids:
            gcm_profile, created = GoogleCloudMessagingProfile.objects.get_or_create(registrationId=canonical_id)
            logger.info('Adding %s' % gcm_profile)

        message = 'Sent notification with payload %s to %d Android user%s' % (
            payload['data'],
            len(all_recipients),
            "s" if len(all_recipients) > 1 else ""
        )
        logger.info(message)

        self.did_fire = True
        self.save()

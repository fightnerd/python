from datetime import datetime

from django.contrib import admin
from fightnerd.notifications.models import GoogleCloudMessagingProfile
from fightnerd.notifications.models import Notification


class FutureNotificationListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = ('fire date')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'isFuture'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('is_future', ('in the future')),
            ('is_past', ('in the past')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'is_future':
            return queryset.filter(fire_date__gte=datetime.utcnow())
        if self.value() == 'is_past':
            return queryset.filter(fire_date__lt=datetime.utcnow())


@admin.register(Notification)
class Notification(admin.ModelAdmin):
    ordering = ('fire_date',)
    list_filter = ('did_fire', FutureNotificationListFilter)


admin.site.register(GoogleCloudMessagingProfile, admin.ModelAdmin)

import urllib

from django.core.urlresolvers import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from fightnerd.notifications.models import AppleDevice
from fightnerd.publications.utils import create_articles
from django.core.management import call_command
from fightnerd.publications.models import Article
from fightnerd.publications.models import Video
from fightnerd.publications.models import Author
from fightnerd.publications.models import Publication
from fightnerd.publications.utils import create_articles
from fightnerd.publications.utils import create_videos
from fightnerd.publications.utils import tear_down_articles
from dateutil.parser import parse as parse_date
from haystack.query import SearchQuerySet
import simplejson


class AppleDeviceRetrievalAPITestCase(APITestCase):
    def test_list(self):
        url = reverse('api_notifications:apple_device_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class AppleDeviceCreationAPITestCase(APITestCase):
    def setUp(self):
        self.data = {
            'token': 'HelloToken!'
        }

    def tearDown(self):
        AppleDevice.objects.all().delete()

    def test_create_apple_device(self):
        url = reverse('api_notifications:apple_device_list')
        response = self.client.post(url, data=self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AppleDevice.objects.all().count(), 1)
        self.assertEqual(AppleDevice.objects.filter(token=self.data.get('token')).count(), 1)

    def test_create_duplicate(self):
        url = reverse('api_notifications:apple_device_list')
        response = self.client.post(url, data=self.data, format='json')
        for i in range(10):
            response = self.client.post(url, data=self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(AppleDevice.objects.all().count(), 1)
        self.assertEqual(AppleDevice.objects.filter(token=self.data.get('token')).count(), 1)

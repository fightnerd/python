# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fighters', '0001_initial'),
        ('notifications', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='notifications',
            field=models.ManyToManyField(to='notifications.Notification'),
        ),
        migrations.AddField(
            model_name='event',
            name='organization',
            field=models.ForeignKey(related_name='events', to='fighters.Organization', null=True),
        ),
        migrations.AddField(
            model_name='corner',
            name='fight',
            field=models.ForeignKey(to='fighters.Fight'),
        ),
        migrations.AddField(
            model_name='corner',
            name='fighter',
            field=models.ForeignKey(to='fighters.Fighter'),
        ),
        migrations.AlterUniqueTogether(
            name='fighter',
            unique_together=set([('full_name', 'sherdog_id')]),
        ),
        migrations.AlterUniqueTogether(
            name='corner',
            unique_together=set([('fight', 'fighter')]),
        ),
    ]

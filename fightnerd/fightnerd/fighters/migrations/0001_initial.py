# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import fightnerd.fighters.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Association',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(unique=True, max_length=50, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Corner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('odds', models.IntegerField(null=True, blank=True)),
                ('is_winner', models.BooleanField()),
                ('order', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(db_index=True, max_length=100, blank=True)),
                ('date', models.DateTimeField(db_index=True, null=True, blank=True)),
                ('sherdog_id', models.CharField(max_length=50, blank=True)),
                ('guid', models.CharField(db_index=True, unique=True, max_length=50, blank=True)),
                ('location', models.CharField(max_length=200, blank=True)),
                ('title', models.CharField(db_index=True, max_length=100, blank=True)),
                ('subtitle', models.CharField(db_index=True, max_length=100, blank=True)),
                ('is_finalized', models.BooleanField(default=False)),
                ('is_time_valid', models.BooleanField(default=False, db_index=True)),
                ('is_prime', models.BooleanField(default=False, db_index=True)),
                ('is_upcoming', models.BooleanField(default=False, db_index=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Fight',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('result', models.CharField(max_length=50, blank=True)),
                ('method', models.CharField(max_length=100, blank=True)),
                ('round', models.IntegerField(null=True, blank=True)),
                ('time', models.IntegerField(null=True, blank=True)),
                ('type', models.IntegerField(default=0, db_index=True)),
                ('order', models.IntegerField(null=True, blank=True)),
                ('event', models.ForeignKey(related_name='fights', to='fighters.Event')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Fighter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('full_name', models.CharField(db_index=True, max_length=100, blank=True)),
                ('sherdog_id', models.CharField(max_length=50, blank=True)),
                ('guid', models.CharField(db_index=True, unique=True, max_length=50, blank=True)),
                ('nickname', models.CharField(max_length=50, blank=True)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('height', models.FloatField(null=True, blank=True)),
                ('weight', models.FloatField(null=True, blank=True)),
                ('weight_class', models.IntegerField(default=14, db_index=True)),
                ('wins_by_knockout', models.IntegerField(null=True, blank=True)),
                ('wins_by_submission', models.IntegerField(null=True, blank=True)),
                ('wins_by_decision', models.IntegerField(null=True, blank=True)),
                ('wins_by_other', models.IntegerField(null=True, blank=True)),
                ('losses_by_knockout', models.IntegerField(null=True, blank=True)),
                ('losses_by_submission', models.IntegerField(null=True, blank=True)),
                ('losses_by_decision', models.IntegerField(null=True, blank=True)),
                ('losses_by_other', models.IntegerField(null=True, blank=True)),
                ('draws', models.IntegerField(null=True, blank=True)),
                ('no_contests', models.IntegerField(null=True, blank=True)),
                ('image', models.ImageField(blank=True, upload_to=b'fighters/', validators=[fightnerd.fighters.models.validate_image])),
                ('city', models.CharField(max_length=50, blank=True)),
                ('state', models.CharField(max_length=50, blank=True)),
                ('country', models.CharField(max_length=50, blank=True)),
                ('associations', models.ManyToManyField(to='fighters.Association')),
                ('fights', models.ManyToManyField(to='fighters.Fight')),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(unique=True, max_length=200)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Ranking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('guid', models.CharField(db_index=True, unique=True, max_length=50, blank=True)),
                ('rank', models.IntegerField()),
                ('rank_delta', models.IntegerField(default=0)),
                ('weight_class', models.IntegerField(default=14, db_index=True)),
                ('not_previously_ranked', models.BooleanField(default=False, db_index=True)),
                ('is_champion', models.BooleanField(default=False, db_index=True)),
                ('is_interim_champion', models.BooleanField(default=False, db_index=True)),
                ('fighter', models.ForeignKey(to='fighters.Fighter')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Referee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('full_name', models.CharField(unique=True, max_length=200)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='fight',
            name='fighters',
            field=models.ManyToManyField(to='fighters.Fighter', through='fighters.Corner'),
        ),
        migrations.AddField(
            model_name='fight',
            name='referee',
            field=models.ForeignKey(blank=True, to='fighters.Referee', null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fighters', '0002_auto_20160119_2256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='notifications',
        ),
    ]

from __future__ import absolute_import

from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Fight
from fightnerd.fighters.models import Association
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Referee
from fightnerd.fighters.models import UpcomingEvent
from fightnerd.fighters.models import UpcomingFight
from fightnerd.fighters.models import Ranking
from rest_framework import serializers


class AssociationSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='association-detail')

    class Meta:
        model = Association
        exclude = ('date_modified', 'date_created')


class FighterListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='fighter-detail')

    class Meta:
        model = Fighter
        exclude = ('associations', 'fights', 'date_modified', 'date_created')


class EventSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='event-detail')

    class Meta:
        model = Event
        exclude = ('date_modified', 'date_created')


class RefereeSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='referee-detail')

    class Meta:
        model = Referee
        exclude = ('date_modified', 'date_created')


class FightSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='fight-detail')
    event = EventSerializer(required=True)
    referee = RefereeSerializer(required=False)
    first_fighter = FighterListSerializer(required=True)
    second_fighter = FighterListSerializer(required=True)

    class Meta:
        model = Fight
        exclude = ('date_modified', 'date_created')


class FighterDetailSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='fighter-detail')
    associations = AssociationSerializer(many=True)
    fights = FightSerializer(many=True)

    class Meta:
        model = Fighter
        exclude = ('date_modified', 'date_created')


class UpcomingFightSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='upcomingfight-detail')
    first_fighter = FighterListSerializer(required=True)
    second_fighter = FighterListSerializer(required=True)

    class Meta:
        model = UpcomingFight
        fields = ('url', 'first_fighter', 'second_fighter', 'order', 'first_fighter_odds', 'second_fighter_odds')
        # exclude = ('date_modified', 'date_created')


class UpcomingEventListSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='upcomingevent-detail')
    fights = serializers.SerializerMethodField()

    def get_fights(self, obj):
        main_event = UpcomingFight.objects.get(event=obj, order__exact=0)
        upcoming_fight_serializer = UpcomingFightSerializer(main_event, context=self.context)
        return [upcoming_fight_serializer.data]

    class Meta:
        model = UpcomingEvent
        exclude = ('date_modified', 'date_created', 'organization', 'notifications', 'location')


class UpcomingEventDetailSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='upcomingevent-detail')
    fights = UpcomingFightSerializer(many=True)

    class Meta:
        model = UpcomingEvent
        exclude = ('date_modified', 'date_created', 'organization', 'notifications', 'location')


class RankingSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='ranking-detail')
    fighter = FighterListSerializer(required=True)

    class Meta:
        model = Ranking
        exclude = ('date_modified', 'date_created')

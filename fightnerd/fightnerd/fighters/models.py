import datetime
from datetime import timedelta

from django.db import models
from django.core.files.storage import FileSystemStorage
import re
import pytz
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_save
from django.db.models.signals import post_save
from django.utils import timezone
from fightnerd.notifications.models import Notification
from django_enumfield import enum
from django.db.models import Q
import hashlib
import base64
from datetime import date
from django.contrib.contenttypes.models import ContentType
from djangorestframework_camel_case.render import CamelCaseJSONRenderer
from rest_framework.request import Request
from django.http import HttpRequest
from django.core.cache import cache


IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')
API_DOMAIN = getattr(settings, 'API_DOMAIN')
API_PROTOCOL = getattr(settings, 'API_PROTOCOL')


class FightType(enum.Enum):
    AMATEUR = 0
    PROFESSIONAL = 1

    labels = {
        AMATEUR: 'Amateur',
        PROFESSIONAL: 'Professional'
    }


class WeightClassType(enum.Enum):
    ATOMWEIGHT = 0
    STRAWWEIGHT = 1
    FLYWEIGHT = 2
    BANTAMWEIGHT = 3
    FEATHERWEIGHT = 4
    LIGHTWEIGHT = 5
    WELTERWEIGHT = 6
    MIDDLEWEIGHT = 7
    LIGHT_HEAVYWEIGHT = 8
    HEAVYWEIGHT = 9
    SUPER_HEAVYWEIGHT = 10
    POUND_FOR_POUND = 11
    WOMENS_BANTAMWEIGHT = 12
    WOMENS_STRAWWEIGHT = 13
    UNKNOWN = 14

    labels = {
        ATOMWEIGHT: 'Atomweight',
        STRAWWEIGHT: 'Strawweight',
        FLYWEIGHT: 'Flyweight',
        BANTAMWEIGHT: 'Bantamweight',
        FEATHERWEIGHT: 'Featherweight',
        LIGHTWEIGHT: 'Lightweight',
        WELTERWEIGHT: 'Welterweight',
        MIDDLEWEIGHT: 'Middleweight',
        LIGHT_HEAVYWEIGHT: 'Light Heavyweight',
        HEAVYWEIGHT: 'Heavyweight',
        SUPER_HEAVYWEIGHT: 'Super Heavyweight',
        POUND_FOR_POUND: 'Pound-For-Pound',
        WOMENS_BANTAMWEIGHT: 'Women\'s Bantamweight',
        WOMENS_STRAWWEIGHT: 'Women\'s Strawweight',
        UNKNOWN: 'Unknown',
    }

    @classmethod
    def ranking_choices(cls):
        return [c for c in cls.choices() if c[0] not in [0, 1, 10, 14]]


def validate_image(obj):
    if obj.file.size > IMAGE_SIZE_LIMIT_IN_MEGABYTES * 1024 * 1024:
        raise ValidationError('Maximum image file size is %d MB' % IMAGE_SIZE_LIMIT_IN_MEGABYTES)


IMAGE_VALIDATORS = [validate_image]


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class Organization(BaseModel):
    prime_organizations = ['ultimate fighting championship',
                           'bellator mma',
                           'invicta fighting championships',
                           'world series of fighting']
    name = models.CharField(max_length=200, unique=True)

    def __unicode__(self):
        return self.name

    def is_prime(self):
        return self.name.lower() in self.prime_organizations


class Event(BaseModel):
    name = models.CharField(max_length=100, blank=True, db_index=True)
    date = models.DateTimeField(null=True, blank=True, db_index=True)
    sherdog_id = models.CharField(max_length=50, blank=True)
    guid = models.CharField(max_length=50, blank=True, db_index=True, unique=True)

    location = models.CharField(max_length=200, blank=True)
    title = models.CharField(max_length=100, blank=True, db_index=True)
    subtitle = models.CharField(max_length=100, blank=True, db_index=True)
    is_finalized = models.BooleanField(db_index=False, default=False)
    is_time_valid = models.BooleanField(db_index=True, default=False)
    is_prime = models.BooleanField(db_index=True, default=False)
    is_upcoming = models.BooleanField(db_index=True, default=False)

    organization = models.ForeignKey(Organization, related_name='events', null=True, db_index=True)

    def __unicode__(self):
        return '%s' % self.name

    @property
    def main_fight(self):
        return Fight.objects.get(event=self, order=0)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        if not instance.is_finalized:
            instance.title = instance.get_title()
            instance.subtitle = instance.get_subtitle()

        # Set guid
        instance.guid = hashlib.md5(str(instance.name) + str(instance.sherdog_id) + str(instance.is_upcoming)).hexdigest()

        # Set is prime
        if instance.organization:
            try:
                instance.is_prime = instance.organization.is_prime() and 'tba' not in instance.name.lower()
            except Organization.DoesNotExist:
                instance.is_prime = False
        else:
            instance.is_prime = False

    @staticmethod
    def post_save(sender, instance, **kwargs):
        if instance.is_upcoming:
            instance.create_notifications()

    def get_fight_by_last_names(self, first_fighter_last_name, second_fighter_last_name):
        first_fighter_last_name = first_fighter_last_name.strip()
        second_fighter_last_name = second_fighter_last_name.strip()
        if not first_fighter_last_name:
            return None
        if not second_fighter_last_name:
            return None
        qs = Corner.objects.filter(fight__event=self).filter(Q(fighter__full_name__iendswith=first_fighter_last_name) | Q(fighter__full_name__iendswith=second_fighter_last_name))
        return qs[0].fight if qs.count() == 2 else None

    def get_title(self):
        if self.organization:
            try:
                toks = re.split(' - ', self.name)
                if 'Ultimate Fighting Championship' in self.organization.name:
                    return toks[0]
                elif 'Bellator MMA' in self.organization.name:
                    return toks[1] if len(toks) > 1 else self.name
                elif 'Invicta Fighting Championships' in self.organization.name:
                    return toks[0]
                elif 'World Series of Fighting' == self.organization.name:
                    return 'World Series of Fighting ' + re.split(' ', toks[0])[1]
                else:
                    return self.name
            except Organization.DoesNotExist:
                pass
        return self.name

    def get_subtitle(self):
        try:
            main_event = Fight.objects.get(event=self, order=0)
        except Fight.DoesNotExist:
            return ''
        try:
            first_fighter = Corner.objects.get(fight=main_event, order=0).fighter
            second_fighter = Corner.objects.get(fight=main_event, order=1).fighter
            return '%s vs. %s' % (first_fighter.truncated_last_name, second_fighter.truncated_last_name)
        except Corner.DoesNotExist:
            return ''

    def create_notification(self, fire_date, body):
        from fightnerd.fighters.api import EventAppleNotificationSerializer

        title = '%s: %s' % (self.title, self.subtitle)
        ticker = '%s %s' % (title, body)

        # Payload
        http_request = HttpRequest()
        http_request.META['HTTP_HOST'] = API_DOMAIN
        serializer = EventAppleNotificationSerializer(instance=self, context={'request': Request(http_request)})
        rendered_data = CamelCaseJSONRenderer().render({'event': serializer.data})

        notification = Notification.objects.create(
            fire_date=fire_date,
            title=title,
            body=body,
            ticker=ticker,
            payload=rendered_data,
            content_object=self,
            object_id=self.id,
            content_type=ContentType.objects.get_for_model(self)
        )
        return notification

    def create_notifications(self):
        if not self.date or not self.is_prime:
            return

        # Delete all notifications
        Notification.objects.filter(
            content_type=ContentType.objects.get_for_model(self),
            object_id=self.id
        ).delete()

        eastern_time = self.date.astimezone(pytz.timezone('US/Eastern')).strftime('%I:%M%p ET').lstrip('0').replace(':00', '')
        pacific_time = self.date.astimezone(pytz.timezone('US/Pacific')).strftime('%I:%M%p PT').lstrip('0').replace(':00', '')

        # Create notification for NOW!!!
        if self.is_time_valid:
            fire_date = self.date
            self.create_notification(fire_date, 'is starting now!')

        # Create notification for 8 hours before!!!
        if self.is_time_valid:
            fire_date = self.date - timedelta(hours=8)
            self.create_notification(fire_date, 'starts today at %s/%s' % (pacific_time, eastern_time))


class Referee(BaseModel):
    full_name = models.CharField(max_length=200, unique=True)

    def __unicode__(self):
        return self.full_name


class Fight(BaseModel):
    result = models.CharField(max_length=50, blank=True)
    method = models.CharField(max_length=100, blank=True)
    round = models.IntegerField(null=True, blank=True)
    time = models.IntegerField(null=True, blank=True)  # Time will be in seconds...
    type = enum.EnumField(FightType, blank=False, default=FightType.AMATEUR, db_index=True)
    order = models.IntegerField(null=True, blank=True)

    event = models.ForeignKey(Event, related_name='fights')
    referee = models.ForeignKey(Referee, null=True, blank=True)
    fighters = models.ManyToManyField('Fighter', through='Corner')

    def __unicode__(self):
        corners = Corner.objects.filter(fight=self).order_by('order')
        if corners.count() > 0:
            return '%s vs %s' % (corners[0].fighter.full_name, corners[1].fighter.full_name)
        else:
            return ''


class Corner(BaseModel):
    fight = models.ForeignKey('Fight')
    fighter = models.ForeignKey('Fighter')
    odds = models.IntegerField(null=True, blank=True)
    is_winner = models.BooleanField()
    order = models.IntegerField()

    class Meta:
        unique_together = ('fight', 'fighter')


class Association(BaseModel):
    name = models.CharField(max_length=50, blank=True, unique=True)

    def __unicode__(self):
        return "%s" % self.name


class FighterQuerySet(models.QuerySet):
    def prime(self):
        cutoff_date = timezone.now() - timedelta(days=3 * 365)
        return self.filter(fights__event__date__gte=cutoff_date, fights__type=FightType.PROFESSIONAL) \
            .filter(
            Q(fights__event__name__icontains='ufc ') |
            Q(fights__event__name__icontains='strikeforce ') |
            Q(fights__event__name__icontains='bellator ') |
            Q(fights__event__name__icontains='invicta ') |
            Q(fights__event__name__icontains='wsof ')
        ).distinct()


class Fighter(BaseModel):
    full_name = models.CharField(max_length=100, blank=True, db_index=True)
    sherdog_id = models.CharField(max_length=50, blank=True)
    guid = models.CharField(max_length=50, blank=True, db_index=True, unique=True)

    nickname = models.CharField(max_length=50, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    height = models.FloatField(null=True, blank=True)  # in centimeters
    weight = models.FloatField(null=True, blank=True)  # in kilograms
    weight_class = enum.EnumField(WeightClassType, blank=False, default=WeightClassType.UNKNOWN, db_index=True)
    wins_by_knockout = models.IntegerField(null=True, blank=True)
    wins_by_submission = models.IntegerField(null=True, blank=True)
    wins_by_decision = models.IntegerField(null=True, blank=True)
    wins_by_other = models.IntegerField(null=True, blank=True)
    losses_by_knockout = models.IntegerField(null=True, blank=True)
    losses_by_submission = models.IntegerField(null=True, blank=True)
    losses_by_decision = models.IntegerField(null=True, blank=True)
    losses_by_other = models.IntegerField(null=True, blank=True)
    draws = models.IntegerField(null=True, blank=True)
    no_contests = models.IntegerField(null=True, blank=True)
    image = models.ImageField(upload_to='fighters/',
                              blank=True,
                              validators=IMAGE_VALIDATORS)

    city = models.CharField(max_length=50, blank=True)
    state = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)

    # Foreigners...
    associations = models.ManyToManyField(Association)
    fights = models.ManyToManyField(Fight)

    objects = FighterQuerySet.as_manager()

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5((instance.full_name + instance.sherdog_id).encode('utf8')).hexdigest()

    def __unicode__(self):
        return self.full_name

    @property
    def first_name(self):
        toks = self.full_name.split(' ')
        return toks[0] if len(toks) > 0 else None

    @property
    def last_name(self):
        toks = self.full_name.split(' ')
        return ' '.join(toks[1:]) if len(toks) > 0 else None

    @property
    def age(self):
        return int((date.today() - self.date_of_birth).days / 365) if self.date_of_birth else None

    @property
    def wins(self):
        return self.wins_by_knockout + self.wins_by_submission + self.wins_by_other + self.wins_by_decision

    @property
    def losses(self):
        return self.losses_by_knockout + self.losses_by_submission + self.losses_by_decision + self.losses_by_other

    @property
    def weight_class_label(self):
        return WeightClassType.label(self.weight_class)

    @property
    def last_name(self):
        toks = self.full_name.split()
        return ' '.join(toks[1:]) if len(toks) > 1 else self.full_name

    @property
    def truncated_last_name(self):
        last_name = self.last_name
        toks = re.split('\s', last_name)
        if len(toks) < 3:
            return last_name
        else:
            return toks[-1]

    @property
    def is_prime(self):
        # return True
        key = 'fighters.Fighter.%s.is_prime' % self.guid
        is_prime = cache.get(key)
        if is_prime is None:
            is_prime = Fighter.objects.filter(id=self.id).prime().exists()
            cache.set(key, is_prime, datetime.timedelta(days=90).total_seconds())
        return is_prime
        # return Fighter.objects.prime().filter(id=self.id).exists()
        # cutoff_date = timezone.now() - timedelta(days=3 * 365)
        # return self.fights.filter(event__date__gte=cutoff_date, type=FightType.PROFESSIONAL)\
        #     .exclude(event__name__icontains='wufc')\
        #     .exclude(event__name__icontains='universal')\
        #     .filter(Q(event__name__icontains='ufc ') | Q(event__name__icontains='strikeforce ') | Q(event__name__icontains='bellator ') | Q(event__name__icontains='invicta ') | Q(event__name__icontains='wsof ')).exists()

    @property
    def image_data(self):
        if not self.image:
            return None
        str = base64.b64encode(self.image.read())
        return str

    class Meta:
        unique_together = ('full_name', 'sherdog_id')


class Ranking(BaseModel):
    guid = models.CharField(max_length=50, blank=True, db_index=True, unique=True)
    fighter = models.ForeignKey('Fighter')
    rank = models.IntegerField()
    rank_delta = models.IntegerField(default=0)
    weight_class = enum.EnumField(WeightClassType, blank=False, default=WeightClassType.UNKNOWN, db_index=True)
    not_previously_ranked = models.BooleanField(default=False, db_index=True)
    is_champion = models.BooleanField(default=False, db_index=True)
    is_interim_champion = models.BooleanField(default=False, db_index=True)

    @property
    def weight_class_label(self):
        return WeightClassType.label(self.weight_class)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        instance.guid = hashlib.md5((str(instance.rank) + str(WeightClassType.label(instance.weight_class)) + str(instance.is_champion) + str(instance.is_interim_champion)).encode('utf8')).hexdigest()

    def __unicode__(self):
        return '%d, %s, %s' % (self.rank, self.weight_class_label, self.fighter.full_name)

    # class Meta:
    #     unique_together = ('rank', 'weight_class', 'is_champion', 'is_interim_champion')


pre_save.connect(Ranking.pre_save, Ranking, dispatch_uid='fightnerd.fighters.models.Ranking')
pre_save.connect(Fighter.pre_save, Fighter, dispatch_uid='fightnerd.fighters.models.Fighter')
pre_save.connect(Event.pre_save, Event, dispatch_uid='fightnerd.fighters.models.Event')
post_save.connect(Event.post_save, Event, dispatch_uid='fightnerd.fighters.models.Event')

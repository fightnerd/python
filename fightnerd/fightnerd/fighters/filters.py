from __future__ import absolute_import

import django_filters
from fightnerd.fighters.models import UpcomingEvent


class UpcomingEventFilter(django_filters.FilterSet):
    min_date = django_filters.CharFilter(name='date', lookup_type='gte')
    max_date = django_filters.CharFilter(name='date', lookup_type='lte')

    class Meta:
        model = UpcomingEvent
        fields = ['is_prime', 'min_date', 'max_date']

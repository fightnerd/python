import random
from datetime import timedelta
import urllib

from django.core.urlresolvers import reverse
from fightnerd.fighters.models import WeightClassType
from fightnerd.fighters.models import FightType
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Fight
from fightnerd.fighters.models import Corner
from fightnerd.fighters.models import Association
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Referee
from fightnerd.fighters.models import Ranking
from fightnerd.fighters.models import Organization
from rest_framework.test import APITestCase
from rest_framework import status
from django.utils import timezone
from django.db import transaction
from django.db import IntegrityError
from dateutil import parser as date_parser
import os
from django.core.files import File


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), 'fixtures')


def get_fighter_image():
    fighter_images_path = os.path.join(FIXTURES_DIR, 'fighter_images/')
    file_paths = [os.path.join(fighter_images_path, f) for f in os.listdir(fighter_images_path)]
    file_paths = [p for p in file_paths if os.path.isfile(p)]
    return File(open(random.choice(file_paths)))


def get_gibberish(min_length, max_length):
    gibberish = ''
    length = random.randint(min_length, max_length)
    for i in range(length):
        ascii_table = random.randint(0, 2)
        if ascii_table == 0:
            gibberish += unichr(random.randint(48, 57))
        elif ascii_table == 1:
            gibberish += unichr(random.randint(65, 90))
        elif ascii_table == 2:
            gibberish += unichr(random.randint(97, 122))
    return gibberish


def tear_down_fighters():
    Fighter.objects.all().delete()
    Fight.objects.all().delete()
    Association.objects.all().delete()
    Event.objects.all().delete()
    Referee.objects.all().delete()
    Ranking.objects.all().delete()
    Organization.objects.all().delete()


def create_fighters(num_fighters, num_fights=100, num_upcoming_fights=5):
    weight_classes = [x[0] for x in WeightClassType.choices()]
    ranking_weight_classes = [x[0] for x in WeightClassType.ranking_choices()]
    organizations = list()
    organizations.append(Organization.objects.get_or_create(name='ultimate fighting championship')[0])
    organizations.append(Organization.objects.get_or_create(name='bellator mma')[0])
    organizations.append(Organization.objects.get_or_create(name='invicta fighting championships')[0])
    organizations.append(Organization.objects.get_or_create(name='world series of fighting')[0])
    organizations.append(Organization.objects.get_or_create(name='titan fc')[0])
    organizations.append(Organization.objects.get_or_create(name='uncle mike\'s backyard')[0])

    # Associations
    associations = list()
    for i in range(3):
        associations.append(Association.objects.create(name=get_gibberish(5, 20)))

    # Referees
    referees = list()
    for i in range(5):
        referees.append(Referee.objects.create(full_name=get_gibberish(5, 20)))

    # Events
    events = list()
    for i in range(5):
        date = timezone.now() + timedelta(seconds=9000)
        organization = organizations[i % len(organizations)]
        events.append(Event.objects.create(
            name='%s %d' % (organization.name, random.randint(1, 9000)),
            date=date,
            sherdog_id=get_gibberish(5, 20),
            organization=organization,
            is_upcoming=True
        ))

    # # Upcoming events
    # upcoming_events = list()
    # for i in range(2 * len(organizations)):
    #     date = timezone.now() + timedelta(
    #         seconds=random.randint(-6 * 4 * 7 * 24 * 60 * 60, 6 * 4 * 7 * 24 * 60 * 60))
    #     name = get_gibberish(5, 20)
    #     sherdog_id = get_gibberish(5, 20)
    #     organization = organizations[i % len(organizations)]
    #     event = Event.objects.create(
    #         name=name,
    #         date=date,
    #         sherdog_id=sherdog_id,
    #         organization=organization
    #     )
    #     upcoming_events.append(event)

    # Fighters
    fighters = list()
    for i in range(num_fighters):
        date_of_birth = timezone.now() - timedelta(seconds=random.randint(0, 30 * 525600 * 60))
        fighter = Fighter.objects.create(
            full_name=get_gibberish(5, 20),
            sherdog_id=get_gibberish(5, 20),
            nickname=get_gibberish(5, 20),
            date_of_birth=date_of_birth,
            height=random.random() * 100,
            weight=random.random() * 100,
            weight_class=weight_classes[i % len(weight_classes)],
            wins_by_knockout=random.randint(0, 100),
            wins_by_submission=random.randint(0, 100),
            wins_by_decision=random.randint(0, 100),
            wins_by_other=random.randint(0, 100),
            losses_by_knockout=random.randint(0, 100),
            losses_by_submission=random.randint(0, 100),
            losses_by_decision=random.randint(0, 100),
            losses_by_other=random.randint(0, 100),
            draws=random.randint(0, 100),
            no_contests=random.randint(0, 100),
            city=get_gibberish(5, 20),
            state=get_gibberish(5, 20),
            country=get_gibberish(5, 20),
            image=get_fighter_image()
        )
        fighter.associations.add(random.choice(associations))
        fighters.append(fighter)

    # Fights
    fights = list()
    for i in range(num_fights):
        did_create_fight = False
        while not did_create_fight:
            try:
                with transaction.atomic():
                    first_fighter = random.choice(fighters)
                    second_fighter = random.choice(fighters)
                    event = random.choice(events)
                    referee = random.choice(referees)
                    result = random.choice(['Submission', 'Knockout', 'TKO', 'Armbar', 'Omoplata'])
                    method = random.choice(['Win', 'Loss'])
                    type = random.choice([FightType.PROFESSIONAL])
                    time = random.randint(1, 25 * 60)
                    round = random.randint(1, 5)
                    if Fight.objects.filter(event=event).exists():
                        order = Fight.objects.filter(event=event).order_by('-order')[0].order + 1
                    else:
                        order = 0
                    fight = Fight.objects.create(
                        result=result,
                        round=round,
                        order=order,
                        method=method,
                        type=type,
                        time=time,
                        event=event,
                        referee=referee,
                    )

                    # Corners
                    Corner.objects.create(
                        fight=fight,
                        fighter=first_fighter,
                        odds=random.randint(-1000, 1000),
                        is_winner=True,
                        order=0

                    )
                    Corner.objects.create(
                        fight=fight,
                        fighter=second_fighter,
                        odds=random.randint(-1000, 1000),
                        is_winner=False,
                        order=1

                    )
                    first_fighter.fights.add(fight)
                    second_fighter.fights.add(fight)
                    fights.append(fight)
                    did_create_fight = True
            except IntegrityError:
                pass

    # Create rankings
    rankings = list()
    for ranking_weight_class in ranking_weight_classes:
        for i in range(min(num_fighters, 10)):
            fighter = random.choice(fighters)
            rank = i
            rank_delta = random.randint(-5, 5)
            weight_class = ranking_weight_class
            not_previously_ranked = False
            if ranking_weight_class == 'pound-for-pound':
                is_champion = False
            else:
                is_champion = i == 0
            ranking = Ranking.objects.create(
                fighter=fighter,
                rank=rank,
                rank_delta=rank_delta,
                weight_class=weight_class,
                not_previously_ranked=not_previously_ranked,
                is_champion=is_champion
            )
            rankings.append(ranking)

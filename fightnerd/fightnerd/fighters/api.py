from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import django_filters
from drf_haystack.viewsets import HaystackViewSet
from fightnerd.api import HaystackFilter
from fightnerd.api import HaystackSerializer
from fightnerd.fighters.models import Corner
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Fight
from fightnerd.fighters.models import Ranking
from fightnerd.fighters.models import WeightClassType
from fightnerd.fighters.search_indexes import FighterIndex
from rest_framework import filters
from rest_framework import mixins
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.reverse import reverse
from django import forms
from django.utils.dateparse import parse_datetime
from django.utils.encoding import force_str, force_text, smart_text
from django.db.models import Q
from django.db.models import Count


class DateTimeField(forms.DateTimeField):
    def strptime(self, value, format):
        return parse_datetime(force_str(value))


class DateTimeFilter(django_filters.DateTimeFilter):
    field_class = DateTimeField


class FighterFilterSet(django_filters.FilterSet):
    q = django_filters.CharFilter(method='filter_by_query')
    is_prime = django_filters.BooleanFilter(method='filter_by_is_prime')

    def filter_by_is_prime(self, queryset, name, value):
        if value:
            return queryset.prime()
        else:
            return queryset

    def filter_by_query(self, queryset, name, value):
        return queryset.filter(Q(full_name__icontains=value) | Q(nickname__icontains=value))

    class Meta:
        model = Fighter


class EventFilterSet(django_filters.FilterSet):
    after_date = DateTimeFilter(name='date', lookup_type='gte')

    class Meta:
        model = Event
        fields = ['after_date']


class FighterSerializer(serializers.HyperlinkedModelSerializer):
    is_prime = serializers.SerializerMethodField()
    image_url = serializers.ImageField(source='image')
    content_url = serializers.HyperlinkedIdentityField(
        view_name='fighters:fighter_detail',
        lookup_field='guid'
    )
    weight_class = serializers.CharField(source='weight_class_label', read_only=True)

    def get_is_prime(self, obj):
        return obj.is_prime

    class Meta:
        model = Fighter
        fields = [
            'full_name',
            'nickname',
            'weight_class',
            'wins_by_knockout',
            'wins_by_submission',
            'wins_by_decision',
            'wins_by_other',
            'losses_by_knockout',
            'losses_by_submission',
            'losses_by_decision',
            'losses_by_other',
            'draws',
            'no_contests',
            'is_prime',
            'guid',
            'image_url',
            'url',
            'content_url'
        ]
        extra_kwargs = {
            'url': {
                'view_name': 'api_fighters:fighter_detail',
                'lookup_field': 'guid'
            }
        }


class RankingSerializer(serializers.ModelSerializer):
    fighter = FighterSerializer(required=True)
    weight_class = serializers.CharField(source='weight_class_label', read_only=True)

    class Meta:
        model = Ranking
        exclude = ('date_modified', 'date_created', 'id')


class CornerSerializer(serializers.ModelSerializer):
    fighter = FighterSerializer(required=True)

    class Meta:
        model = Corner
        fields = [
            'fighter',
            'odds',
            'order'
        ]


class FightSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        payload = super(FightSerializer, self).to_representation(instance)
        payload['corners'] = list()
        corners = Corner.objects.filter(fight=instance)
        for corner in corners:
            corner_serializer = CornerSerializer(corner, context=self.context)
            payload.get('corners').append(corner_serializer.data)
        assert len(payload.get('corners')) == 2, 'Fight (%d) is invalid' % instance.id
        payload['content_url'] = reverse('fighters:tale_of_the_tape', request=self.context.get('request'), kwargs={
            'first_fighter_guid': corners[0].fighter.guid,
            'second_fighter_guid': corners[1].fighter.guid
        })
        return payload

    class Meta:
        model = Fight
        fields = [
            'order'
        ]


class EventListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='api_fighters:event_detail', lookup_field='guid')
    fights = FightSerializer(many=True)

    # def to_representation(self, instance):
    #     payload = super(EventListSerializer, self).to_representation(instance)
    #     payload['fights'] = list()
    #     fight_serializer = FightSerializer(instance.main_fight, context=self.context)
    #     payload['fights'].append(fight_serializer.data)
    #     return payload

    class Meta:
        model = Event
        fields = [
            'title',
            'subtitle',
            'date',
            'guid',
            'is_upcoming',
            'is_time_valid',
            'url',
            'fights'
        ]


class EventAppleNotificationSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='api_fighters:event_detail', lookup_field='guid')

    class Meta:
        model = Event
        fields = [
            'url',
        ]

class EventDetailSerializer(serializers.ModelSerializer):
    fights = FightSerializer(many=True)
    url = serializers.HyperlinkedIdentityField(view_name='api_fighters:event_detail', lookup_field='guid')

    class Meta:
        model = Event
        fields = [
            'title',
            'subtitle',
            'date',
            'is_upcoming',
            'guid',
            'is_time_valid',
            'fights',
            'url'
        ]


class RankingViewSet(mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    # queryset = Ranking.objects.all()
    serializer_class = RankingSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Ranking.objects.all()
        weight_class = self.request.query_params.get('weightClass', None)
        if weight_class is not None:
            for item in WeightClassType.labels.items():
                if weight_class.lower() == item[1].lower():
                    weight_class = item[0]
                    break
            queryset = queryset.filter(weight_class=weight_class)
        return queryset


class FighterViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Fighter.objects.exclude(weight_class=WeightClassType.UNKNOWN)
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    filter_class = FighterFilterSet
    lookup_field = 'guid'

    def get_serializer_class(self):
        return FighterSerializer


class EventViewSet(mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   viewsets.GenericViewSet):
    list_serializer_class = EventListSerializer
    detail_serializer_class = EventDetailSerializer
    lookup_field = 'guid'
    queryset = Event.objects.filter(is_prime=True, is_upcoming=True)
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    filter_class = EventFilterSet
    ordering_fields = ('date',)
    ordering = ('date',)

    def get_serializer_class(self):
        if self.action == 'list':
            return EventListSerializer
        else:
            return EventDetailSerializer
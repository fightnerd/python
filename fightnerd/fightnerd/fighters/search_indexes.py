from haystack import indexes
from fightnerd.fighters.models import WeightClassType
from fightnerd.fighters.models import Fighter
from django.conf import settings
from django.template import loader
from fightnerd.context_processors import constants
from django.contrib.sites.models import Site
from django.utils import timezone
from datetime import timedelta

API_DOMAIN = getattr(settings, 'API_DOMAIN')
API_PROTOCOL = getattr(settings, 'API_PROTOCOL')


# class FighterIndex(indexes.SearchIndex, indexes.Indexable):
#     text = indexes.EdgeNgramField(document=True, use_template=True)
#     is_prime = indexes.BooleanField()
#     image_url = indexes.CharField()
#     html_page = indexes.CharField()
#     weight_class = indexes.CharField()
#
#     def get_model(self):
#         return Fighter
#
#     def get_updated_field(self):
#         return 'date_modified'
#
#     def prepare_weight_class(self, obj):
#         return WeightClassType.label(obj.weight_class)
#
#     def prepare_html_page(self, obj):
#         # Render
#         current_site = Site.objects.get_current()
#         context = dict({
#             'domain': current_site.domain,
#             'site_name': current_site.name,
#             'protocol': 'http',
#             'fighter': obj
#         })
#         context.update(constants(None))
#         return loader.render_to_string('fighters/fighter_detail.html', context)
#
#     def prepare_image_url(self, obj):
#         return '%s://%s%s' % (API_PROTOCOL, API_DOMAIN, obj.image.url) if obj.image else ''
#
#     def prepare_is_prime(self, obj):
#         return obj.is_prime


class FighterIndex(indexes.ModelSearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    is_prime = indexes.BooleanField()
    image_url = indexes.CharField()
    weight_class = indexes.CharField()

    def index_queryset(self, using=None):
        return Fighter.objects\
            .exclude(weight_class=WeightClassType.UNKNOWN)\
            .exclude(wins_by_knockout=0, wins_by_submission=0, wins_by_decision=0, wins_by_other=0)\
            .exclude(losses_by_knockout=0, losses_by_submission=0, losses_by_decision=0, losses_by_other=0)\
            .filter(fights__event__date__gte=timezone.now() - timedelta(days=5 * 365))\
            .distinct()

    def get_updated_field(self):
        return 'date_modified'

    def prepare_weight_class(self, obj):
        return WeightClassType.label(obj.weight_class)

    def prepare_image_url(self, obj):
        return obj.image.url if obj.image else ''

    def prepare_is_prime(self, obj):
        return obj.is_prime

    class Meta:
        model = Fighter
        fields = [
            'full_name',
            'nickname',
            'wins_by_knockout',
            'wins_by_submission',
            'wins_by_decision',
            'wins_by_other',
            'losses_by_knockout',
            'losses_by_submission',
            'losses_by_decision',
            'losses_by_other',
            'draws',
            'no_contests',
            'guid'
        ]

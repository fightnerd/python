from django.conf.urls import patterns, url
from fightnerd.fighters.views import FighterDetailView
from fightnerd.fighters.views import TaleOfTheTapeView
from django.views.decorators.cache import cache_page, never_cache

urlpatterns = patterns('',
    url(r'^fighter/(?P<guid>[0-9a-zA-Z]+)/$', cache_page(60 * 60 * 24)(FighterDetailView.as_view()), name='fighter_detail'),
    url(r'^tale-of-the-tape/(?P<first_fighter_guid>[0-9a-zA-Z]+)/(?P<second_fighter_guid>[0-9a-zA-Z]+)/$', cache_page(60 * 60 * 24)(TaleOfTheTapeView.as_view()), name='tale_of_the_tape'),
)

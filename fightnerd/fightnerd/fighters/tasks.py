from __future__ import absolute_import

import base64

from fightnerd.fighters.models import WeightClassType
from fightnerd.fighters.models import FightType
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Association
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Referee
from fightnerd.fighters.models import Fight
from fightnerd.fighters.models import Organization
from fightnerd.fighters.models import Ranking
from fightnerd.fighters.models import Corner
import re
from fightnerd.celeryapp import app
from dateutil import parser as dateparser
from django.core.files.base import ContentFile
from celery.utils.log import get_task_logger
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
from django.core import management
from django.core.cache import cache


PROJECT_DIR = getattr(settings, 'PROJECT_DIR')
LOGS_ROOT = getattr(settings, 'LOGS_ROOT')
logger = get_task_logger(__name__)
TZD = getattr(settings, 'TZD')


def get_fight_type(fight_label):
    fight_type = FightType.PROFESSIONAL
    for item in FightType.labels.items():
        if item[1].lower() == fight_label.lower():
            fight_type = item[0]
            break
    return fight_type


def get_weight_class_type(weight_class_label):
    weight_class_type = WeightClassType.UNKNOWN
    for item in WeightClassType.labels.items():
        if item[1].lower() == weight_class_label.lower():
            weight_class_type = item[0]
            break
    return weight_class_type


@app.task()
def crawl_upcoming_events():
    spiders = [
        'sherdog_upcoming_events',
        'ufc_upcoming_events',
        'ufc_rankings',
        'fivedimes',
    ]
    for spider in spiders:
        management.call_command('scrapy', 'crawl %s' % spider)


def process_event(event_item, is_upcoming=False):
    sherdog_id = event_item['sherdog_id']
    defaults = dict()
    defaults['name'] = event_item['name']
    defaults['date'] = dateparser.parse(event_item['date'], tzinfos=TZD) if event_item['date'] else None
    if 'location' in event_item:
        defaults['location'] = event_item['location']
    if 'organization' in event_item:
        defaults['organization'], is_created = Organization.objects.get_or_create(name=event_item['organization'])
    event, is_created = Event.objects.update_or_create(sherdog_id=sherdog_id, is_upcoming=is_upcoming, defaults=defaults)
    return event


def process_referee(referee_item):
    full_name = referee_item['full_name']
    referee, did_create = Referee.objects.get_or_create(full_name=full_name)
    return referee


def process_fight(fight_item, fighter):
    # Process event
    event = process_event(fight_item['event']) if fight_item['event'] else None

    # Process referee
    referee = process_referee(fight_item['referee']) if fight_item['referee'] else None

    # Process winners/losers
    if fight_item['result'].lower() == 'win':
        winner = fighter
        loser = process_fighter(fight_item['opponent'])
    else:
        loser = fighter
        winner = process_fighter(fight_item['opponent'])

    # Create fight
    is_no_contest = fight_item['result'].lower() == 'nc'
    fight = Fight.objects.create(
        result=fight_item['result'].lower(),
        method=fight_item['method'],
        round=fight_item['round'],
        time=fight_item['time'],
        type=get_fight_type(fight_item['type']),
        referee=referee,
        event=event
    )
    winner_corner = Corner.objects.create(
        fight=fight,
        fighter=winner,
        order=0,
        is_winner=not is_no_contest
    )
    loser_corner = Corner.objects.create(
        fight=fight,
        fighter=loser,
        order=1,
        is_winner=False
    )
    return fight


def process_association(association_item):
    association, did_create = Association.objects.get_or_create(name=association_item['name'])
    return association


@app.task(bind=True)
def process_fighter(self, fighter_item):
    # Create or update fighter
    full_name = fighter_item['full_name']

    if 'sherdog_id' not in fighter_item:
        try:
            fighter = Fighter.objects.get(full_name=full_name)
        except Fighter.DoesNotExist:
            fighter = None
        return fighter

    sherdog_id = fighter_item['sherdog_id']
    defaults = {}
    if not fighter_item['is_partial']:
        defaults['nickname'] = fighter_item['nickname'] if fighter_item['nickname'] else ''
        defaults['date_of_birth'] = dateparser.parse(fighter_item['date_of_birth'], tzinfos=TZD) if fighter_item[
            'date_of_birth'] else None
        defaults['height'] = fighter_item['height']
        defaults['weight'] = fighter_item['weight']
        defaults['weight_class'] = get_weight_class_type(fighter_item['weight_class'].lower() if fighter_item['weight_class'] else 'unknown')
        defaults['wins_by_knockout'] = fighter_item['wins_by_knockout']
        defaults['wins_by_submission'] = fighter_item['wins_by_submission']
        defaults['wins_by_decision'] = fighter_item['wins_by_decision']
        defaults['wins_by_other'] = fighter_item['wins_by_other']
        defaults['losses_by_knockout'] = fighter_item['losses_by_knockout']
        defaults['losses_by_submission'] = fighter_item['losses_by_submission']
        defaults['losses_by_decision'] = fighter_item['losses_by_decision']
        defaults['losses_by_other'] = fighter_item['losses_by_other']
        defaults['draws'] = fighter_item['draws']
        defaults['no_contests'] = fighter_item['no_contests']
        if 'city' in fighter_item and fighter_item['city']:
            defaults['city'] = fighter_item['city']
        if 'state' in fighter_item and fighter_item['state']:
            defaults['state'] = fighter_item['state']
        if 'country' in fighter_item and fighter_item['country']:
            defaults['country'] = fighter_item['country']

        fighter, did_create = Fighter.objects.update_or_create(full_name=full_name,
                                                               sherdog_id=sherdog_id,
                                                               defaults=defaults)
    else:
        fighter, did_create = Fighter.objects.get_or_create(full_name=full_name,
                                                            sherdog_id=sherdog_id)

    # If this is not a full item we are done...
    if fighter_item['is_partial']:
        return fighter

    # Load images into DB
    if 'images' in fighter_item and len(fighter_item['images']) > 0:
        image = fighter_item['images'][0]
        image_file = ContentFile(base64.b64decode(image['data']))
        fighter.image.save(image['name'], image_file)

    # Process associations
    if 'associations' in fighter_item:
        fighter.associations.clear()
        for association_item in fighter_item['associations']:
            association = process_association(association_item)
            fighter.associations.add(association)

    # Process fights
    if 'fights' in fighter_item:
        fighter.fights.clear()
        for fight_item in fighter_item['fights']:
            fight = process_fight(fight_item, fighter)
            fighter.fights.add(fight)

    # Is it prime?
    key = 'fighters.Fighter.%s.is_prime' % fighter.guid
    cache.delete(key)
    is_prime = fighter.is_prime

    logger.info('Processed %s with %d fights' % (fighter, fighter.fights.count()))
    return fighter


def process_upcoming_fight(upcoming_fight_item, upcoming_event):
    first_fighter = process_fighter(upcoming_fight_item['first_fighter'])
    second_fighter = process_fighter(upcoming_fight_item['second_fighter'])
    order = upcoming_fight_item['order']

    # Create fight
    upcoming_fight = Fight.objects.create(event=upcoming_event, order=order, type=FightType.PROFESSIONAL)
    winner_corner = Corner.objects.create(
        fight=upcoming_fight,
        fighter=first_fighter,
        order=0,
        is_winner=False
    )
    loser_corner = Corner.objects.create(
        fight=upcoming_fight,
        fighter=second_fighter,
        order=1,
        is_winner=False
    )
    return upcoming_fight


@app.task()
def process_upcoming_event(upcoming_event_item):
    defaults = dict()
    event = process_event(upcoming_event_item, is_upcoming=True)

    # if event.date < timezone.now() + timedelta(seconds=86400):
    #     logger.info('Skipping event (%s) that already occurred' % event)
    #     return event

    # Process fights
    event.fights.all().delete()
    for upcoming_fight_item in upcoming_event_item['upcoming_fights']:
        logger.info('Processing %s vs %s' % (upcoming_fight_item['first_fighter']['full_name'], upcoming_fight_item['second_fighter']['full_name']))
        upcoming_fight = process_upcoming_fight(upcoming_fight_item, event)

    event.save()

    logger.info('Processed %s with %d upcoming fights\n\n' % (event, event.fights.all().count()))
    return event


@app.task()
def process_upcoming_ufc_event_time(upcoming_event_item):
    logger.info(upcoming_event_item)
    last_names = {
        'Wanderlei': 'Silva',
        'Nogueira': 'Rodrigo Nogueira',
        'Santos': 'Dos Santos',
        'Saint Preux': 'St. Preux',
        'Dos Anjos': 'dos Anjos',
        'Bigfoot': 'Silva',
        'Cro Cop': 'Filipovic'
    }

    # Get fighter names
    upcoming_fight_item = upcoming_event_item['upcoming_fights'][0]
    first_fighter_last_name = upcoming_fight_item['first_fighter']
    second_fighter_last_name = upcoming_fight_item['second_fighter']

    # Filter out nicknames...etc...
    first_fighter_last_name = last_names[first_fighter_last_name] if first_fighter_last_name in last_names else first_fighter_last_name
    second_fighter_last_name = last_names[second_fighter_last_name] if second_fighter_last_name in last_names else second_fighter_last_name

    # Find the matching upcoming event
    did_process_event_time = False
    cutoff_date = timezone.now() - timedelta(days=2)
    for upcoming_event in Event.objects.filter(date__gte=cutoff_date, is_upcoming=True):
        if not upcoming_event.is_prime:
            continue
        if 'ufc' not in upcoming_event.name.lower() and 'the ultimate fighter' not in upcoming_event.name.lower():
            continue
        # logger.debug(upcoming_event)
        upcoming_fight = upcoming_event.get_fight_by_last_names(first_fighter_last_name, second_fighter_last_name)
        if upcoming_fight:
            upcoming_event.date = upcoming_event_item['date']
            upcoming_event.is_time_valid = True
            upcoming_event.save()
            did_process_event_time = True
            logger.info('Processed event time for %s @ %s' % (upcoming_event, upcoming_event_item['date']))
            break
    if not did_process_event_time:
        logger.error('Failed to process event time for %s' % upcoming_event_item['name'])
    return upcoming_event


@app.task(bind=True)
def process_odds(self, upcoming_event_item):
    upcoming_event_name = upcoming_event_item['name']
    upcoming_event_name = re.sub(':', ' -', upcoming_event_name).lower()

    # For WSOF...
    if 'world series of fighting' in upcoming_event_name:
        upcoming_event_name = re.sub('world series of fighting', 'wsof', upcoming_event_name)
        upcoming_event_name = re.sub('x$', ' 10', upcoming_event_name)
        upcoming_event_name = re.sub('ix$', '9', upcoming_event_name)
        upcoming_event_name = re.sub('viii$', '8', upcoming_event_name)
        upcoming_event_name = re.sub('vii$', '7', upcoming_event_name)
        upcoming_event_name = re.sub('vi$', '6', upcoming_event_name)
        upcoming_event_name = re.sub('v$', '5', upcoming_event_name)
        upcoming_event_name = re.sub('iv$', '4', upcoming_event_name)
        upcoming_event_name = re.sub('iii$', '3', upcoming_event_name)
        upcoming_event_name = re.sub('ii$', '2', upcoming_event_name)
        upcoming_event_name = re.sub('i$', '1', upcoming_event_name)
    if 'iii' in upcoming_event_name:
        upcoming_event_name = re.sub('iii', '3', upcoming_event_name)
    elif 'ii' in upcoming_event_name:
        upcoming_event_name = re.sub('ii', '2', upcoming_event_name)
    elif 'iv' in upcoming_event_name:
        upcoming_event_name = re.sub('iv', '4', upcoming_event_name)

    # Get the upcoming event
    try:
        upcoming_event = Event.objects.get(is_upcoming=True, name__icontains=upcoming_event_name)
    except (Event.DoesNotExist, Event.MultipleObjectsReturned):
        logger.error('Failed to process odds due to missing event (%s)\n%s' % (upcoming_event_name, upcoming_event_item))
        return None

    # Get the upcoming fight
    upcoming_fight_item = upcoming_event_item['upcoming_fights'][0]
    first_fighter = process_fighter(upcoming_fight_item['first_fighter'])
    second_fighter = process_fighter(upcoming_fight_item['second_fighter'])
    if not first_fighter or not second_fighter:
        missing_fighters = []
        if not first_fighter:
            missing_fighters.append(upcoming_fight_item['first_fighter']['full_name'])
        if not second_fighter:
            missing_fighters.append(upcoming_fight_item['second_fighter']['full_name'])
        logger.error('Failed to process odds for upcoming fight due to not finding %s\n%s' % (' and'.join(missing_fighters), upcoming_fight_item))
        return None

    first_corner = None
    second_corner = None
    try:
        first_corner = Corner.objects.get(fight__event=upcoming_event, fighter=first_fighter)
    except Corner.DoesNotExist:
        pass
    try:
        second_corner = Corner.objects.get(fight__event=upcoming_event, fighter=second_fighter)
    except Corner.DoesNotExist:
        pass

    if first_corner and second_corner:
        first_corner.odds = upcoming_fight_item['first_fighter_odds']
        second_corner.odds = upcoming_fight_item['second_fighter_odds']
        first_corner.save()
        second_corner.save()
        logger.debug('Processed odds for %s' % first_corner.fight)
        return upcoming_event
    else:
        logger.error('Failed to process odds for upcoming fight due to missing fight\n%s' % upcoming_fight_item)
        return None


def process_ufc_fighter(fighter_item):
    guids = {
        'Cris Cyborg': 'e3a5d65b19a0c6269d51403a245c03ae',
        'Dooho Choi': '789e8e3f3b3e8ffe38da06b0c75c337b',
        'Tim Johnson': 'af8274441f16160fe434df5b1b155b5b',
        'Ashlee Evans-Smith': 'd459d17dc23251ed0c846d8c664962e7',
        'TJ Dillashaw': '49bdbc4a199ae29bb19e6d7aab819f9b',
        'Jussier Formiga': '90b747f0d4023472dc01e9784343a207',
        'Jacare Souza': 'ae03ee78c63a49d0933b62edb8e0d26e',
        'Timothy Elliott': 'c2b33c5669572a6c0490e0c4d5549f50',
        'CB Dollaway': '265e86608ee8379fb305f8d1fe72b8ad',
        'Ovince Saint Preux': '537a7534e66254111dc04f3032f4bc5d',
        'Junior Dos Santos': 'a5e27b63e20fb202aa8fab51feff0513',
        'Juliana Lima': '1a0bff5af4ebac4f4f4b0edc832483bd',
        'Seohee Ham': '36dd79d52fe9700cd76a6af8f320d897',
        'Rampage Jackson': '8e14fac12817243ac22f3609f940b055',
        'Alexey Oliynyk': 'b381adac8fc3452388a602d6ab0ffb0b',
        'Rafael Dos Anjos': '08bd3f56d6965141cf062f686058c200'
    }
    full_name = fighter_item['full_name']
    if full_name in guids:
        guid = guids[full_name]
        fighter = None
        try:
            fighter = Fighter.objects.get(guid=guid)
        except:
            logger.exception('Failed to find %s in the database' % full_name)
        return fighter
    else:
        query_set = Fighter.objects.filter(full_name=full_name)
        for fighter in query_set:
            if fighter.is_prime:
                return fighter
    return None


@app.task(bind=True)
def process_rankings(self, ranking_list_item):
    # Make sure each fighter exists in the DB
    fighters = []
    weight_class = None
    for ranking_item in ranking_list_item['rankings']:
        fighter_item = ranking_item['fighter']
        weight_class = get_weight_class_type(ranking_item['weight_class'])
        fighter = process_ufc_fighter(fighter_item)
        # logger.debug(fighter)
        if not fighter:
            logger.error('Failed to process rankings for %s at %s' % (fighter_item['full_name'], ranking_item['weight_class']))
        else:
            fighters.append(fighter)
    if len(fighters) != len(ranking_list_item['rankings']):
        return None

    # Delete current rankings
    Ranking.objects.filter(weight_class=weight_class).delete()

    for ranking_item, fighter in zip(ranking_list_item['rankings'], fighters):
        ranking = Ranking.objects.create(fighter=fighter,
                                         rank=ranking_item['rank'],
                                         rank_delta=ranking_item['rank_delta'],
                                         weight_class=weight_class,
                                         not_previously_ranked=ranking_item['not_previously_ranked'],
                                         is_champion=ranking_item['is_champion'],
                                         is_interim_champion=ranking_item['is_interim_champion'])
        # logger.info('Processed ranking for %s (%d) at %s' % (fighter.full_name, ranking.rank, WeightClassType.label(ranking.weight_class)))

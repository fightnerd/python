from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase
from fightnerd.fighters.models import Organization
from fightnerd.fighters.models import Event
from fightnerd.notifications.models import Notification
from django.utils import timezone
from fightnerd.utils import get_lorem_text
from datetime import timedelta


class EventTestCase(TestCase):
    def tearDown(self):
        Event.objects.all().delete()
        Notification.objects.all().delete()
        Organization.objects.all().delete()

    def test_notifications(self):
        event = Event.objects.create(
            name=get_lorem_text(1, 'w'),
            date=timezone.now(),
            sherdog_id=get_lorem_text(1, 'w'),
            is_upcoming=True,
            is_prime=True,
            is_time_valid=True,
            organization=Organization.objects.create(name='ultimate fighting championship')
        )

        self.assertEqual(Notification.objects.all().count(), 2)
        self.assertTrue(Notification.objects.filter(fire_date=event.date).exists())
        self.assertTrue(Notification.objects.filter(fire_date=event.date - timedelta(hours=8)).exists())

    def test_notifications_with_invalid_time(self):
        event = Event.objects.create(
            name=get_lorem_text(1, 'w'),
            date=timezone.now(),
            sherdog_id=get_lorem_text(1, 'w'),
            is_upcoming=True,
            is_prime=True,
            is_time_valid=False,
            organization=Organization.objects.create(name='ultimate fighting championship')
        )

        self.assertEqual(Notification.objects.all().count(), 0)

    def test_multiple_event_saves(self):
        event = Event.objects.create(
            name=get_lorem_text(1, 'w'),
            date=timezone.now(),
            sherdog_id=get_lorem_text(1, 'w'),
            is_upcoming=True,
            is_prime=True,
            is_time_valid=True,
            organization=Organization.objects.create(name='ultimate fighting championship')
        )

        for i in range(10):
            event.save()

        self.assertEqual(Notification.objects.all().count(), 2)
        self.assertTrue(Notification.objects.filter(fire_date=event.date).exists())
        self.assertTrue(Notification.objects.filter(fire_date=event.date - timedelta(hours=8)).exists())

from django.core.management import call_command
from django.core.urlresolvers import reverse
from django_webtest import WebTest
from fightnerd.fighters.utils import create_fighters
from fightnerd.fighters.utils import tear_down_fighters
from fightnerd.fighters.models import Fighter
from rest_framework import status


class BaseTestCases(object):
    class FighterViewTestCase(WebTest):
        def get_url(self):
            raise NotImplementedError()

        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.FighterViewTestCase, cls).setUpClass()
            create_fighters(10)

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.FighterViewTestCase, cls).tearDownClass()
            tear_down_fighters()

        def test_cache_headers(self):
            url = self.get_url()
            response = self.app.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertIn('Last-Modified', response.headers)
            self.assertIn('Expires', response.headers)
            self.assertIn('Cache-Control', response.headers)
            self.assertIn('max-age=86400', response.headers['Cache-Control'])


class TaleOfTheTapeViewTestCase(BaseTestCases.FighterViewTestCase):
    def setUp(self):
        super(TaleOfTheTapeViewTestCase, self).setUp()
        fighters = Fighter.objects.all()
        self.first_fighter = fighters[0]
        self.second_fighter = fighters[1]

    def get_url(self):
        return reverse('fighters:tale_of_the_tape', kwargs={
            'first_fighter_guid': self.first_fighter.guid,
            'second_fighter_guid': self.second_fighter.guid
        })

    def test_page(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class FighterDetailViewTestCase(BaseTestCases.FighterViewTestCase):
    def setUp(self):
        super(FighterDetailViewTestCase, self).setUp()
        self.fighter = Fighter.objects.all()[0]

    def get_url(self):
        return reverse('fighters:fighter_detail', kwargs={'guid': self.fighter.guid})

    def test_page(self):
        response = self.app.get(self.get_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)


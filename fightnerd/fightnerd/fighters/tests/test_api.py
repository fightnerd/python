import urllib

from django.core.urlresolvers import reverse
from django.core.management import call_command
from rest_framework.test import APITestCase
from rest_framework import status
from fightnerd.fighters.utils import create_fighters
from fightnerd.fighters.utils import tear_down_fighters
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import WeightClassType
import simplejson
from haystack.query import SearchQuerySet
from django.utils import timezone
from dateutil.parser import parse as parse_date
from datetime import timedelta


def reverse_with_query(view_name, query_params):
    url = reverse(view_name)
    if query_params:
        url += '?' + urllib.urlencode(query_params)
    return url


class BaseTestCases(object):
    class FighterAPITestCase(APITestCase):
        list_view_name = None

        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.FighterAPITestCase, cls).setUpClass()
            create_fighters(300)

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.FighterAPITestCase, cls).tearDownClass()
            tear_down_fighters()

        def authenticate_application(self):
            pass

        def test_cache_headers(self):
            url = reverse(self.list_view_name)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertIn('Last-Modified', response)
            self.assertIn('Expires', response)
            self.assertIn('Cache-Control', response)
            self.assertIn('max-age=3600', response['Cache-Control'])


class EventRetrievalAPITestCase(BaseTestCases.FighterAPITestCase):
    list_view_name = 'api_fighters:event_list'
    detail_view_name = 'api_fighters:event_detail'

    # def test_list(self):
    #     url = reverse(self.list_view_name)
    #     response = self.client.get(url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     for event in response.data.get('results'):
    #         self.assertTrue('guid' in event)
    #         self.assertTrue('date' in event)
    #         self.assertTrue('title' in event)
    #         self.assertTrue('subtitle' in event)
    #         self.assertTrue('fights' in event)
    #         self.assertTrue('url' in event)
    #         self.assertTrue('isTimeValid' in event)
    #         fights = event.get('fights')
    #         self.assertEqual(len(fights), 1)
    #         fight = fights[0]
    #         self.assertTrue('contentUrl' in fight)
    #         self.assertTrue('order' in fight)
    #         corners = fight.get('corners')
    #         self.assertEqual(len(corners), 2)
    #         for corner in corners:
    #             self.assertTrue('odds' in corner)
    #             self.assertTrue('isWinner' in corner)
    #             self.assertTrue('fighter' in corner)
    #             self.assertTrue('order' in corner)

    def test_limit(self):
        query = {
            'limit': 1,
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get('results')), 1)

    def test_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for event in response.data.get('results'):
            self.assertTrue('guid' in event)
            self.assertTrue('isUpcoming' in event)
            self.assertTrue('date' in event)
            self.assertTrue('title' in event)
            self.assertTrue('subtitle' in event)
            self.assertTrue('fights' in event)
            self.assertTrue('url' in event)
            self.assertTrue('isTimeValid' in event)
            fights = event.get('fights')
            self.assertGreater(len(fights), 1)
            for fight in fights:
                self.assertTrue('contentUrl' in fight)
                self.assertTrue('order' in fight)
                corners = fight.get('corners')
                self.assertEqual(len(corners), 2)
                for corner in corners:
                    self.assertTrue('odds' in corner)
                    self.assertTrue('fighter' in corner)
                    self.assertTrue('order' in corner)

    def test_detail(self):
        event = Event.objects.all()[0]
        url = reverse(self.detail_view_name, kwargs={'guid': event.guid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        event = simplejson.loads(response.rendered_content)
        self.assertTrue('guid' in event)
        self.assertTrue('date' in event)
        self.assertTrue('title' in event)
        self.assertTrue('subtitle' in event)
        self.assertTrue('fights' in event)
        fights = event.get('fights')
        self.assertGreater(len(fights), 1)

        for fight in fights:
            self.assertTrue('contentUrl' in fight)
            self.assertTrue('order' in fight)
            corners = fight.get('corners')
            self.assertEqual(len(corners), 2)
            for corner in corners:
                self.assertTrue('odds' in corner)
                self.assertTrue('fighter' in corner)
                self.assertTrue('order' in corner)

    def test_filter_by_date(self):
        after_date = timezone.now() - timedelta(seconds=9000)
        query = {
            'after_date': after_date.strftime('%Y-%m-%dT%H:%M:%S.%f%z'),
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for event in response.data.get('results'):
            self.assertGreaterEqual(parse_date(event.get('date')), after_date)


class RankingRetrievalAPITestCase(BaseTestCases.FighterAPITestCase):
    list_view_name = 'api_fighters:ranking_list'

    def test_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for ranking in response.data.get('results'):
            self.assertTrue('guid' in ranking)
            self.assertTrue('rank' in ranking)
            self.assertTrue('rankDelta' in ranking)
            self.assertTrue('weightClass' in ranking)
            self.assertTrue('notPreviouslyRanked' in ranking)
            self.assertTrue('isChampion' in ranking)
            self.assertTrue('isInterimChampion' in ranking)
            self.assertTrue('fighter' in ranking)
            fighter = ranking.get('fighter')
            self.assertTrue('fullName' in fighter)
            self.assertTrue('nickname' in fighter)
            self.assertTrue('imageUrl' in fighter)
            self.assertTrue('weightClass' in fighter)
            self.assertTrue('winsByKnockout' in fighter)
            self.assertTrue('winsBySubmission' in fighter)
            self.assertTrue('winsByDecision' in fighter)
            self.assertTrue('winsByOther' in fighter)
            self.assertTrue('lossesByKnockout' in fighter)
            self.assertTrue('lossesBySubmission' in fighter)
            self.assertTrue('lossesByDecision' in fighter)
            self.assertTrue('lossesByOther' in fighter)
            self.assertTrue('draws' in fighter)
            self.assertTrue('noContests' in fighter)
            self.assertTrue('isPrime' in fighter)
            self.assertTrue('contentUrl' in fighter)
            self.assertTrue('url' in fighter)
            self.assertIsNotNone(fighter.get('contentUrl'))
            self.assertTrue('http://' in fighter.get('contentUrl'))
            self.assertIsNotNone(fighter.get('imageUrl'))
            self.assertTrue('http://' in fighter.get('imageUrl'))

    def test_filter_by_weight_class(self):
        weight_class = WeightClassType.label(WeightClassType.HEAVYWEIGHT)
        query = {
            'weightClass': WeightClassType.label(weight_class),
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for ranking in response.data.get('results'):
            self.assertEqual(ranking.get('weightClass'), weight_class)

    def test_filter_by_pound_for_pound(self):
        weight_class = 'Pound-For-Pound'
        query = {
            'weightClass': weight_class,
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for ranking in response.data.get('results'):
            self.assertEqual(ranking.get('weightClass'), weight_class)


class FighterRetrievalAPITestCase(BaseTestCases.FighterAPITestCase):
    list_view_name = 'api_fighters:fighter_list'
    detail_view_name = 'api_fighters:fighter_detail'

    def test_pagination(self):
        url = reverse(self.list_view_name)
        last_results = None
        for i in range(2):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertGreater(len(response.data.get('results')), 0)
            self.assertTrue('next' in response.data)
            if last_results:
                self.assertNotEqual(last_results, response.data.get('results'))
            last_results = response.data.get('results')
            url = response.data.get('next')

    def test_detail(self):
        fighter = Fighter.objects.all()[0]
        url = reverse(self.detail_view_name, kwargs={'guid': fighter.guid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        fighter = response.rendered_content
        fighter = simplejson.loads(fighter)
        self.assertTrue('text' not in fighter)
        self.assertTrue('fullName' in fighter)
        self.assertTrue('nickname' in fighter)
        self.assertTrue('imageUrl' in fighter)
        self.assertIsNotNone(fighter.get('imageUrl'))
        self.assertTrue('weightClass' in fighter)
        self.assertTrue('winsByKnockout' in fighter)
        self.assertTrue('winsBySubmission' in fighter)
        self.assertTrue('winsByDecision' in fighter)
        self.assertTrue('winsByOther' in fighter)
        self.assertTrue('lossesByKnockout' in fighter)
        self.assertTrue('lossesBySubmission' in fighter)
        self.assertTrue('lossesByDecision' in fighter)
        self.assertTrue('lossesByOther' in fighter)
        self.assertTrue('draws' in fighter)
        self.assertTrue('noContests' in fighter)
        self.assertTrue('isPrime' in fighter)
        self.assertIsNotNone(fighter.get('isPrime'))
        self.assertTrue('contentUrl' in fighter)
        self.assertTrue('url' in fighter)
        self.assertIsNotNone(fighter.get('contentUrl'))
        self.assertTrue('http://' in fighter.get('contentUrl'))

    def test_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for fighter in response.data.get('results'):
            self.assertTrue('text' not in fighter)
            self.assertTrue('fullName' in fighter)
            self.assertTrue('nickname' in fighter)
            self.assertTrue('imageUrl' in fighter)
            self.assertIsNotNone(fighter.get('imageUrl'))
            self.assertTrue('weightClass' in fighter)
            self.assertTrue('winsByKnockout' in fighter)
            self.assertTrue('winsBySubmission' in fighter)
            self.assertTrue('winsByDecision' in fighter)
            self.assertTrue('winsByOther' in fighter)
            self.assertTrue('lossesByKnockout' in fighter)
            self.assertTrue('lossesBySubmission' in fighter)
            self.assertTrue('lossesByDecision' in fighter)
            self.assertTrue('lossesByOther' in fighter)
            self.assertTrue('draws' in fighter)
            self.assertTrue('noContests' in fighter)
            self.assertTrue('isPrime' in fighter)
            self.assertIsNotNone(fighter.get('isPrime'))
            self.assertTrue('contentUrl' in fighter)
            self.assertIsNotNone(fighter.get('url'))
            self.assertTrue('http://' in fighter.get('url'))

    def test_filter_by_prime(self):
        query = {
            'is_prime': True,
        }
        url = reverse_with_query(self.list_view_name, query)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for fighter in response.data.get('results'):
            self.assertTrue(fighter.get('isPrime'))

    def test_filter_by_query(self):
        query = Fighter.objects.all()[0].full_name
        url = reverse_with_query(self.list_view_name, {'q': query})
        response = self.client.get(url)
        fighters = response.data.get('results')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(fighters), 0)
        for fighter in fighters:
            self.assertTrue(query.lower() in '%s%s' % (fighter.get('fullName').lower(), fighter.get('nickname', '').lower()))

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from celery.utils.log import get_task_logger
from django.core.management.base import BaseCommand
from django.http import HttpRequest
from djangorestframework_camel_case.render import CamelCaseJSONRenderer
from fightnerd.fighters import api
from fightnerd.fighters.models import Fighter
from haystack.query import SearchQuerySet
from rest_framework import serializers
from rest_framework.request import Request
from django.conf import settings

API_DOMAIN = getattr(settings, 'API_DOMAIN')
API_PROTOCOL = getattr(settings, 'API_PROTOCOL')


logger = get_task_logger(__name__)


class FighterSerializer(api.FighterHaystackSerializer):
    image_data = serializers.SerializerMethodField()

    def get_image_data(self, obj):
        try:
            return obj.object.image_data
        except:
            pass
        return None


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('--limit', dest='num_fighters', type=int)

    def handle(self, *args, **options):
        http_request = HttpRequest()
        http_request.META['HTTP_HOST'] = API_DOMAIN
        sqs = SearchQuerySet().models(Fighter).filter(is_prime=True)

        if options.get('num_fighters'):
            sqs = sqs[:options.get('num_fighters')]

        serializer = FighterSerializer(sqs, many=True, context={'request': Request(http_request)})
        rendered_data = CamelCaseJSONRenderer().render(serializer.data)
        print(rendered_data)

from datetime import datetime

from django.contrib import admin
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Association
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Referee
from fightnerd.fighters.models import Fight
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Ranking


class FutureUpcomingEventListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = ('date')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'isFuture'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('isFuture', ('in the future')),
            ('isPast', ('in the past')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'isFuture':
            return queryset.filter(date__gte=datetime.utcnow())
        if self.value() == 'isPast':
            return queryset.filter(date__lt=datetime.utcnow())


@admin.register(Fighter)
class FighterAdmin(admin.ModelAdmin):
    raw_id_fields = ('fights',)


# @admin.register(Event)
# class EventAdmin(admin.ModelAdmin):
#     # raw_id_fields = ('notifications',)
#     readonly_fields = ('is_prime',)
#     ordering = ('is_prime', 'date',)
#     list_filter = (FutureUpcomingEventListFilter,)


@admin.register(Fight)
class FightAdmin(admin.ModelAdmin):
    raw_id_fields = ('fighters',)


@admin.register(Ranking)
class RankingAdmin(admin.ModelAdmin):
    raw_id_fields = ('fighter',)
    ordering = ('weight_class', 'rank')

from __future__ import absolute_import

import logging

from django.views.generic import DetailView
from django.views.generic import TemplateView
from fightnerd.fighters.models import FightType
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Corner
from datetime import timedelta
from math import ceil

logger = logging.getLogger(__name__)


class TaleOfTheTapeView(TemplateView):
    template_name = 'fighters/tale_of_the_tape.html'


class FighterDetailView(DetailView):
    model = Fighter
    template_name_field = 'fighter'
    slug_field = 'guid'
    slug_url_kwarg = 'guid'

    def get_context_data(self, **kwargs):
        context_data = super(FighterDetailView, self).get_context_data(**kwargs)
        fighter = context_data.get('fighter')
        fights = list()
        for fight in fighter.fights.filter(type=FightType.PROFESSIONAL).order_by('-event__date'):
            opponent = fight.fighters.exclude(pk=fighter.pk)[0]
            time = '%d:%.2d' % (int(fight.time) / 60, int(fight.time) % 60)
            is_no_contest = 'win' not in fight.result.lower() and 'loss' not in fight.result.lower()
            fights.append({
                'opponent': opponent,
                'is_winner': Corner.objects.get(fight=fight, fighter=fighter).is_winner and not is_no_contest,
                'is_loser': Corner.objects.get(fight=fight, fighter=opponent).is_winner and not is_no_contest,
                'event': fight.event,
                'date': fight.event.date + timedelta(days=1),
                'round': fight.round,
                'time': time,
                'method': fight.method,
                'result': fight.result,

            })
        context_data['fights'] = fights
        context_data['has_losses_by_other'] = fighter.losses_by_other > 0

        # Height
        if fighter.height:
            height_in_inches = fighter.height * 0.393701
            context_data['height'] = '%d\'%d\"' % (int(height_in_inches / 12), height_in_inches % 12)
        else:
            context_data['height'] = None

        # Weight
        if fighter.weight:
            weight_in_pounds = int(ceil(fighter.weight * 2.20462))
            context_data['weight'] = '%d' % weight_in_pounds
        else:
            context_data['weight'] = None

        # Fuck you Google
        context_data['is_google_a_bag_of_dicks'] = bool(self.request.GET.get('android', None))

        # For the donuts
        if fighter.losses > 0:
            context_data['losses_graph'] = {
                'a': (float(fighter.losses_by_knockout) / fighter.losses),
                'b': (float(fighter.losses_by_submission) / fighter.losses),
                'c': (float(fighter.losses_by_decision) / fighter.losses),
                'd': (float(fighter.losses_by_other) / fighter.losses),
            }
            context_data['losses_graph'].update({
                'b_offset': context_data['losses_graph'].get('a'),
                'c_offset': context_data['losses_graph'].get('a') + context_data['losses_graph'].get('b'),
                'd_offset': context_data['losses_graph'].get('a') + context_data['losses_graph'].get('b') + context_data['losses_graph'].get('c'),
            })
        if fighter.wins > 0:
            context_data['wins_graph'] = {
                'a': (float(fighter.wins_by_knockout) / fighter.wins),
                'b': (float(fighter.wins_by_submission) / fighter.wins),
                'c': (float(fighter.wins_by_decision) / fighter.wins),
                'd': (float(fighter.wins_by_other) / fighter.wins),
            }
            context_data['wins_graph'].update({
                'b_offset': context_data['wins_graph'].get('a'),
                'c_offset': context_data['wins_graph'].get('a') + context_data['wins_graph'].get('b'),
                'd_offset': context_data['wins_graph'].get('a') + context_data['wins_graph'].get('b') + context_data['wins_graph'].get('c'),
            })
        return context_data

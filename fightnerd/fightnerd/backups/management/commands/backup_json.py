from __future__ import absolute_import

import logging

from django.core.management.base import BaseCommand
from django.utils import timezone
from django.core import management

import os
import gzip
from django.conf import settings
from fabric.api import local
from fightnerd.backups.utils import upload


logger = logging.getLogger(__name__)
DATABASE_NAME = getattr(settings, 'DATABASES')['default']['NAME']
DATABASE_HOST = getattr(settings, 'DATABASES')['default']['HOST']
DATABASE_USER = getattr(settings, 'DATABASES')['default']['USER']
DATABASE_PORT = getattr(settings, 'DATABASES')['default']['PORT']
RUN_DIR = getattr(settings, 'RUN_DIR')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def handle(self, *args, **options):
        # Dump database into string buffer
        logger.info('Dumping database...')
        time_text = timezone.now().strftime('%Y-%m-%dT%H:%M:%S%Z')
        json_file_name = '%s.%s.json' % (DATABASE_NAME, time_text)
        json_file_path = os.path.join(RUN_DIR, json_file_name)
        with open(json_file_path, 'w') as json_file:
            management.call_command('dumpdata', indent=4, exclude=['auth.permission', 'contenttypes'], stdout=json_file)

        # Compress database
        logger.info('Compressing database...')
        gzip_file_name = '%s.%s.json.gz' % (DATABASE_NAME, time_text)
        gzip_file_path = os.path.join(RUN_DIR, gzip_file_name)
        gzip_file = gzip.open(gzip_file_path, 'wb')
        with open(json_file_path, 'r') as json_file:
            lines = json_file.readlines()
            for line in lines:
                gzip_file.write(line.encode('utf8'))
        gzip_file.close()
        upload(gzip_file_path, 'json')
        os.remove(gzip_file_path)
        os.remove(json_file_path)

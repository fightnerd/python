from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
from django.conf import settings
from django.core.files.base import ContentFile
from storages.backends.s3boto import S3BotoStorage
import logging

logger = logging.getLogger(__name__)
DATABASE_NAME = getattr(settings, 'DATABASES')['default']['NAME']
DATABASE_HOST = getattr(settings, 'DATABASES')['default']['HOST']
DATABASE_USER = getattr(settings, 'DATABASES')['default']['USER']
DATABASE_PORT = getattr(settings, 'DATABASES')['default']['PORT']
RUN_DIR = getattr(settings, 'RUN_DIR')
BACKUPS_BACKUP_DIRECTORY = getattr(settings, 'BACKUPS_BACKUP_DIRECTORY')
BACKUPS_BACKUP_CLEANUP_LIMIT = getattr(settings, 'BACKUPS_BACKUP_CLEANUP_LIMIT')
AWS_STORAGE_BUCKET_NAME = getattr(settings, 'AWS_STORAGE_BUCKET_NAME')


def listfiles(s3_boto_storage, directory):
    logger.info('Getting list of backups...')
    directories, files = s3_boto_storage.listdir(directory)
    files = [f for f in files if f != '']
    return files


def upload(upload_file_path, dir_name):
    file_size_in_kilobytes = float(os.path.getsize(upload_file_path)) / 1000.0
    with open(upload_file_path, 'rb') as upload_file:
        s3_dir_name = BACKUPS_BACKUP_DIRECTORY + '/%s/' % dir_name

        logger.info('Connecting to S3...')
        s3_boto_storage = S3BotoStorage(acl='private')

        # Check number of backups in S3
        files = listfiles(s3_boto_storage, s3_dir_name)

        # Enforce the limit
        if len(files) >= BACKUPS_BACKUP_CLEANUP_LIMIT:
            for i in range((len(files) - BACKUPS_BACKUP_CLEANUP_LIMIT) + 1):
                file_path = os.path.join(s3_dir_name, files[i])
                logger.info('Deleting %s' % file_path)
                s3_boto_storage.delete(file_path)

        # Push to S3
        logger.info('Uploading %.2f KB to %s:%s' % (file_size_in_kilobytes, AWS_STORAGE_BUCKET_NAME, s3_dir_name))
        s3_boto_storage.save(os.path.join(s3_dir_name, os.path.basename(upload_file_path)), ContentFile(upload_file.read()))

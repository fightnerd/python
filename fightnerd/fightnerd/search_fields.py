from haystack import fields

from django.utils import datetime_safe, six

from haystack.exceptions import SearchFieldError
import pytz
from django.conf import settings

TIME_ZONE = getattr(settings, 'TIME_ZONE')


class TimezoneAwareDateTimeField(fields.DateTimeField):
    def convert(self, value):
        if value is None:
            return None

        if isinstance(value, six.string_types):
            match = fields.DATETIME_REGEX.search(value)

            if match:
                data = match.groupdict()
                return datetime_safe.datetime(int(data['year']), int(data['month']), int(data['day']), int(data['hour']), int(data['minute']), int(data['second']), tzinfo=pytz.timezone(TIME_ZONE))
            else:
                raise SearchFieldError("Datetime provided to '%s' field doesn't appear to be a valid datetime string: '%s'" % (self.instance_name, value))

        return value

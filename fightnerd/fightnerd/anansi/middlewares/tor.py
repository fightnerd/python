from django.template import Context, Template
from django.conf import settings
import tempfile
import shlex
import subprocess
import time
import os
import scrapy
from TorCtl import TorCtl
import logging
from datetime import datetime
from datetime import timedelta


class TorMiddleware(object):
    last_change_ip_time = None
    ip_refresh_period = timedelta(seconds=10)

    def change_ip(self):
        if not os.path.exists('/var/run/tor/control.authcookie'):
            if not os.path.exists('var/run/tor/'):
                os.makedirs('/var/run/tor/')
            open('/var/run/tor/control.authcookie', 'a').close()
        connection = TorCtl.connect(controlAddr=os.environ['TOR_PORT_9051_TCP_ADDR'], controlPort=int(os.environ['TOR_PORT_9051_TCP_PORT']), passphrase=os.environ['TOR_1_ENV_PASSWORD'])
        connection.send_signal("NEWNYM")
        connection.close()

    def process_request(self, request, spider):
        # if self.last_change_ip_time is None or datetime.now() - self.last_change_ip_time > self.ip_refresh_period:
        #     self.change_ip()
        #     self.last_change_ip_time = datetime.now()
        return None
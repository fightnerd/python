import scrapy
import logging
from scrapy.http import Request


class ICanHazIPSpider(scrapy.Spider):
    name = 'icanhazip'
    allowed_domains = ['icanhazip.com']

    def __init__(self, name=None, **kwargs):
        super(ICanHazIPSpider, self).__init__(name, **kwargs)
        self.start_urls = list()
        [self.start_urls.append('http://icanhazip.com/') for i in range(1000)]

    def parse(self, response):
        logging.info('================ %s =================' % response.body.strip())

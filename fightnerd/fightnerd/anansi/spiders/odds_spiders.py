import time
import unicodedata

from scrapy.spider import Spider
from scrapy.spider import Request
from scrapy.selector import Selector
from fightnerd.anansi.items import UpcomingEventItem, UpcomingFightItem, FighterItem
from scrapy.http import FormRequest
import re
from fightnerd.anansi.misc import extract


class FiveDimesSpider(Spider):
    name = 'fivedimes'
    start_urls = ['http://www.5dimes.eu/']

    def parse(self, response):
        form_request = FormRequest.from_response(response,
                                                 formdata={'customerID': '5D2111353', 'password': '7777777'},
                                                 callback=FiveDimesSpider.parse_loading_screen)
        return form_request

    @staticmethod
    def parse_loading_screen(response):
        time.sleep(5)
        return [Request('http://www.5dimes.eu/bbSportSelection.asp', callback=FiveDimesSpider.parse_user_page)]

    @staticmethod
    def parse_user_page(response):
        form_request = FormRequest.from_response(response,
                                                 formdata={'Fighting_UFC': 'on', 'Fighting_Bellator': 'on'},
                                                 callback=FiveDimesSpider.parse_odds)
        return form_request

    @staticmethod
    def parse_odds(response):
        selector = Selector(response)

        # Get event names
        table_rows = selector.xpath('//tr[@class]')
        event_names = []
        event_name = None
        for table_row in table_rows:
            class_name = extract(table_row, './@class')
            if class_name == 'linesSubHeader':
                event_name = extract(table_row, './td/strong/small/text()')
                event_name = event_name.split('-')[0]
                event_name = event_name.strip()
            elif class_name == 'linesRow':
                if event_name:
                    event_names.append(event_name)

        # Get first fighters
        table_rows = selector.xpath('//tr[@class="linesRow"]')
        first_fighters = FiveDimesSpider.get_fighters(table_rows)

        # Get second fighters
        table_rows = selector.xpath('//tr[@class="linesRowBot"]')
        second_fighters = FiveDimesSpider.get_fighters(table_rows)

        upcoming_events = []
        for first_fighter, second_fighter, event_name in zip(first_fighters, second_fighters, event_names):
            upcoming_event = UpcomingEventItem()
            upcoming_fight = UpcomingFightItem()
            upcoming_event['name'] = event_name
            upcoming_event['upcoming_fights'] = []
            upcoming_fight['first_fighter'] = first_fighter[0]
            upcoming_fight['first_fighter_odds'] = first_fighter[1]
            upcoming_fight['second_fighter'] = second_fighter[0]
            upcoming_fight['second_fighter_odds'] = second_fighter[1]
            upcoming_event['upcoming_fights'].append(upcoming_fight)
            upcoming_events.append(upcoming_event)
        return upcoming_events

    @staticmethod
    def strip_accents(s):
        return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

    @staticmethod
    def get_fighters(table_rows):
        fighters = []
        for table_row in table_rows:
            table_data = table_row.xpath('./td')
            full_name = table_data[2]
            odds = table_data[4]
            full_name = extract(full_name, './text()')
            full_name = re.sub('\\xa0', ' ', full_name)
            full_name = ' '.join(full_name.split()[1:])
            fighter_item = FighterItem()
            fighter_item['full_name'] = FiveDimesSpider.strip_accents(full_name)
            fighter_item['is_partial'] = True
            odds = int(extract(odds, './text()'))
            fighter = (fighter_item, odds)
            fighters.append(fighter)
        return fighters

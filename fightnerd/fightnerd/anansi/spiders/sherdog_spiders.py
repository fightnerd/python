import datetime
from urlparse import urljoin

from scrapy.contrib.spiders import CrawlSpider
from scrapy.contrib.spiders import Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.selector import HtmlXPathSelector
from fightnerd.anansi.items import FighterItem
from fightnerd.anansi.items import UpcomingEventItem
from fightnerd.anansi.items import UpcomingFightItem
from fightnerd.anansi.items import FightItem
from fightnerd.anansi.items import RefereeItem
from fightnerd.anansi.items import EventItem
from fightnerd.anansi.items import AssociationItem
from scrapy.http import Request
import re
from fightnerd.anansi.misc import MONTH_ABBR_TO_NUMBER
from fightnerd.anansi.misc import extract
from fightnerd.anansi.misc import get_date


SHERDOG_URL = 'http://www.sherdog.com'
SHERDOG_DOMAIN = 'www.sherdog.com'
GOOGLE_CACHE_DOMAIN = 'webcache.googleusercontent.com'


class SherdogUpcomingEventSpider(CrawlSpider):
    name = 'sherdog_upcoming_events'
    fighter_link_extractor = SgmlLinkExtractor(allow=('fighter/\w*-.*-\d{1,10}$', ))
    start_urls = [urljoin(SHERDOG_URL, '/events/upcoming/1-page'),
                  urljoin(SHERDOG_URL, '/events/upcoming/2-page'),
                  urljoin(SHERDOG_URL, '/events/upcoming/3-page'),
                  urljoin(SHERDOG_URL, '/events/upcoming/4-page'),
                  urljoin(SHERDOG_URL, '/events/upcoming/5-page')]
    allowed_domains = [SHERDOG_DOMAIN, GOOGLE_CACHE_DOMAIN]
    rules = (
        # Extract links matching 'http://www.sherdog.com/events/upcoming/2-page'
        # This link will point to the next page on the database
        # There is no callback for this rule because I actually want to scan the page for event links
        # Rule(SgmlLinkExtractor(allow=('upcoming/\d{1,5}-page', ))),

        # Extract links matching 'http://www.sherdog.com/events/BFC-Bellator-Fighting-Championships-88-27085'
        # These links will take me to a particular event's data
        # Since this rule has a callback, NO other links will be extracted...e
        Rule(SgmlLinkExtractor(allow=('events/.*?-\d{1,5}', )), callback='parse_upcoming_event'),
    )

    @staticmethod
    def extract_fighter_links(response, callback):
        links = SherdogUpcomingEventSpider.fighter_link_extractor.extract_links(response)
        requests = []
        for l in links:
            request = Request(url=l.url, callback=callback)
            requests.append(request)
        return requests

    @staticmethod
    def parse_fighter(url, full_name):
        fighter = FighterItem()
        toks = re.split('/', url)
        toks = re.split('-', toks[2])
        fighter['sherdog_id'] = toks[-1]
        fighter['full_name'] = full_name
        fighter['is_partial'] = True
        return fighter

    @staticmethod
    def parse_upcoming_event(response):
        upcoming_event_item = UpcomingEventItem()
        upcoming_event_item['upcoming_fights'] = []
        hxs = HtmlXPathSelector(response)

        # Let's parse the sherdogId right out of the url
        toks = re.split('/', response.url)
        toks = re.split('-', toks[4])
        upcoming_event_item['sherdog_id'] = toks[-1]

        # Name
        upcoming_event_item['name'] = extract(hxs, '//span[contains(@itemprop,"name")]/text()')

        # Date
        try:
            date_format = "%Y-%m-%d"
            upcoming_event_item['date'] = get_date(
                extract(hxs, '//meta[contains(@itemprop,"startDate")]/@content')).strftime(
                date_format) + "T00:00:00 UTC"
        except:
            upcoming_event_item['date'] = None

        # Location, location, location...
        upcoming_event_item['location'] = extract(hxs,
                                                  '//span[contains(@itemprop,"location")]/text()')  # This string will contain arena, city, state, country

        # Organization
        upcoming_event_item['organization'] = extract(hxs, '//a/strong/span[contains(@itemprop,"name")]/text()')

        # Main Event
        # Start with the left side...
        url = extract(hxs, '//div[contains(@class,"fighter left_side")]/h3/a/@href')
        if not url or 'void' in url:  # Sometimes events have no fights scheduled...
            return None
        full_name = extract(hxs, '//div[contains(@class,"fighter left_side")]/h3/a/span/text()')
        first_fighter = SherdogUpcomingEventSpider.parse_fighter(url, full_name)

        # Get the right side fighter
        url = extract(hxs, '//div[contains(@class,"fighter right_side")]/h3/a/@href')
        if not url or 'void' in url:  # And sometimes the second fighter is unknown...
            return None
        full_name = extract(hxs, '//div[contains(@class,"fighter right_side")]/h3/a/span/text()')
        second_fighter = SherdogUpcomingEventSpider.parse_fighter(url, full_name)

        upcoming_fight = UpcomingFightItem()
        upcoming_fight['first_fighter'] = first_fighter
        upcoming_fight['second_fighter'] = second_fighter
        upcoming_fight['order'] = 0
        upcoming_event_item['upcoming_fights'].append(upcoming_fight)

        # Let's get the rest of the card...
        order = 1
        for t in hxs.xpath('//tr[contains(@itemprop,"subEvent")]'):
            # For the first fighter...
            fighter_tags = t.xpath('./td[contains(@itemprop,"performer")]')
            url = extract(fighter_tags[0], './a/@href')  # get sherdogId
            if not url:
                url = extract(fighter_tags[0], './div/a/@href')
            if 'void' in url:
                return None
            full_name = extract(fighter_tags[0], './a/span/text()')  # get name
            if not full_name:
                full_name = extract(fighter_tags[0], './div/a/span/text()')
            first_fighter = SherdogUpcomingEventSpider.parse_fighter(url, full_name)

            # For the second fighter...
            url = extract(fighter_tags[1], './a/@href')  # get sherdogId
            if not url:
                url = extract(fighter_tags[1], './div/a/@href')
            if 'void' in url:
                return None
            full_name = extract(fighter_tags[1], './a/span/text()')  # get name
            if not full_name:
                full_name = extract(fighter_tags[1], './div/a/span/text()')
            second_fighter = SherdogUpcomingEventSpider.parse_fighter(url, full_name)

            upcoming_fight = UpcomingFightItem()
            upcoming_fight['first_fighter'] = first_fighter
            upcoming_fight['second_fighter'] = second_fighter
            upcoming_fight['order'] = order
            order += 1
            upcoming_event_item['upcoming_fights'].append(upcoming_fight)

        ret = [upcoming_event_item]
        ret += SherdogUpcomingEventSpider.extract_fighter_links(response, SherdogFighterSpider.parse_fighter)
        return ret


class SherdogFighterSpider(CrawlSpider):
    name = 'sherdog_fighters'
    valid_weight_classes = (['atomweight',
                             'strawweight',
                             'flyweight',
                             'bantamweight',
                             'featherweight',
                             'lightweight',
                             'welterweight',
                             'middleweight',
                             'light heavyweight',
                             'heavyweight'])
    start_urls = [
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=15&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=13&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=10&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=9&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=8&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=7&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=6&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=5&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=4&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=3&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=&weight=2&association='),

        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=juliana+lima&weight=&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=jessica+rakoczy&weight=&association='),
        urljoin(SHERDOG_URL, '/stats/fightfinder?SearchTxt=ashlee&weight=&association=')
    ]
    # urljoin(SHERDOG_URL,'/stats/fightfinder?SearchTxt=sadollah&weight=&association=')]
    #                  urljoin(SHERDOG_URL,'?SearchTxt=&weight=1&association=')]
    allowed_domains = [SHERDOG_DOMAIN, GOOGLE_CACHE_DOMAIN]

    rules = (
        # Extract links matching 'http://www.sherdog.com/stats/fightfinder.php?association=&weight=10&SearchTxt=&page=2'
        # This link will point to the next page on the database
        # There is no callback for this rule because I actually want to scan the page for fighter links
        Rule(SgmlLinkExtractor(allow=('SearchTxt=&page=\d{1,5}', ))),

        # Extract links matching 'http://www.sherdog.com/fighter/Janailson-Kevin-Pereira-Lima-25614'
        # These links will take me to a particular fighter's data
        # I assume that each fighter link ends with a number that is 1 to 10 digits...
        # Since this rule has a callback, NO other links will be extracted...e
        Rule(SgmlLinkExtractor(allow=('fighter/\w*-.*-\d{1,10}$', )), callback='parse_fighter'),

    )

    @staticmethod
    def parse_fight_history(selector):
        fights = []
        for t in selector.xpath('//div[contains(@class,"module fight_history")]'):
            # Figure out what type of fight this is
            fight_type = extract(t, './div[contains(@class,"module_header")]/h2/text()')
            fight_type = fight_type.lower()
            if 'upcoming fights' in fight_type:
                fight_type = 'upcoming'
            elif 'fight history' in fight_type:
                fight_type = 'professional'
            elif 'amateur fights' in fight_type:
                fight_type = 'amateur'

            # Get professional/amateur fights...
            if fight_type == 'professional' or fight_type == 'amateur':
                for u in t.xpath('./div[contains(@class,"content table")]/table/tr'):
                    if u.xpath('./tr[contains(@class,"table_head")]'):
                        continue
                    fight = FightItem()
                    fight['type'] = fight_type

                    # Grab all the data in one row
                    table_data = u.xpath('./td')

                    # Get result
                    fight['result'] = extract(table_data[0], './span/text()')

                    # Get opponent (ex: /fighter/Brock-Lesnar-17522)
                    fighter_item = FighterItem()
                    fighter_item['is_partial'] = True
                    # If I can't find an opponent skip this row...
                    try:
                        url = table_data[1].xpath('./a/@href').extract()[0]
                    except:
                        continue

                    # Let's parse the firstName, lastName, and sherdogId right out of the url
                    toks = re.split('/', url)
                    try:
                        toks = re.split('-', toks[2])
                        fighter_item['sherdog_id'] = toks[-1]
                        full_name = table_data[1].xpath('./a/text()').extract()[0]
                        fighter_item['full_name'] = full_name
                    except:
                        fighter_item['sherdog_id'] = '666'
                        fighter_item['full_name'] = 'Unknown Fighter'
                    fight['opponent'] = fighter_item

                    # Get event (ex: /events/UFC-141-Lesnar-vs-Overeem-18346)
                    event = EventItem()
                    # If I can't find an event skip this row...
                    try:
                        url = table_data[2].xpath('./a/@href').extract()[0]
                    except:
                        continue
                    toks = re.split('/', url)
                    toks = re.split('-', toks[2])
                    event['name'] = None
                    try:
                        event['name'] = table_data[2].xpath('./a/span/text()').extract()[0]
                    except:
                        event['name'] = table_data[2].xpath('./a/text()').extract()[0]
                    event['sherdog_id'] = toks[-1]

                    # Get the date for this event...
                    toks = extract(table_data[2], './span/text()')
                    toks = re.split(' |/', toks)
                    date_format = '%Y-%m-%d'
                    event['date'] = datetime.date(int(toks[6]),
                                                  MONTH_ABBR_TO_NUMBER[toks[0].lower()],
                                                  int(toks[3])).strftime(date_format) + 'T00:00:00 UTC'
                    fight['event'] = event

                    # Get method
                    fight['method'] = extract(table_data[3], './text()')
                    if not fight['method']:
                        fight['method'] = 'N/A'

                    # Get referee
                    full_name = extract(table_data[3], './span/text()')
                    fight['referee'] = None
                    if full_name != 'N/A':
                        referee = RefereeItem()
                        referee['full_name'] = full_name
                        fight['referee'] = referee

                    # Get round
                    fight['round'] = int(extract(table_data[4], './text()'))

                    # Get time
                    # Final value should be in seconds...
                    toks = extract(table_data[5], './text()')
                    fight['time'] = -1
                    toks = re.compile('(\d+):(\d+)').match(toks)
                    if toks:
                        fight['time'] = int(toks.group(1)) * 60 + int(toks.group(2))

                    # Save this fight
                    fights.append(fight)
        return fights

    @staticmethod
    def parse_fighter(response):
        fighter_item = FighterItem()
        associations = []
        selector = Selector(response)

        # Let's parse the sherdogId right out of the url
        toks = re.split('/', response.url)
        toks = re.split('-', toks[4])
        fighter_item['sherdog_id'] = toks[-1]

        # full_name
        fighter_item['full_name'] = extract(selector, '//span[contains(@class,"fn")]/text()')

        # Nickname
        fighter_item['nickname'] = extract(selector, '//span[contains(@class,"nickname")]/em/text()')

        # DOB
        try:
            date_format = "%Y-%m-%d"
            fighter_item['date_of_birth'] = get_date(
                extract(selector, '//span[contains(@itemprop,"birthDate")]/text()')).strftime(
                date_format) + "T00:00:00 UTC"
        except:
            fighter_item['date_of_birth'] = None

        # City, State
        toks = extract(selector,
                       '//span[contains(@itemprop,"addressLocality")]/text()')  # This string will contain city, state
        if toks:
            toks = re.split(',', toks)
            if len(toks) > 1:
                fighter_item['city'] = toks[0]
                fighter_item['state'] = toks[1][1:]
            else:
                fighter_item['state'] = toks[0]
                fighter_item['city'] = None

        # Country
        fighter_item['country'] = extract(selector, '//strong[contains(@itemprop,"nationality")]/text()')

        # Height
        toks = extract(selector,
                       '//span[contains(@class,"item height")]/strong/text()')  # Grabbed the height in feet and inches then convert to cm
        toks = re.split(r"""'|\"""", toks)
        fighter_item['height'] = (12.0 * float(toks[0]) + float(toks[1])) * 2.54
        if fighter_item['height'] == 0:
            fighter_item['height'] = None

        # Weight
        toks = extract(selector,
                       '//span[contains(@class,"item weight")]/strong/text()')  # Grabbed the weight in pounds then convert to kg
        toks = re.split(r"""\s""", toks)
        fighter_item['weight'] = float(toks[0]) * 0.453592

        # Weight class
        fighter_item['weight_class'] = extract(selector, '//h6[contains(@class,"item wclass")]/strong/text()')
        if fighter_item['weight_class'] and 'weight' not in fighter_item['weight_class'].lower():
            fighter_item['weight_class'] = None

        # If the fighter does not have a weight class...I won't push the item or extract links...
        if not fighter_item['weight_class'] or not fighter_item['weight_class'].lower() in SherdogFighterSpider.valid_weight_classes:
            return None

        # Image url
        image_url = extract(selector, '//img[contains(@class,"profile_image photo")]/@src')
        fighter_item['image_urls'] = []
        if image_url and 'default' not in image_url:
            fighter_item['image_urls'].append(image_url)

        # Wins by KO/TKO
        i = 0
        tags = selector.xpath('//div[@class="bio_graph"]/span[@class="graph_tag"]/text()')
        found_others = len(tags) == 8
        # print len(tags)
        fighter_item['wins_by_knockout'] = 0
        fighter_item['wins_by_submission'] = 0
        fighter_item['wins_by_decision'] = 0
        fighter_item['wins_by_other'] = 0
        for t in tags:
            toks = t.extract()
            if toks == ')':
                continue
            if found_others:
                if i == 0:
                    fighter_item['wins_by_knockout'] = int(toks.split(' ')[0])
                elif i == 1:
                    fighter_item['wins_by_submission'] = int(toks.split(' ')[0])
                elif i == 2:
                    fighter_item['wins_by_decision'] = int(toks.split(' ')[0])
                elif i == 3:
                    fighter_item['wins_by_other'] = int(toks.split(' ')[0])
            else:
                if i == 0:
                    fighter_item['wins_by_knockout'] = int(toks.split(' ')[0])
                elif i == 1:
                    fighter_item['wins_by_submission'] = int(toks.split(' ')[0])
                elif i == 2:
                    fighter_item['wins_by_decision'] = int(toks.split(' ')[0])
            i += 1

        # Losses by KO/TKO
        i = 0
        tags = selector.xpath('//div[@class="bio_graph loser"]/span[@class="graph_tag"]/text()')
        found_others = len(tags) == 8
        fighter_item['losses_by_knockout'] = 0
        fighter_item['losses_by_submission'] = 0
        fighter_item['losses_by_decision'] = 0
        fighter_item['losses_by_other'] = 0
        for t in tags:
            toks = t.extract()
            if toks == ')':
                continue
            if found_others:
                if i == 0:
                    fighter_item['losses_by_knockout'] = int(toks.split(' ')[0])
                elif i == 1:
                    fighter_item['losses_by_submission'] = int(toks.split(' ')[0])
                elif i == 2:
                    fighter_item['losses_by_decision'] = int(toks.split(' ')[0])
                elif i == 3:
                    fighter_item['losses_by_other'] = int(toks.split(' ')[0])
            else:
                if i == 0:
                    fighter_item['losses_by_knockout'] = int(toks.split(' ')[0])
                elif i == 1:
                    fighter_item['losses_by_submission'] = int(toks.split(' ')[0])
                elif i == 2:
                    fighter_item['losses_by_decision'] = int(toks.split(' ')[0])
            i += 1

        # Draws and no contests
        fighter_item['draws'] = 0
        fighter_item['no_contests'] = 0
        result_type = extract(selector,
                              '//div[contains(@class,"right_side")]/div[contains(@class,"bio_graph")]/span[contains(@class,"card")]/span[contains(@class,"result")]/text()')
        if result_type == "Draws":
            fighter_item['draws'] = int(extract(selector,
                                                '//div[contains(@class,"right_side")]/div[contains(@class,"bio_graph")]/span[contains(@class,"card")]/span[contains(@class,"counter")]/text()'))
        elif result_type == "N/C":
            fighter_item['no_contests'] = int(extract(selector,
                                                      '//div[contains(@class,"right_side")]/div[contains(@class,"bio_graph")]/span[contains(@class,"card")]/span[contains(@class,"counter")]/text()'))

        # Associations
        for t in selector.xpath('//span[contains(@itemprop,"memberOf")]'):
            association_item = AssociationItem()
            association_item['name'] = extract(t, './a/span/text()')
            associations.append(association_item)
        fighter_item['associations'] = associations

        # Fight History
        fighter_item['fights'] = SherdogFighterSpider.parse_fight_history(selector)

        # Is whole...?
        fighter_item['is_partial'] = False

        return [fighter_item]

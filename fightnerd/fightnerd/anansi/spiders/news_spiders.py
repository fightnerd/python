from HTMLParser import HTMLParser
import datetime

from scrapy.spider import Spider
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import XMLFeedSpider
from scrapy.contrib.spiders import CrawlSpider
from scrapy.contrib.spiders import Rule
from fightnerd.anansi.items import PublicationItem
from fightnerd.anansi.items import ArticleItem
from fightnerd.anansi.items import AuthorItem
from fightnerd.anansi.misc import extract
from bs4 import BeautifulSoup, Tag, NavigableString, Comment
from scrapy import Selector
from scrapy import Request
import dateutil
from dateutil import parser as date_parser
from scrapy.contrib.linkextractors import LinkExtractor
import re
from selenium.webdriver.phantomjs.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
import time
import logging
from django.utils import html
import re
from django.utils import timezone

logger = logging.getLogger(__name__)


PUBLICATION_NAMES = dict()
PUBLICATION_NAMES['Bloody Elbow -  All Posts'] = 'Bloody Elbow'
PUBLICATION_NAMES['MMA Fighting -  All Posts'] = 'MMA Fighting'
PUBLICATION_NAMES['MMAmania.com -  All Posts'] = 'MMA Mania'


def clean_src(content):
    content = content.replace('src="//www.youtube.com/', 'src="http://www.youtube.com/')
    content = content.replace('src="//instagram.com/', 'src="http://www.instagram.com/')
    return content


def clean_font_family_divs(html):
    soup = BeautifulSoup(html)
    for div in soup.find_all('div'):
        if 'style' in div.attrs:
            div['style'] = re.sub('Times New Roman', 'HelveticaNeue', div['style'])
    return soup.body.contents[0].prettify()


def clean_floating_divs(html):
    soup = BeautifulSoup(html)
    for div in soup.find_all('div'):
        if 'style' in div.attrs:
            div['style'] = re.sub('float:right; ', '', div['style'])
    return soup.body.contents[0].prettify()


def clean_trailing_br(html):
    soup = BeautifulSoup(html)
    div = soup.html.body.div
    last_tag = div.find_all()[-1]
    while last_tag and last_tag.name == u'br':
        last_tag.extract()
        last_tag = div.find_all()[-1]
    return soup.body.contents[0].prettify()


def replace_div_by_style(html, style, replacement):
    soup = BeautifulSoup(html)
    for div in soup.find_all('div'):
        if 'style' in div.attrs and div['style'] == style:
            if replacement:
                del div['style']
                for k in replacement:
                    div[k] = replacement[k]
    return soup.body.contents[0].prettify()


def clean_br_hr_br(html):
    soup = BeautifulSoup(html)
    prev_tag = None
    prev_prev_tag = None
    for tag in soup.find_all():
        if tag and prev_tag and prev_prev_tag:
            is_tag_valid = tag and ((isinstance(tag, NavigableString) and tag.string == u'br') or (isinstance(tag, Tag) and tag.name == u'br'))
            is_prev_tag_valid = prev_tag and ((isinstance(prev_tag, NavigableString) and prev_tag.string == u'hr') or (isinstance(prev_tag, Tag) and prev_tag.name == u'hr'))
            is_prev_prev_tag_valid = prev_prev_tag and ((isinstance(prev_prev_tag, NavigableString) and prev_prev_tag.string == u'br') or (isinstance(prev_prev_tag, Tag) and prev_prev_tag.name == u'br'))
            is_valid = is_tag_valid and is_prev_tag_valid and is_prev_prev_tag_valid
            if is_valid:
                prev_tag.extract()
                prev_prev_tag.extract()
                tag.extract()
                prev_tag = None
                prev_prev_tag = None
        prev_prev_tag = prev_tag
        prev_tag = tag
    return soup.body.contents[0].prettify()


def clean_img(html):
    soup = BeautifulSoup(html)
    for img in soup.find_all('img'):
        if 'style' in img.attrs:
            del img.attrs['style']
    return soup.body.contents[0].prettify()


def clean_scripts(soup):
    for script in soup.find_all('script'):
        if 'src' in script.attrs:
            script.attrs['src'] = re.sub('^//platform.twitter.com/', 'http://platform.twitter.com/', script.attrs['src'])
            # if 'player.espn.com' in script['src']:
            #     toks = script['src'].split('&')
            #     for tok in toks:
            #         if 'width' in tok:
            #             toks.remove(tok)
            #             break
            #     script['src'] = '&'.join(toks)
    return soup


def clean_iframes(soup):
    for iframe in soup.find_all('iframe'):
        is_youtube = False
        is_instagram = False
        if 'frameborder' in iframe.attrs:
            del iframe.attrs['frameborder']
        if 'width' in iframe.attrs:
            del iframe.attrs['width']
        if 'height' in iframe.attrs:
            del iframe.attrs['height']
        if 'src' in iframe.attrs:
            iframe.attrs['src'] = re.sub('^//www.youtube.com/', 'http://www.youtube.com/', iframe.attrs['src'])
            iframe.attrs['src'] = re.sub('^//instagram.com/', 'http://www.instagram.com/', iframe.attrs['src'])
            if 'youtube' in iframe.attrs['src']:
                is_youtube = True
            elif 'instagram' in iframe.attrs['src']:
                is_instagram = True
        div_class = ''
        if is_instagram:
            div_class = 'embed-instagram-container'
        elif is_youtube:
            div_class = 'embed-youtube-container'
            iframe['class'] = 'video'
        div = iframe.parent
        div.attrs['class'] = div_class
    return soup


def clean_imgs(soup):
    for img in soup.find_all('img'):
        img['style'] = None
        img['class'] = 'img-responsive'
        img['width'] = None
        img['height'] = None
        img.parent['style'] = None
        toks = img['src'].split('?')
        img['src'] = toks[0]
    return soup


class XMLNewsSpider(XMLFeedSpider):
    name = 'xmlnews'
    start_urls = None
    itertag = 'item'
    publication_name = None
    namespaces = [('dc', 'http://mmajunkie.com/news/feed/')]

    def parse_article(self, response):
        publication = response.meta['publication']
        return None

    def parse_node(self, response, node):
        publication = PublicationItem()
        publication['name'] = self.publication_name
        publication['articles'] = list()
        article = ArticleItem()

        # Get title
        title = extract(node, './title/text()')
        title = HTMLParser().unescape(title)
        article['title'] = title

        # Get author
        name = extract(node, './author/text()')
        if not name:
            name = re.split(re.escape('</dc:creator>'), re.split(re.escape('<dc:creator>'), node.extract())[1])[
                0].strip()
        if name:
            author = AuthorItem()
            author['name'] = name
            article['author'] = author

        # Get date
        date = extract(node, './pubDate/text()')
        date = re.split(', ', date)[1]
        date = date_parser.parse(date)
        date = date.astimezone(dateutil.tz.tzutc())
        date_format = '%Y-%m-%dT%H:%M:%S UTC'
        date = min(timezone.now(), date)
        date = date.strftime(date_format)
        article['date'] = date

        # Get url
        link = extract(node, './link/text()')
        article['source_url'] = link

        publication['articles'].append(article)

        # Let's build a request that will eventually fetch the most awesomest imagery!!!
        request = Request(article['source_url'], callback=self.parse_article)
        request.meta['publication'] = publication
        return request


class MMAJunkieSpiderMixin(object):
    publication_name = 'MMA Junkie'

    @staticmethod
    def clean_content(content, image_url):
        soup = BeautifulSoup(content)
        for div in soup.find_all('div'):
            if div.attrs and 'class' in div.attrs:
                if 'OUTBRAIN' in div['class']:
                    div.extract()
        for script in soup.find_all('script'):
            script.extract()
        for a in soup.find_all('a'):
            if a.attrs and 'class' in a.attrs:
                if 'edit-btn' in a['class']:
                    a.extract()

        soup = clean_iframes(soup)
        soup = clean_scripts(soup)
        soup = clean_imgs(soup)

        html_parser = HTMLParser()
        for img in soup.find_all('img'):
            img['class'] = 'img-responsive'
            src = html_parser.unescape(img['src'])
            src = src.replace('https', 'http')
            toks = src.split('?')
            src = toks[0]
            if src == image_url.replace('https', 'http'):
                img.parent.extract()
                break

        content = soup.body.contents[0].prettify()
        content = clean_img(content)
        return clean_src(content)

    def parse_content(self, selector, article_item):
        article_item['video_url'] = None
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        article_item['content'] = extract(selector, '//div[contains(@class,"articleBody")]')
        article_item['content'] = self.clean_content(article_item['content'], article_item['image_url'])
        if article_item['image_url']:
            article_item['image_url'] = article_item['image_url'].replace('https', 'http')
        return article_item


class MMAJunkieXMLNewsSpider(MMAJunkieSpiderMixin, XMLNewsSpider):
    name = 'mma_junkie_xml_news'
    start_urls = ['http://mmajunkie.com/news/feed/']

    def parse_article(self, response):
        selector = Selector(response)
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]
        self.parse_content(selector, article_item)
        return publication_item


class MMAJunkieNewsSpider(MMAJunkieSpiderMixin, CrawlSpider):
    name = 'mma_junkie_news'
    allowed_domains = ['mmajunkie.com']
    rules = (
        # Extract links matching 'http://www.sherdog.com/news/news/UFC-183-Results-Silva-vs-Diaz-PlaybyPlay-Updates-80853'
        # These links will take me to a particular article
        Rule(SgmlLinkExtractor(allow='\d{4}/\d{2}/.*', restrict_xpaths='//article'), callback='parse_article'),
    )

    def start_requests(self):
        requests = list()
        for i in range(1, 3000):
            requests.append(Request('http://mmajunkie.com/page/%d' % i))
        return requests

    def parse_article(self, response):
        selector = Selector(response)

        if 'mmajunkie.com/rankings' in response.url:
            return None

        # Build the publication
        publication_item = PublicationItem()
        publication_item['name'] = self.publication_name
        publication_item['articles'] = list()

        # Build article
        article_item = ArticleItem()

        # Get the date
        date = extract(selector, '//p[@itemprop="author"]/span/text()')
        article_item['date'] = '%s UTC' % date

        # Get the title
        article_item['title'] = HTMLParser().unescape(extract(selector, '//h1[@class="entry-title"]/text()'))

        # Get the content
        self.parse_content(selector, article_item)

        # Get the url
        article_item['source_url'] = response.url

        # Build the author
        name = extract(selector, '//a[@rel="author"]/text()')
        if name:
            author_item = AuthorItem()
            author_item['name'] = name
            article_item['author'] = author_item

        publication_item['articles'].append(article_item)
        return publication_item


class CagePotatoSpiderMixin(object):
    @staticmethod
    def clean_content(content):
        bad_classes = ['tags_list', 'social']
        content = clean_img(content)
        soup = BeautifulSoup(content)
        for div in soup.find_all('div'):
            div['style'] = None
            if div.attrs:
                if 'class' in div.attrs:
                    if div['class'][0] in bad_classes:
                        div.extract()
        soup = clean_iframes(soup)
        soup = clean_scripts(soup)
        soup = clean_imgs(soup)

        # Remove main image
        for img in soup.select('div.post_excerpt > p > a > img'):
            img.parent.parent.extract()

        # Remove main video
        for iframe in soup.select('div.post_excerpt > p > iframe'):
            iframe.parent.extract()
            break

        # Clean iframes
        for iframe in soup.find_all('iframe'):
            if 'streamable' in iframe['src']:
                iframe['style'] = None
                iframe['class'] = 'video'
                if iframe.parent and iframe.parent.parent:
                    iframe.parent['style'] = None
                    iframe.parent.parent['style'] = None

        # Remove byline
        for a in soup.find_all('a'):
            if a.contents and 'By ' in a.contents[0]:
                print a
                a.extract()
                break

        return clean_src(soup.body.contents[0].prettify())

    def parse_content(self, selector, article_item):
        article_item['video_url'] = extract(selector, '//div[contains(@class,"post_excerpt")]/p/iframe/@src')
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        article_item['content'] = extract(selector, '//div[contains(@class,"post_excerpt")]')
        article_item['content'] = self.clean_content(article_item['content'])
        if not article_item['image_url'] and article_item['video_url'] and 'youtube' in article_item['video_url']:
            toks = re.split('embed/|\?', article_item['video_url'])
            if len(toks) > 1:
                article_item['image_url'] = 'http://img.youtube.com/vi/%s/0.jpg' % toks[1]


class CagePotatoXMLNewsSpider(CagePotatoSpiderMixin, XMLNewsSpider):
    name = 'cage_potato_xml_news'
    start_urls = ['http://www.cagepotato.com/feed/']
    publication_name = 'Cage Potato'

    def parse_article(self, response):
        selector = Selector(response)
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]
        self.parse_content(selector, article_item)
        return publication_item


# class CagePotatoNewsSpider(CagePotatoSpiderMixin, CrawlSpider):
# name = 'cage_potato_news'
#     publication_name = 'Cage Potato'
#     start_urls = ['http://www.cagepotato.com']
#     allowed_domains = ['cagepotato.com']
#     rules = (
#         # Extract links matching 'http://www.sherdog.com/news/news/list/2'
#         # This link will point to the next in the archive
#         # There is no callback for this rule because I actually want to scan the page for article links
#         Rule(SgmlLinkExtractor(allow='page/\d{1,3}')),
#
#         # Extract links matching 'http://www.sherdog.com/news/news/UFC-183-Results-Silva-vs-Diaz-PlaybyPlay-Updates-80853'
#         # These links will take me to a particular article
#         Rule(SgmlLinkExtractor(allow='.*', restrict_xpaths='//a[@class="read_more"]'), callback='parse_article'),
#     )
#
#     def parse_article(self, response):
#         selector = Selector(response)
#
#         # Build the publication
#         publication_item = PublicationItem()
#         publication_item['name'] = self.publication_name
#         publication_item['articles'] = list()
#
#         # Build article
#         article_item = ArticleItem()
#
#         # Get the date
#         date = extract(selector, '//div[@class="authors_info"]/span[@class="date"]/text()')
#         article_item['date'] = '%s 00:00:00 UTC' % date
#
#         # Get the title
#         article_item['title'] = HTMLParser().unescape(extract(selector, '//h1[@class="article-title"]/a/text()'))
#
#         # Get the content
#         self.parse_content(selector, article_item)
#
#         # Get the url
#         article_item['source_url'] = response.url
#
#         # Build the author
#         name = extract(selector, '//div[@class="authors_info"]/span[@class="author"]/a/text()')
#         if name:
#             author_item = AuthorItem()
#             author_item['name'] = name
#             article_item['author'] = author_item
#
#         publication_item['articles'].append(article_item)
#         print publication_item
#         return None
#         return publication_item


class SherdogSpiderMixin(object):
    publication_name = 'Sherdog'

    @staticmethod
    def clean_content(content, video_url):
        content = clean_br_hr_br(content)
        content = replace_div_by_style(content, u'float:right; font-family:Times New Roman; font-size: 12px',
                                       {'id': 'byline'})
        content = clean_trailing_br(content)
        content = clean_floating_divs(content)
        content = clean_font_family_divs(content)
        content = clean_img(content)

        soup = BeautifulSoup(content)

        for div in soup.find_all('div'):
            if div.attrs:
                if 'style' in div.attrs:
                    if 'font-family:HelveticaNeue' in div['style']:
                        div.extract()
                if 'id' in div.attrs:
                    if 'byline' in div['id']:
                        div.extract()

        soup = clean_iframes(soup)
        soup = clean_scripts(soup)
        soup = clean_imgs(soup)

        # Remove comments
        for comment in soup.find_all(text=lambda text: isinstance(text, Comment)):
            comment.extract()

        # Remove video url
        for iframe in soup.find_all('iframe'):
            if iframe['src'] == video_url:
                iframe.parent.extract()
                break

        # Remove leading br
        content = soup.body.contents[0].prettify()
        content = re.sub('^<div class="content body_content">\s*(<br/*>\s*)+', '<div class="content body_content">', content)

        return clean_src(content)

    def parse_iframe(self, response):
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]
        selector = Selector(response)
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        return publication_item

    def parse_content(self, selector, article_item):
        article_item['video_url'] = extract(selector, '//div[contains(@class,"content body_content")]//iframe/@src')
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        article_item['content'] = extract(selector, '//div[contains(@class,"content body_content")]')
        article_item['content'] = self.clean_content(article_item['content'], article_item['video_url'])
        if article_item['video_url'] and 'youtube' in article_item['video_url']:
            toks = re.split('embed/|\?', article_item['video_url'])
            if len(toks) > 1:
                article_item['image_url'] = 'http://img.youtube.com/vi/%s/0.jpg' % toks[1]
        return article_item


class SherdogXMLNewsSpider(SherdogSpiderMixin, XMLNewsSpider):
    name = 'sherdog_xml_news'
    start_urls = ['http://www.sherdog.com/rss/news2.xml',
                  'http://www.sherdog.com/rss/articles2.xml']

    def parse_article(self, response):
        selector = Selector(response)
        publication = response.meta['publication']
        article = publication['articles'][0]
        self.parse_content(selector, article)
        if article['video_url'] and not article['image_url']:
            request = Request(article['video_url'], callback=self.parse_iframe)
            request.meta['publication'] = publication
            return request
        return publication

    def parse_node(self, response, node):
        request = super(SherdogXMLNewsSpider, self).parse_node(response, node)
        try:
            article = request.meta['publication']['articles'][0]
            author = article['author']
            name = author['name']
            pattern = re.compile('.*@sherdog.com \((.*)\)')
            match_object = pattern.match(name)
            if match_object:
                author['name'] = match_object.group(1)
                if '@sherdog.com' in author['name']:
                    article['author'] = None
        except Exception:
            pass
        return request


class SherdogNewsSpider(SherdogSpiderMixin, CrawlSpider):
    name = 'sherdog_news'
    start_urls = ['http://www.sherdog.com/news/news/list']
    allowed_domains = ['www.sherdog.com']
    rules = (
        # Extract links matching 'http://www.sherdog.com/news/news/list/2'
        # This link will point to the next in the archive
        # There is no callback for this rule because I actually want to scan the page for article links
        Rule(SgmlLinkExtractor(allow=('news/news/list/\d{1,2}', ))),

        # Extract links matching 'http://www.sherdog.com/news/news/UFC-183-Results-Silva-vs-Diaz-PlaybyPlay-Updates-80853'
        # These links will take me to a particular article
        Rule(SgmlLinkExtractor(allow=('news/news/.*-\d{1,5}',)), callback='parse_article'),
    )

    def parse_article(self, response):
        selector = Selector(response)

        # Build the publication
        publication_item = PublicationItem()
        publication_item['name'] = self.publication_name
        publication_item['articles'] = list()

        # Build article
        article_item = ArticleItem()

        # Get the date
        date = extract(selector, '//div[@class="authors_info"]/span[@class="date"]/text()')
        article_item['date'] = '%s 00:00:00 UTC' % date

        # Get the title
        article_item['title'] = HTMLParser().unescape(extract(selector, '//div[@class="section_title"]/h1/text()'))

        # Get the content
        self.parse_content(selector, article_item)

        # Get the url
        article_item['source_url'] = response.url

        # Build the author
        name = extract(selector, '//div[@class="authors_info"]/span[@class="author"]/a/text()')
        if name:
            author_item = AuthorItem()
            author_item['name'] = name
            article_item['author'] = author_item

        publication_item['articles'].append(article_item)
        return publication_item


class FiveOuncesOfPainSpiderMixin(object):
    publication_name = 'Five Ounces of Pain'

    @staticmethod
    def clean_content(content):
        soup = BeautifulSoup(content)
        for script in soup.find_all('script'):
            script.extract()
        for div in soup.find_all('div'):
            if div.attrs:
                if 'id' in div.attrs:
                    if 'comment' in div['id'] or 'rm_theme_ad' in div['id'] or 'recommended' in div['id']:
                        div.extract()
                        # if 'class' in div.attrs:
                        #     if not ('block-interior' in div['class'] or 'block-image' in div['class']):
                        #         div.extract()
        soup = clean_iframes(soup)
        soup = clean_scripts(soup)
        soup = clean_imgs(soup)
        content = soup.body.contents[0].prettify()
        return clean_src(content)

    def parse_content(self, selector, article_item):
        article_item['video_url'] = None
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        article_item['content'] = extract(selector, '//div[@class="block-interior"]')
        # if article_item['image_url']:
        #     img_div = '<div>' + '<img src=\"' + article_item['image_url'] + '\" />' + '</div>'
        #     article_item['content'] = '<div>' + img_div + article_item['content'] + '</div>'
        article_item['content'] = self.clean_content(article_item['content'])


class FiveOuncesOfPainXMLNewsSpider(FiveOuncesOfPainSpiderMixin, XMLNewsSpider):
    name = 'five_ounces_of_pain_xml_news'
    start_urls = ['http://fiveouncesofpain.com/feed/']

    def parse_article(self, response):
        selector = Selector(response)
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]
        self.parse_content(selector, article_item)
        return publication_item


class FiveOuncesOfPainNewsSpider(FiveOuncesOfPainSpiderMixin, CrawlSpider):
    name = 'five_ounces_of_pain_news'
    allowed_domains = ['fiveouncesofpain.com']
    rules = (
        # Extract links matching 'http://www.sherdog.com/news/news/UFC-183-Results-Silva-vs-Diaz-PlaybyPlay-Updates-80853'
        # These links will take me to a particular article
        Rule(SgmlLinkExtractor(allow=('\d{4}/\d{2}/\d{2}/.*',), restrict_xpaths='//div[@class="block-latest"]'),
             callback='parse_article'),
    )

    def start_requests(self):
        requests = list()
        for i in range(0, 285):
            requests.append(Request('http://fiveouncesofpain.com/archives/page/%d/' % i))
        return requests

    def parse_article(self, response):
        selector = Selector(response)

        # Build the publication
        publication_item = PublicationItem()
        publication_item['name'] = self.publication_name
        publication_item['articles'] = list()

        # Build article
        article_item = ArticleItem()

        # Get the date
        date = extract(selector, '//span[@class="post-date"]/text()')
        article_item['date'] = '%s 00:00:00 UTC' % date

        # Get the title
        article_item['title'] = HTMLParser().unescape(extract(selector, '//div[@class="title-meta"]/h1/text()'))

        # Get the content
        self.parse_content(selector, article_item)

        # Get the url
        article_item['source_url'] = response.url

        # Build the author
        name = extract(selector, '//a[@rel="author"]/text()')
        if name:
            author_item = AuthorItem()
            author_item['name'] = name
            article_item['author'] = author_item

        publication_item['articles'].append(article_item)
        return publication_item


class BleacherReportSpiderMixin(object):
    publication_name = 'Bleacher Report'

    @staticmethod
    def clean_content(content):
        soup = BeautifulSoup(content)
        # Remove poll modules
        for div in soup.find_all('div'):
            if 'id' in div.attrs and 'class' in div.attrs:
                if re.search('poll', div['id']):
                    div.extract()

        # Some cleanin...
        soup = clean_iframes(soup)
        soup = clean_scripts(soup)
        # soup = clean_imgs(soup)

        # Replace span
        # for span in soup.find_all('span'):
        #     if not span.attrs:
        #         div = soup.new_tag('div')
        #         div.attrs = {'id': 'byline'}
        #         span.replaceWith(div)

        # Make images responsive
        for img in soup.find_all('img'):
            img['style'] = None
            img['class'] = 'img-responsive'
            img['width'] = None
            img['height'] = None
            img.parent['style'] = None
            # toks = img['src'].split('?')
            # img['src'] = toks[0]


        # Remove accrediation and captions
        elements = soup.find_all('div', class_='article_image-caption')
        elements += soup.find_all('span', class_='article_image-accreditation')
        elements += soup.find_all('a', class_='js-load-more-entries')
        for e in elements:
            e.extract()

        # Remove shares
        for d in soup.find_all('ul', class_='share-buttons'):
            d.extract()

        # Clean divs
        for div in soup.find_all('div'):
            div['class'] = None
            div['style'] = None

        # Clean imgs
        for img in soup.find_all('img'):
            img['width'] = '100%'
            img['height'] = 'auto'

        return clean_src(soup.body.contents[0].prettify())

    def parse_content(self, selector, article_item):
        article_item['video_url'] = None
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        article_item['content'] = extract(selector, '//div[@class="article_body cf"]')
        if not article_item['content']:
            article_item['content'] = ''
        article_item['content'] = self.clean_content(article_item['content'])
        return article_item

    def parse_iframe(self, response):
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]
        selector = Selector(response)

        # Iframe is video
        if 'streamable' in response.url:
            # Scrape poster
            url = extract(selector, '//*[@id="media-container"]/div[1]/video/@poster')
            if url:
                url = 'http:' + url
            article_item['image_url'] = url

            # Scrape video
            url = extract(selector, '//*[@id="media-container"]/div[1]/video/source/@src')
            if url:
                url = 'http:' + url
            article_item['video_url'] = url
        elif 'instagram' in response.url:
            style = extract(selector, '/html/body/div/div[1]/div/@style')
            toks = style.split('\'')
            article_item['image_url'] = toks[1] if toks and len(toks) > 1 else None

        return publication_item


class BleacherReportXMLNewsSpider(BleacherReportSpiderMixin, XMLNewsSpider):
    name = 'bleacher_report_xml_news'
    start_urls = ['http://bleacherreport.com/articles;feed?tag_id=24']

    def parse_article(self, response):
        selector = Selector(response)
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]
        article_item = self.parse_content(selector, article_item)
        ret = list()

        # Fuck slideshows
        if 'Begin Slideshow' in response.body:
            return None

        iframe_url = extract(selector, '//*[@id="article-slider"]/article/div/section/div[1]/div/iframe/@src')
        if not iframe_url:
            iframe_url = extract(selector, '//*[@id="article-slider"]/article/div/section/div[1]/div/iframe/@src')
        if iframe_url and not article_item['image_url']:
            if iframe_url.startswith('//'):
                iframe_url = 'http:' + iframe_url
            request = Request(iframe_url, callback=self.parse_iframe)
            request.meta['publication'] = publication_item
            ret.append(request)
        else:
            ret.append(publication_item)

        return ret


class BleacherReportNewsSpider(BleacherReportSpiderMixin, CrawlSpider):
    name = 'bleacher_report_news'
    allowed_domains = ['bleacherreport.com']
    rules = (
        Rule(LinkExtractor(allow='articles/\d{1,20}-.*',
                           restrict_xpaths='//div[@id="archive-results"]/ol[@class="archive-list"]'),
             callback='parse_article'),
    )

    def start_requests(self):
        requests = list()
        for i in range(1, 285):
            requests.append(Request('http://bleacherreport.com/mma/archives/newest/%d?filter=all' % i))
        return requests

    def parse_article(self, response):
        ret = list()
        selector = Selector(response)

        # Fuck slideshows
        if 'Begin Slideshow' in response.body:
            return None

        # Build the publication
        publication_item = PublicationItem()
        publication_item['name'] = self.publication_name
        publication_item['articles'] = list()

        # Build article
        article_item = ArticleItem()

        # Get the date
        date = extract(selector, '//time[@class="article_timestamp"]/@data-updated_at')
        article_item['date'] = datetime.datetime.fromtimestamp(int(date)).strftime('%Y-%m-%d %H:%M:%S UTC')

        # Get the title
        article_item['title'] = HTMLParser().unescape(extract(selector, '//h1[@class="article_title"]/text()'))

        # Get the content
        self.parse_content(selector, article_item)

        # Get the url
        article_item['source_url'] = response.url

        # Build the author
        name = extract(selector, '//a[@class="article_author-link"]/span/text()')
        if name:
            name = ' '.join(name.split())
            author_item = AuthorItem()
            author_item['name'] = name
            article_item['author'] = author_item

        publication_item['articles'].append(article_item)

        # Check for video
        iframe_url = extract(selector, '//*[@id="article-slider"]/article/div/section/div[1]/div/iframe/@src')
        if not iframe_url:
            iframe_url = extract(selector, '//*[@id="article-slider"]/article/div/section/div[1]/div/iframe/@src')
        if iframe_url and not article_item['image_url']:
            if iframe_url.startswith('//'):
                iframe_url = 'http:' + iframe_url
            request = Request(iframe_url, callback=self.parse_iframe)
            request.meta['publication'] = publication_item
            ret.append(request)
        else:
            ret.append(publication_item)

        return ret


class SBNationSpiderMixin(object):
    @staticmethod
    def clean_content(content):
        bad_classes = ['next-clicks-main-container']
        content = clean_img(content)
        soup = BeautifulSoup(content)
        for div in soup.find_all('div'):
            if div.attrs:
                if 'class' in div.attrs:
                    if div['class'][0] in bad_classes:
                        div.extract()

        soup = clean_iframes(soup)
        soup = clean_scripts(soup)
        soup = clean_imgs(soup)

        # Remove must read crap
        for img in soup.find_all('img'):
            if 'alt' in img.attrs and img['alt'] == 'Star-divide':
                for p in img.parent.parent.find_next_siblings(['p', 'div', 'img', 'b', 'script', 'blockquote']):
                    p.extract()
                img.extract()
                break

        return clean_src(soup.body.contents[0].prettify())

    def parse_content(self, selector, article_item):
        article_item['video_url'] = None

        # Grab the content
        article_item['content'] = extract(selector, '//div[contains(@class,"m-entry__body")]')

        # Grab the image url
        article_item['image_url'] = extract(selector, '//html/head/meta[@property="og:image"]/@content')
        article_item['video_url'] = extract(selector, '//div[contains(@class, "m-video__pane")]/iframe/@src')
        if article_item['video_url'] and 'youtube' in article_item['video_url']:
            toks = re.split('embed/|\?', article_item['video_url'])
            if len(toks) > 1:
                article_item['image_url'] = 'http://img.youtube.com/vi/%s/0.jpg' % toks[1]
        if article_item['image_url']:
            article_item['image_url'] = article_item['image_url'].replace('https', 'http')

        # Clean the content
        article_item['content'] = self.clean_content(article_item['content'])


class SBNationXMLNewsSpider(SBNationSpiderMixin, XMLFeedSpider):
    name = 'sbnation_xml_news'
    start_urls = ['http://feeds.feedburner.com/sportsblogs/bloodyelbow?format=xml',
                  'http://feeds.feedburner.com/sportsblogs/mmafighting.xml',
                  'http://feeds.feedburner.com/sportsblogs/mmamania.xml']

    def parse_article(self, response):
        selector = Selector(response)
        publication_item = response.meta['publication']
        article_item = publication_item['articles'][0]

        self.parse_content(selector, article_item)

        return publication_item

    def parse(self, response):
        selector = Selector(response, type='html')

        # Get publication name
        publication_name = extract(selector, '//title/text()')
        publication_name = PUBLICATION_NAMES[publication_name]

        # Get articles
        for entry in selector.xpath('//entry'):
            publication = PublicationItem()
            publication['articles'] = list()
            publication['name'] = publication_name
            article = ArticleItem()

            # Get title
            title = extract(entry, './title/text()')
            title = HTMLParser().unescape(title)
            article['title'] = title

            # Get author
            name = extract(entry, './author/name/text()')
            if name and name.find('(') >= 0:
                start_index = name.find('(')
                end_index = name.find(')')
                name = name[start_index + 1:end_index]
            if name:
                author = AuthorItem()
                author['name'] = name
                article['author'] = author

            # Get date
            date = dateutil.parser.parse(extract(entry, './published/text()'))
            date = date.astimezone(dateutil.tz.tzutc())
            date_format = '%Y-%m-%dT%H:%M:%S UTC'
            date = date.strftime(date_format)
            article['date'] = date

            # Get url
            url = extract(entry, './link/@href')
            article['source_url'] = url

            # Add article to publication
            publication['articles'].append(article)

            # Let's build a request that will eventually fetch the most awesomest imagery!!!
            request = Request(article['source_url'], callback=self.parse_article)
            request.meta['publication'] = publication
            yield request


class SBNationNewsSpider(SBNationSpiderMixin, CrawlSpider):
    name = 'sbnation_news'
    allowed_domains = ['bloodyelbow.com',
                       'mmamania.com',
                       'mmafighting.com']
    start_urls = ['http://www.bloodyelbow.com/archives/1',
                  'http://www.mmamania.com/archives/1',
                  'http://www.mmafighting.com/archives/1']

    rules = (
        Rule(SgmlLinkExtractor(allow=('archives/\d{1,3}', ))),

        # Extract links matching 'http://www.bloodyelbow.com/2015/2/2/7960865/anderson-silva-bes-baddest-greatest-striker-tournament-lyoto-machida-ufc-183-mma-goat'
        # These links will take me to a particular article
        Rule(SgmlLinkExtractor(allow=('\d{4}/\d{1,2}/\d{1,2}/\d{1,7}',), restrict_xpaths='//div[@class="l-chunk"]'),
             callback='parse_article'),
    )

    def parse_article(self, response):
        selector = Selector(response)

        # Build the publication
        publication_item = PublicationItem()
        publication_item['articles'] = list()
        if 'bloodyelbow' in response.url:
            publication_item['name'] = 'Bloody Elbow'
        elif 'mmamania' in response.url:
            publication_item['name'] = 'MMA Mania'
        elif 'mmafighting' in response.url:
            publication_item['name'] = 'MMA Fighting'
        else:
            return None

        # Build article
        article_item = ArticleItem()

        # Get the date
        selectors = selector.xpath('//p[@class="m-entry__byline"]/text()')
        date_text = '\n'.join([s.extract() for s in selectors])
        match_object = re.search(r'\s*on\s*([A-Za-z]{3} \d{1,2}, \d{4}, \d{1,2}:\d{2}[a|p])', date_text)
        date = match_object.group(1)
        article_item['date'] = '%s EST' % date

        # Get the title
        article_item['title'] = HTMLParser().unescape(
            extract(selector, '//h2[@class="m-entry__title"]/text()').replace(u'\xa0', u' '))

        # Get the content
        self.parse_content(selector, article_item)

        # Get the url
        article_item['source_url'] = response.url

        # Build the author
        name = extract(selector, '//a[@rel="author"]/text()')
        if name:
            author_item = AuthorItem()
            author_item['name'] = name
            article_item['author'] = author_item

        publication_item['articles'].append(article_item)
        return publication_item

from HTMLParser import HTMLParser
import json

from scrapy.spider import Spider
from scrapy.spider import Request
from fightnerd.anansi.items import VideoItem
from fightnerd.anansi.items import AuthorItem
import gdata.youtube
import gdata.youtube.service


class YoutubeVideoSpider(Spider):
    name = 'youtube_videos'
    download_delay = 0.5

    def start_requests(self):
        start_urls = ['http://gdata.youtube.com/feeds/api/users/MMAFightingOnSBN/uploads?alt=json',
                      'http://gdata.youtube.com/feeds/api/users/UFC/uploads?alt=json',
                      'http://gdata.youtube.com/feeds/api/users/BellatorMMA/uploads?alt=json']
        max_results = 50
        total_results = 9000
        requests = list()
        for url in start_urls:
            for i in range(1, total_results, max_results):
                requests.append(Request('%s&max-results=%d&start-index=%d' % (url, max_results, i)))
        return requests

    def parse_video(self, video_json):
        video_item = VideoItem()

        # Get title
        video_item['title'] = HTMLParser().unescape(video_json['title']['$t'])

        # Get url
        video_item['url'] = video_json['id']['$t']

        # Get date
        video_item['date'] = video_json['published']['$t']

        # Get thumbnail
        video_item['image_url'] = video_json['media$group']['media$thumbnail'][0]['url']

        # Get description
        video_item['description'] = HTMLParser().unescape(video_json['media$group']['media$description']['$t'])

        return video_item

    def parse(self, response):
        youtube_json = json.loads(response.body)

        # Get author name
        try:
            author_item = AuthorItem()
            author_item['name'] = youtube_json['feed']['author'][0]['name']['$t']
        except:
            author_item = None

        # Get videos
        if 'entry' in youtube_json['feed']:
            for video_json in youtube_json['feed']['entry']:
                video_item = self.parse_video(video_json)
                if author_item:
                    video_item['author'] = author_item
                yield video_item

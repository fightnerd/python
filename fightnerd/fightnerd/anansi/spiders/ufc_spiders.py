from datetime import datetime

from scrapy.spider import Spider
from scrapy.selector import Selector
from fightnerd.anansi.items import RankingListItem
from fightnerd.anansi.items import FighterItem
from fightnerd.anansi.items import RankingItem
from fightnerd.anansi.items import UpcomingEventItem
from fightnerd.anansi.items import UpcomingFightItem
# from fightnerd.anansi.items import BarItem
import re
from dateutil import parser as date_parser
from dateutil.relativedelta import relativedelta
import pytz
from scrapy.contrib.spiders import CrawlSpider
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule
from fightnerd.anansi.misc import extract
from django.conf import settings


TZD = getattr(settings, 'TZD')


class UFCRankingSpider(Spider):
    name = 'ufc_rankings'
    start_urls = ['http://www.ufc.com/rankings']

    @staticmethod
    def get_rank_delta(ranking_movement_path):
        ret = 0
        try:
            ret = int(ranking_movement_path.xpath('./text()').extract()[1].strip())
            if 'down' in ranking_movement_path.extract()[0]:
                ret = -ret
        except:
            pass
        return ret

    @staticmethod
    def get_ranking_item(full_name, weight_class, rank, rank_delta, not_previously_ranked=False, is_champion=False, is_interim_champion=False):
        fighter_item = FighterItem()
        fighter_item['full_name'] = full_name
        ranking_item = RankingItem()
        ranking_item['fighter'] = fighter_item
        ranking_item['rank'] = rank
        ranking_item['rank_delta'] = rank_delta
        ranking_item['weight_class'] = weight_class
        ranking_item['not_previously_ranked'] = not_previously_ranked
        ranking_item['is_champion'] = is_champion
        ranking_item['is_interim_champion'] = is_champion
        return ranking_item

    def parse(self, response):
        selector = Selector(response)
        for div in selector.xpath('//div[contains(@class,"ranking-list tall")]'):
            ranking_list_item = RankingListItem()
            ranking_list_item['rankings'] = []

            # Fucking UFC...
            prefix_path = './div[@class="row"]'
            use_prefix_path = extract(div, prefix_path) is not None

            # Get weight class
            weight_class_path = './div[@class="weight-class-name"]/text()'
            if not use_prefix_path:
                weight_class = extract(div, weight_class_path)
            else:
                weight_class = extract(div, './div[@class="row"]/%s' % weight_class_path)
            weight_class = weight_class.strip().lower()

            # Get champion
            champion_path = './div[@class="rankings-champions"]/div[@id="champion-fighter"]/span[@id="champion-fighter-name"]/a/text()'
            if not use_prefix_path:
                champion_name = extract(div, champion_path)
            else:
                champion_name = extract(div, '%s/%s' % (prefix_path, champion_path))

            # Append ranking for champion
            rank = 0
            if champion_name:
                ranking_item = self.get_ranking_item(champion_name, weight_class, rank, 0, False, True)
                ranking_list_item['rankings'].append(ranking_item)
                rank += 1

            # Get fighters
            row_path = './div[@class="rankings-table"]/table/tr'
            if use_prefix_path:
                row_path = '%s/%s' % (prefix_path, row_path)
            for row in div.xpath(row_path):
                # Get fighter name
                full_name = extract(row, './td[@class="name-column"]/a/text()')
                is_interim_champion = False
                if 'Interim Champion' in full_name:
                    is_interim_champion = True
                full_name = full_name.replace('(Interim Champion)', '').strip()

                # Get ranking movement
                ranking_movement_path = row.xpath('./td[@class="name-column"]/div[@class="rankingMovement"]')

                # Get not previously ranked
                not_previously_ranked = '*NR' in ranking_movement_path.extract()[0].strip()

                # Get rank delta
                rank_delta = self.get_rank_delta(ranking_movement_path)

                ranking_item = self.get_ranking_item(full_name, weight_class, rank, rank_delta, not_previously_ranked,
                                                     False, is_interim_champion=is_interim_champion)
                ranking_list_item['rankings'].append(ranking_item)
                rank += 1

            # Yield the rankings
            yield ranking_list_item
            # print ranking_list_item


class UFCUpcomingEventSpider(Spider):
    name = 'ufc_upcoming_events'
    start_urls = ['http://www.ufc.com/schedule/event/US']

    def parse(self, response):
        selector = Selector(response)

        for li in selector.xpath('//div[@id="schedule-features"]/ul/li'):
            # Get title of event
            event_name = extract(li,
                                 './div[@class="feature-header"]/div[@class="header-text"]/div[@class="title"]/a/text()')
            event_name = event_name.strip()

            # Get the last names of fighters on the main card
            toks = re.sub('UFC Fight Night', '', event_name)
            toks = re.sub('UFC \d+', '', toks)
            toks = re.sub('I{2,}', '', toks)
            toks = re.sub('\d', '', toks)
            toks = re.sub(r' vs\.* ', ' ', toks)
            toks = toks.strip().split(' ')
            first_fighter_last_name = toks[0]
            second_fighter_last_name = toks[1]

            # Scrape date/time
            event_date = extract(li,
                                 './div[@class="feature-footer"]/div[contains(@class,"feature-details")]/div[@class="date"]/text()').strip()
            try:
                event_time = extract(li,
                                     './div[@class="feature-footer"]/div[contains(@class,"feature-details")]/div[@class="time"]/text()').strip().split(
                    '/')[0]
            except:
                continue
            event_time_zone = 'EDT'
            if 'tbd' == event_time.lower() or 'tbd' == first_fighter_last_name.lower() or 'tbd' == second_fighter_last_name.lower():
                continue

            # Get the actual time
            try:
                event_date = date_parser.parse(' '.join([event_date, event_time, event_time_zone]), tzinfos=TZD)
            except:
                continue

            if datetime.utcnow().replace(tzinfo=pytz.utc) > event_date + relativedelta(months=10):
                event_date = event_date + relativedelta(years=1)

            upcoming_fight_item = UpcomingFightItem()
            upcoming_fight_item['first_fighter'] = first_fighter_last_name.strip()
            upcoming_fight_item['second_fighter'] = second_fighter_last_name.strip()
            upcoming_fight_item['order'] = 0

            upcoming_event_item = UpcomingEventItem()
            upcoming_event_item['upcoming_fights'] = []
            upcoming_event_item['name'] = event_name
            upcoming_event_item['date'] = event_date
            upcoming_event_item['is_time_precise'] = True
            upcoming_event_item['upcoming_fights'].append(upcoming_fight_item)

            yield upcoming_event_item


class UFCBarSpider(CrawlSpider):
    name = 'ufc_bars'
    start_urls = ['http://bars.ufc.com/find-bar-by-state']
    allowed_domains = ['bars.ufc.com']

    rules = (
        Rule(SgmlLinkExtractor(allow=('bars-in/[A-Z]{2}', ))),
        Rule(SgmlLinkExtractor(allow=('bars/\d{1,10}/[a-zA-Z]{2}/[a-zA-Z]{2}/',)), callback='parse_bar'),
    )

    def parse_bar(self, response):
        selector = Selector(response)

        # Grab the name of the bar
        bar_item = BarItem()
        bar_item['name'] = extract(selector, '//section[@id="detail-left-column"]/header/h1/text()')

        # Grab the street address
        bar_item['street_address'] = extract(selector, '//section[@id="detail-left-column"]/header/address/text()')

        # Grab the phone number
        phone_number = extract(selector, '//section[@id="detail-left-column"]/header/h3/text()')
        phone_number = phone_number.split('Phone: ')[1]
        bar_item['phone_number'] = phone_number

        # Grab the lat/lon
        bar_item['latitude'] = extract(selector, '//meta[@property="og:latitude"]/@content')
        bar_item['longitude'] = extract(selector, '//meta[@property="og:logitude"]/@content')

        return bar_item

# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from __future__ import absolute_import

import base64

import os
from fightnerd.anansi.items import FighterItem
from fightnerd.anansi.items import UpcomingEventItem
from fightnerd.anansi.items import RankingListItem
from fightnerd.anansi.items import PublicationItem
from fightnerd.anansi.items import VideoItem
from fightnerd.fighters.tasks import process_fighter
from fightnerd.fighters.tasks import process_upcoming_event
from fightnerd.fighters.tasks import process_upcoming_ufc_event_time
from fightnerd.fighters.tasks import process_odds
from fightnerd.fighters.tasks import process_rankings
from fightnerd.publications.tasks import process_publication
from fightnerd.anansi.settings import IMAGES_STORE


class Base64EncodeImagesPipeline(object):
    def process_item(self, item, spider):
        if 'images' not in item:
            return item

        encoded_images = []
        for image in item['images']:
            with open(os.path.join(IMAGES_STORE, image['path'])) as image_file:
                encoded_image = dict()
                encoded_image['name'] = os.path.basename(image['path'])
                encoded_image['checksum'] = image['checksum']
                encoded_image['url'] = image['url']
                encoded_image['data'] = base64.b64encode(image_file.read())
                encoded_images.append(encoded_image)
            os.remove(os.path.join(IMAGES_STORE, image['path']))
        item['images'] = encoded_images
        return item


class AMQPPipeline(object):
    def process_item(self, item, spider):
        if type(item) is FighterItem:
            process_fighter.delay(item)
        elif type(item) is UpcomingEventItem:
            upcoming_fight_item = item['upcoming_fights'][0] if 'upcoming_fights' in item and item['upcoming_fights'] and len(item['upcoming_fights']) > 0 else None
            if 'is_time_precise' in item and item['is_time_precise']:
                process_upcoming_ufc_event_time.delay(item)
            elif upcoming_fight_item and 'first_fighter_odds' in upcoming_fight_item and upcoming_fight_item['first_fighter_odds']:
                process_odds.delay(item)
            else:
                process_upcoming_event.delay(item)
        # if 'full_name' in item:
        # elif 'upcoming_fights' in item:
        #     upcoming_fight_item = item['upcoming_fights'][0]
        #     if 'first_fighter_odds' in upcoming_fight_item:
        #         process_odds.delay(item)
        #     elif 'is_time_precise' in item and item['is_time_precise']:
        #         process_upcoming_event_time.delay(item)
        #     else:
        #         process_upcoming_event.delay(item)
        elif type(item) is PublicationItem:
            process_publication.delay(item)
        elif type(item) is RankingListItem:
            process_rankings.delay(item)
        # elif 'street_address' in item:
        #     process_bar.delay(item)
        return item

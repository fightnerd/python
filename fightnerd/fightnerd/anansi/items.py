# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy_djangoitem import DjangoItem
from fightnerd.fighters.models import Fighter
from fightnerd.fighters.models import Association
from fightnerd.fighters.models import Referee
from fightnerd.fighters.models import Event
from fightnerd.fighters.models import Fight
# from fightnerd.fighters.models import UpcomingEvent
# from fightnerd.fighters.models import UpcomingFight
from fightnerd.fighters.models import Ranking
from fightnerd.publications.models import Publication
from fightnerd.publications.models import Article
from fightnerd.publications.models import Author
from fightnerd.publications.models import Video
# from fightnerd.bars.models import Bar
from scrapy.item import Field


class FighterItem(DjangoItem):
    django_model = Fighter
    associations = Field()
    fights = Field()
    is_partial = Field()
    image_urls = Field()
    images = Field()


class AssociationItem(DjangoItem):
    django_model = Association


class RefereeItem(DjangoItem):
    django_model = Referee


class EventItem(DjangoItem):
    django_model = Event


class FightItem(DjangoItem):
    django_model = Fight
    opponent = Field()
    event = Field()


class UpcomingEventItem(DjangoItem):
    django_model = Event
    is_time_precise = Field()
    upcoming_fights = Field()
    organization = Field()


class RankingItem(DjangoItem):
    django_model = Ranking
    fighter = Field()


class RankingListItem(DjangoItem):
    rankings = Field()


class UpcomingFightItem(DjangoItem):
    django_model = Fight
    first_fighter = Field()
    second_fighter = Field()
    first_fighter_odds = Field()
    second_fighter_odds = Field()


class PublicationItem(DjangoItem):
    django_model = Publication
    articles = Field()


class ArticleItem(DjangoItem):
    django_model = Article
    author = Field()


class AuthorItem(DjangoItem):
    django_model = Author


class VideoItem(DjangoItem):
    django_model = Video
    author = Field()


# class BarItem(DjangoItem):
#     django_model = Bar
#     latitude = Field()
#     longitude = Field()

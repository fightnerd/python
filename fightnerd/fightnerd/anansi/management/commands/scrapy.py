from __future__ import absolute_import
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('cmd')

    def handle(self, *args, **options):
        from scrapy.cmdline import execute
        from fightnerd.anansi.settings import LOG_FILE
        import os
        if os.path.exists(LOG_FILE):
            os.remove(LOG_FILE)
        execute(['scrapy'] + options.get('cmd').split())

# Scrapy settings for anansi project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
from django.conf import settings
import os

LOGS_ROOT = getattr(settings, 'LOGS_ROOT')
MEDIA_ROOT = getattr(settings, 'MEDIA_ROOT')


BOT_NAME = 'anansi'
BOT_VERSION = '1.0'

ITEM_PIPELINES = {'scrapy.contrib.pipeline.images.ImagesPipeline': 0,
                  'fightnerd.anansi.pipelines.Base64EncodeImagesPipeline': 1,
                  'fightnerd.anansi.pipelines.AMQPPipeline': 2}

CLOSESPIDER_ITEMCOUNT = 0
SPIDER_MODULES = ['fightnerd.anansi.spiders']
NEWSPIDER_MODULE = 'fightnerd.anansi.spiders'
LOG_LEVEL = 'DEBUG'
LOG_FILE = os.path.join(LOGS_ROOT, 'scrapy.log')
IMAGES_STORE = os.path.join(MEDIA_ROOT, 'scrapy/')
if not os.path.exists(IMAGES_STORE):
    os.makedirs(IMAGES_STORE)
IMAGES_EXPIRES = 90

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17'

DOWNLOAD_DELAY = 0

# EXTENSIONS = {'scrapy.contrib.statsmailer.StatsMailer': 500,
#               'scrapy.contrib.closespider.CloseSpider': 500,
#               }
# STATSMAILER_RCPTS = ['cezekwe@thecageapp.com']
#CLOSESPIDER_ITEMCOUNT = 5000

DOWNLOADER_MIDDLEWARES = {
    'fightnerd.anansi.middlewares.tor.TorMiddleware': 543,
}

DOWNLOAD_HANDLERS = {
    's3': None
}
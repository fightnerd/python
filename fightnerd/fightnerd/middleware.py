from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings

FORWARDED_HOST = getattr(settings, 'FORWARDED_HOST') if hasattr(settings, 'FORWARDED_HOST') else None


class ForwardedHostMiddleware(object):
    def __init__(self):
        self.forwarded_host = FORWARDED_HOST

    def process_request(self, request):
        if self.forwarded_host:
            request.META['HTTP_X_FORWARDED_HOST'] = self.forwarded_host
        return None
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import sys
import os
import braintree
from django.core.urlresolvers import reverse_lazy
from psycopg2.extras import NumericRange
from datetime import timedelta
import ast
import imp
from celery.schedules import crontab

APP_NAME = 'fightnerd'
PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
BASE_DIR = os.path.join(PROJECT_DIR, APP_NAME)
APP_DIR = os.path.join(BASE_DIR, APP_NAME)
RUN_DIR = '/var/run/%s/' % APP_NAME
if not os.path.exists(RUN_DIR):
    os.makedirs(RUN_DIR)


IS_TEST_MODE = len(sys.argv) > 1 and sys.argv[1] == 'test'
IS_PRODUCTION = ast.literal_eval(os.environ['IS_PRODUCTION_ENVIRONMENT'])
IS_COLLECT_STATICS_ENVIRONMENT = len(sys.argv) > 1 and sys.argv[1] == 'collectstatic'  # ast.literal_eval(os.environ.get('IS_COLLECT_STATICS_ENVIRONMENT', 'False'))

# IS_LOCAL = 'chukwuemeka' in socket.gethostname().lower() or 'new-host' in socket.gethostname()
ADMINS = (
    ('Chukwuemeka Ezekwe', 'cue0083@gmail.com'),
    ('Chukwuemeka Ezekwe', 'emeka@thecageapp.com'),
)
STAFF = ADMINS

# Which site...?
SITE_ID = 1
SITE_DOMAIN = os.environ['SITE_DOMAIN']  # 'localhost:5031'
SITE_NAME = os.environ['SITE_NAME']  # 'SHOTZU'
SITE_DISPLAY_NAME = os.environ['SITE_DISPLAY_NAME']  # 'SHOTZU'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%yb^e0oqkl#176&70x%%i#n)(0m$d$g-gr2yx(dvr0+2i_uw&e'

# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = IS_LOCAL or os.environ['DEBUG'] == 'True'
DEBUG = ast.literal_eval(os.environ.get('DEBUG', 'False').capitalize())
# IS_PRODUCTION = not DEBUG and not IS_LOCAL

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'fightnerd.context_processors.api_keys',
                'fightnerd.context_processors.constants',
            ],
            'debug': DEBUG
        },
        'DIRS': [
            os.path.join(APP_DIR, 'templates/'),
        ]
    },
]

INTERNAL_IPS = (
    'localhost',
    'localhost:4597',
    'localhost:4577',
    '127.0.0.1'
)

ALLOWED_HOSTS = [
    'test.thecageapp.com',
    'thecageapp.com',
]

AUTH_USER_MODEL = 'users.User'

# Application definition

INSTALLED_APPS = [
    # 'grappelli',
    'djcelery',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.gis',
    'django.contrib.sites',
    'social.apps.django_app.default',
    'storages',
    'seacucumber',
    'easy_thumbnails',
    'django_extensions',
    'django_countries',
    'compressor',
    'admin_honeypot',
    'haystack',
    'backups',
    'mathfilters',
    'fightnerd.utils',
    'fightnerd.users',
    'fightnerd.anansi',
    'fightnerd.instagram_bots',
    'fightnerd.twitter_bots',
    'fightnerd.fighters',
    'fightnerd.notifications',
    'fightnerd.publications',
]
# INSTALLED_APPS.append('django.contrib.sites')
#
# if not IS_TEST_MODE:
#     INSTALLED_APPS.append('django.contrib.sites')

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'fightnerd.middleware.ForwardedHostMiddleware'
    # 'django.middleware.cache.FetchFromCacheMiddleware',
)

ROOT_URLCONF = 'fightnerd.urls'

WSGI_APPLICATION = 'fightnerd.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

USE_I18N = True

USE_L10N = True

# Time
USE_TZ = True
TIME_INPUT_FORMATS = (
    '%H:%M:%S',     # '14:30:59'
    '%H:%M:%S.%f',  # '14:30:59.000200'
    '%H:%M',        # '14:30'
    '%I:%M %p',
    '%I:%M%p'
)
TIME_ZONE = 'UTC'

# GeoIP
GEOIP_PATH = '/usr/local/Cellar/geoip/1.6.4/data/'
GEOIP_LIBRARY_PATH = '/usr/local/Cellar/geoip/1.6.4/lib/libGeoIP.dylib'
GEOIP_DATABASE = os.path.join(GEOIP_PATH, 'GeoLiteCity.dat')
GEOIPV6_DATABASE = os.path.join(GEOIP_PATH, 'GeoIPv6.dat')

# Email
SERVER_EMAIL = 'Bot <bot@thecageapp.com>'
DEFAULT_FROM_EMAIL = SERVER_EMAIL
SUPPORT_EMAIL = 'support@thecageapp.com'
EMAIL_CLOSING = 'Peace,\nThe %s Crew' % SITE_DISPLAY_NAME
STAFF_EMAILS = [staff_email[1] for staff_email in STAFF]
EMAIL_BACKEND = os.environ['EMAIL_BACKEND']

# BACKUPS
BACKUPS_BACKUP_DIRECTORY = '/backups'
BACKUPS_BACKUP_CLEANUP_LIMIT = 9000

# Logging
def get_logging(logs_root):
    logging = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
            'standard': {
                'format': '%(levelname)s %(message)s'
            },
            'basic': {
                'format': '%(message)s'
            },
        },
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'INFO' if not IS_TEST_MODE else 'CRITICAL',
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': os.path.join(logs_root, 'django.log'),
            },
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler',
                'include_html': True
            },
            'rotating_file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': os.path.join(logs_root, 'django.log'),
                'maxBytes': 1024 * 1024 * 10,  # 10MB
                'backupCount': 10,
                'formatter': 'verbose',
            },
        },
        'loggers': {
            'stripe': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'WARNING',
                'propagate': False
            },
            'django.request': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'WARNING',
                'propagate': False
            },
            'django.security': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'WARNING',
                'propagate': False
            },
            'urllib3': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'ERROR',
                'propagate': False
            },
            'requests': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'ERROR',
                'propagate': False
            },
            'celery.task': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'fightnerd.fighters.tasks': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'fightnerd.publications.tasks': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            '': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
        },
    }

    return logging
LOGS_ROOT = '/var/log/django'
if not os.path.exists(LOGS_ROOT):
    os.makedirs(LOGS_ROOT)
LOGGING = get_logging(LOGS_ROOT)

# Django REST Framework
REST_FRAMEWORK = {
    # 'DEFAULT_AUTHENTICATION_CLASSES': (
    #     'rest_framework.authentication.SessionAuthentication',
    # ),
    #
    # 'DEFAULT_PERMISSION_CLASSES': (
    #     'rest_framework.permissions.IsAuthenticated',
    # ),
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),

    'DEFAULT_PAGINATION_CLASS': 'fightnerd.pagination.LimitOffsetPagination',

    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
    ),

    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
    ),
}
API_PROTOCOL = os.environ['API_PROTOCOL']
API_DOMAIN = os.environ['API_DOMAIN']

# AWS
AWS_S3_SECURE_URLS = True
AWS_HEADERS = {
    'Cache-Control': 'max-age=31536000',
}
AWS_CLOUDFRONT_DOMAIN = os.environ['AWS_CLOUDFRONT_DOMAIN']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_AUTO_CREATE_BUCKET = True
if ast.literal_eval(os.environ['IS_AWS_CLOUDFRONT_ENABLED']):
    AWS_S3_CUSTOM_DOMAIN = AWS_CLOUDFRONT_DOMAIN
    S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN
else:
    AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
    S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN

# Media
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = 5.0
DO_USE_MEDIA_S3_STORAGE = ast.literal_eval(os.environ['IS_AWS_S3_MEDIA_STORAGE_ENABLED'])
DO_USE_MEDIA_S3_STORAGE = not IS_TEST_MODE and DO_USE_MEDIA_S3_STORAGE
if DO_USE_MEDIA_S3_STORAGE:
    DEFAULT_FILE_STORAGE = 'fightnerd.storages.S3MediaStorage'
    MEDIAFILES_LOCATION = 'media'
    MEDIA_URL = '%s/%s/' % (S3_URL, MEDIAFILES_LOCATION)
    THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE
else:
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(RUN_DIR, 'media')
    DEFAULT_FILE_STORAGE = 'fightnerd.storages.FileSystemStorage'
    THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE

# Static
STATICFILES_VERSION = os.environ['STATICFILES_VERSION']
STATICFILES_LOCATION = 'static/%s' % STATICFILES_VERSION
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)
STATICFILES_DIRS = (
    os.path.join(APP_DIR, 'static/dist/'),
)
do_use_static_s3_storage = ast.literal_eval(os.environ['IS_AWS_S3_STATIC_STORAGE_ENABLED'])
if do_use_static_s3_storage:
    STATIC_ROOT = os.path.join(BASE_DIR, 'static')
    if not os.path.exists(STATIC_ROOT):
        os.makedirs(STATIC_ROOT)
    STATICFILES_STORAGE = 'fightnerd.storages.S3StaticStorage'
    STATIC_URL = '%s/%s/' % (S3_URL, STATICFILES_LOCATION)
else:
    STATIC_URL = '/static/'
    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
    STATIC_ROOT = os.path.join(RUN_DIR, 'static')
    if not os.path.exists(STATIC_ROOT):
        os.makedirs(STATIC_ROOT)

# Grappelli
GRAPPELLI_ADMIN_TITLE = 'fightnerd-web'

# Sessions
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_SAVE_EVERY_REQUEST = True
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

# SCRAPY
os.environ['SCRAPY_SETTINGS_MODULE'] = 'fightnerd.anansi.settings'

# GOOGLE
GOOGLE_API_KEY = 'AIzaSyD9AtMYSEudmhLWhPtlipgATmxNw9n4lD0'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.environ['POSTGRES_DB'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ['POSTGRES_HOST'],
        'PORT': os.environ['POSTGRES_PORT'],
    }
}

# Cache
CACHE_BACKEND = os.environ['CACHE_BACKEND'] if not IS_TEST_MODE else 'django.core.cache.backends.locmem.LocMemCache'
url = 'redis://:%s@%s:%s' % (
    os.environ['REDIS_PASSWORD'],
    os.environ['REDIS_HOST'],
    os.environ['REDIS_PORT']
)
CACHES = {
    'default': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/1' % (url, ),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'twitter_bots': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/2' % (url, ),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'instagram_bots': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/2' % (url, ),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

# Celery
CELERY_ALWAYS_EAGER = IS_TEST_MODE
CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
BROKER_URL = 'redis://:%s@%s:%s/4' % (os.environ['REDIS_PASSWORD'], os.environ['REDIS_HOST'], os.environ['REDIS_PORT'])
if ast.literal_eval(os.environ.get('IS_CELERYBEAT_SCHEDULE_ENABLED', 'False')):
    CELERYBEAT_SCHEDULE = {
        'crawl_publications': {
            'task': 'fightnerd.publications.tasks.crawl_publications',
            'schedule': timedelta(minutes=30),
        },
        'crawl_upcoming_events': {
            'task': 'fightnerd.fighters.tasks.crawl_upcoming_events',
            'schedule': crontab(minute=0, hour=9),
        },
        # 'fire_notifications': {
        #     'task': 'fightnerd.notifications.tasks.fire_notifications',
        #     'schedule': timedelta(seconds=30),
        # },
        # 'backup_database': {
        #     'task': 'backups.tasks.BackupDatabaseTask',
        #     'schedule': crontab(minute=0, hour=10),
        # },
        'twitter_follow': {
            'task': 'fightnerd.twitter_bots.tasks.run_follow_bots',
            'schedule': crontab(minute=0)
        },
        'twitter_unfollow': {
            'task': 'fightnerd.twitter_bots.tasks.run_unfollow_bots',
            'schedule': crontab(minute=20)
        },
        # 'instagram_follow': {
        #     'task': 'fightnerd.instagram_bots.tasks.run_follow_bots',
        #     'schedule': crontab(minute=10)
        # },
        # 'instagram_unfollow': {
        #     'task': 'fightnerd.instagram_bots.tasks.run_unfollow_bots',
        #     'schedule': crontab(minute=30)
        # },
    }

# Haystack
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
    # 'default': {
    #     'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
    #     'URL': 'http://%s:%d/' % (os.environ['ELASTICSEARCH_PORT_9200_TCP_ADDR'], int(os.environ['ELASTICSEARCH_PORT_9200_TCP_PORT'])),
    #     'INDEX_NAME': 'test' if IS_TEST_MODE else 'haystack',
    # },
}

# SSL
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# APNS
APNS_CERTIFICATE_FILE_PATH = os.path.join(PROJECT_DIR, 'deploy/docker/python/apns/sandbox.certificate.pem')
APNS_CERTIFICATE_AUTHORITY_FILE_PATH = os.path.join(PROJECT_DIR, 'deploy/docker/python/apns/entrust_2048_ca.cer')
APNS_PRIVATE_KEY_FILE_PATH = os.path.join(PROJECT_DIR, 'deploy/docker/python/apns/sandbox.private.pem')
APNS_USE_SANDBOX = True

# Timezones...?
tz_str = '''-12 Y
-11 X NUT SST
-10 W CKT HAST HST TAHT TKT
-9 V AKST GAMT GIT HADT HNY
-8 U AKDT CIST HAY HNP PST PT
-7 T HAP HNR MST PDT
-6 S CST EAST GALT HAR HNC MDT
-5 R CDT COT EASST ECT EST ET HAC HNE PET
-4 Q AST BOT CLT COST EDT FKT GYT HAE HNA PYT
-3 P ADT ART BRT CLST FKST GFT HAA PMST PYST SRT UYT WGT
-2 O BRST FNT PMDT UYST WGST
-1 N AZOT CVT EGT
0 Z EGST GMT UTC WET WT
1 A CET DFT WAT WEDT WEST
2 B CAT CEDT CEST EET SAST WAST
3 C EAT EEDT EEST IDT MSK
4 D AMT AZT GET GST KUYT MSD MUT RET SAMT SCT
5 E AMST AQTT AZST HMT MAWT MVT PKT TFT TJT TMT UZT YEKT
6 F ALMT BIOT BTT IOT KGT NOVT OMST YEKST
7 G CXT DAVT HOVT ICT KRAT NOVST OMSST THA WIB
8 H ACT AWST BDT BNT CAST HKT IRKT KRAST MYT PHT SGT ULAT WITA WST
9 I AWDT IRKST JST KST PWT TLT WDT WIT YAKT
10 K AEST ChST PGT VLAT YAKST YAPT
11 L AEDT LHDT MAGT NCT PONT SBT VLAST VUT
12 M ANAST ANAT FJT GILT MAGST MHT NZST PETST PETT TVT WFT
13 FJST NZDT
11.5 NFT
10.5 ACDT LHST
9.5 ACST
6.5 CCT MMT
5.75 NPT
5.5 SLT
4.5 AFT IRDT
3.5 IRST
-2.5 HAT NDT
-3.5 HNT NST NT
-4.5 HLV VET
-9.5 MART MIT'''

TZD = {}
for tz_descr in map(unicode.split, tz_str.split('\n')):
    tz_offset = int(float(tz_descr[0]) * 3600)
    for tz_code in tz_descr[1:]:
        TZD[tz_code] = tz_offset

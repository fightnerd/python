from __future__ import absolute_import

import urllib
from HTMLParser import HTMLParser

from django.core.urlresolvers import reverse


def reverse_with_query(view_name, query_params, do_escape=True, **kwargs):
    url = reverse(view_name, **kwargs)
    if query_params:
        url += '?'
        if do_escape:
            url += urllib.urlencode(query_params)
        else:
            url += HTMLParser().unescape(urllib.urlencode(query_params)).replace('%2F', '/')
    return url

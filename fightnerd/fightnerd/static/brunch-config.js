module.exports = {
    files: {
        javascripts: {
            joinTo: 'app.js'
        },
        stylesheets: {
            joinTo: 'app.css'
        }
    },
    paths: {
        public: 'dist'
    },
    plugins: {
        postcss: {
            processors: [
                require('autoprefixer')(['last 8 versions']),
                require('csswring')()
            ]
        }
    },
    // watcher: {
    //     usePolling: true
    // }

};
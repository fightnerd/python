from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from fightnerd.routers import Router
from fightnerd.fighters.api import FighterViewSet
from fightnerd.fighters.api import RankingViewSet
from fightnerd.fighters.api import EventViewSet
from fightnerd.notifications.api import AppleDeviceViewSet
from fightnerd.publications.api import ArticleViewSet
from fightnerd.publications.api import PublicationViewSet
from fightnerd.publications.api import VideoViewSet
from fightnerd.publications.api import ChannelViewSet
from django.views.decorators.cache import cache_page, never_cache
from fightnerd.views import IndexView


urls = list()

# Notifications API
notifications_router = Router(cache_timeout=3600)
notifications_router.register(r'apple_device', AppleDeviceViewSet, base_name='apple_device')
urls.append(
    url(r'^api/notifications/', include(notifications_router.urls, namespace='api_notifications'))
)

# Publications API
publications_router = Router(cache_timeout=3600)
publications_router.register(r'publication', PublicationViewSet, base_name='publication')
publications_router.register(r'article', ArticleViewSet, base_name='article')
publications_router.register(r'video', VideoViewSet, base_name='video')
publications_router.register(r'channel', ChannelViewSet, base_name='channel')
urls.append(
    url(r'^api/publications/', include(publications_router.urls, namespace='api_publications'))
)

# Fighters API
fighters_router = Router(cache_timeout=3600)
fighters_router.register(r'fighter', FighterViewSet, base_name='fighter')
fighters_router.register(r'ranking', RankingViewSet, base_name='ranking')
fighters_router.register(r'event', EventViewSet, base_name='event')
urls.append(
    url(r'^api/fighters/', include(fighters_router.urls, namespace='api_fighters')),
)

# Fighter
urls.append(
    url(r'^fighters/', include('fightnerd.fighters.urls', namespace='fighters')),
)

# Publication
urls.append(
    url(r'^publications/', include('fightnerd.publications.urls', namespace='publications')),
)


# Admin
urls.append(
    url(r'^cd72789d/', include(admin.site.urls)),
)
urls.append(
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
)

# Static stuff
if settings.DEBUG:
    urls.append(
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
    )

    urls.append(
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT})
    )

urls.append(
    url(r'^$', cache_page(60 * 60 * 24)(IndexView.as_view()), name='index'),
)

urlpatterns = patterns('', *urls)

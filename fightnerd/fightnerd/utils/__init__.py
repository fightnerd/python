import random
import string
from django.template import Context, Template


def get_lorem_text(count, method):
    context = Context()
    template_string = '{%' + ('lorem %d %s random' % (count, method)) + '%}'
    template = Template(template_string)
    return template.render(context)


def is_int(x):
    try:
        int(x)
        return True
    except TypeError:
        pass
    return False


def get_title(min_length, max_length):
    toks = get_lorem_text(max_length, 'w')[:random.randrange(min_length, max_length)].split()
    return ' '.join([x for x in toks]).title()


def get_ascii_text(length=10):
    ascii_characters = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
    return ''.join([random.choice(ascii_characters) for i in range(length)])
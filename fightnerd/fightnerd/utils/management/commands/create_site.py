from __future__ import absolute_import

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.sites.models import Site

SITE_DOMAIN = getattr(settings, 'SITE_DOMAIN')
SITE_NAME = getattr(settings, 'SITE_NAME')
SITE_ID = getattr(settings, 'SITE_ID')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def handle(self, *args, **options):
        site, is_created = Site.objects.update_or_create(id=SITE_ID, defaults={
            'domain': SITE_DOMAIN,
            'name': SITE_NAME
        })
        # site.domain = SITE_DOMAIN
        # site.name = SITE_NAME
        # site.save()

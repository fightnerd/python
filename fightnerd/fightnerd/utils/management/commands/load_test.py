from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import gevent.monkey
gevent.monkey.patch_all(socket=True, dns=True, time=True, select=True, thread=False, os=True, ssl=True, httplib=False, aggressive=False)
import requests
import logging
from django.core.management.base import BaseCommand
from rest_framework import status
from django.utils import timezone
import timeit
import random
import gevent
from gevent.pool import Pool

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('-p', dest='num_workers', action='store', type=int)
        parser.add_argument('-u', dest='start_urls', action='append')

    @staticmethod
    def process_url(url):
        while True:
            next_url = url
            while next_url:
                start_time = timezone.now()
                response = requests.get(next_url)
                delta = timezone.now() - start_time
                logging.info('%s\t%.2f ms\t%d', next_url, delta.microseconds / 1000, response.status_code)
                next_url = response.json().get('next', None)

    def handle(self, *args, **options):
        urls = options.get('start_urls')
        if len(urls) < int(options.get('num_workers')):
            limit = int(options.get('num_workers')) - len(urls)
            for i in range(limit):
                urls.append(random.choice(options.get('start_urls')))

        # Prepare a pool for 5 workers
        pool = Pool(options.get('num_workers'))

        [pool.spawn(self.process_url, url) for url in urls]
        pool.join()
        # for start_url in options.get('start_urls'):
        # site.domain = SITE_DOMAIN
        # site.name = SITE_NAME
        # site.save()

from __future__ import absolute_import

from django.core.management.base import BaseCommand
from django.core import cache


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def handle(self, *args, **options):
        cache.clear()

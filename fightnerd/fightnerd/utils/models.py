# -*- coding: utf-8 -*-

from django_enumfield import enum
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class MonthType(enum.Enum):
    JANUARY = 0
    FEBRUARY = 1
    MARCH = 2
    APRIL = 3
    MAY = 4
    JUNE = 5
    JULY = 6
    AUGUST = 7
    SEPTEMBER = 8
    OCTOBER = 9
    NOVEMBER = 10
    DECEMBER = 11

    labels = {
        JANUARY: 'January',
        FEBRUARY: 'February',
        MARCH: 'March',
        APRIL: 'April',
        MAY: 'May',
        JUNE: 'June',
        JULY: 'July',
        AUGUST: 'August',
        SEPTEMBER: 'September',
        OCTOBER: 'October',
        NOVEMBER: 'November',
        DECEMBER: 'December',
    }


class EscrowStatusType(enum.Enum):
    HOLD_PENDING = 0
    HELD = 1
    RELEASE_PENDING = 2
    RELEASED = 3
    REFUNDED = 4

    labels = {
        HOLD_PENDING: 'hold_pending',
        HELD: 'held',
        RELEASE_PENDING: 'release_pending',
        RELEASED: 'released',
        REFUNDED: 'refunded',
    }


class CurrencyType(enum.Enum):
    AED = 0
    ARS = 1
    AUD = 2
    BRL = 3
    CAD = 4
    CHF = 5
    CNY = 6
    CRC = 7
    CZK = 8
    DKK = 9
    EUR = 10
    GBP = 11
    HKD = 12
    HRK = 13
    HUF = 14
    IDR = 15
    ILS = 16
    INR = 17
    JPY = 18
    KRW = 19
    MAD = 20
    MXN = 21
    MYR = 22
    NOK = 23
    NZD = 24
    PEN = 25
    PHP = 26
    PLN = 27
    RON = 28
    RUB = 29
    SEK = 30
    SGD = 31
    THB = 32
    TRY = 33
    TWD = 34
    USD = 35
    VND = 36
    ZAR = 37

    symbols = {
        AED: u'د.إ',
        ARS: u'$',
        AUD: u'$',
        BRL: u'R$',
        CAD: u'$',
        CHF: u'CHF',
        CNY: u'¥',
        CRC: u'₡',
        CZK: u'Kč',
        DKK: u'kr',
        EUR: u'€',
        GBP: u'£',
        HKD: u'HK$',
        HRK: u'kn',
        HUF: u'Ft',
        IDR: u'Rp',
        ILS: u'₪',
        INR: u'₹',
        JPY: u'¥',
        KRW: u'₩',
        MAD: u'MAD',
        MXN: u'$',
        MYR: u'RM',
        NOK: u'kr',
        NZD: u'$',
        PEN: u'S/.',
        PHP: u'₱',
        PLN: u'zł',
        RON: u'lei',
        RUB: u'руб',
        SEK: u'kr',
        SGD: u'$',
        THB: u'฿',
        TRY: u'TRY',
        TWD: u'NT$',
        USD: u'$',
        VND: u'₫',
        ZAR: u'R'
    }


class UserActionType(enum.Enum):
    VIEW = 0
    SAVE = 1
    UNSAVE = 2
    LIKE = 3
    UNLIKE = 4


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_modified = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        abstract = True


class UserAction(BaseModel):
    type = enum.EnumField(UserActionType, blank=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

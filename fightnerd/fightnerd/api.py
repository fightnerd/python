# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

import operator
from itertools import chain

from drf_haystack import filters, serializers
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils import six


class HaystackFilter(filters.HaystackFilter):
    @staticmethod
    def build_filter(view, filters=None):
        """
        Creates a single SQ filter from querystring parameters that
        correspond to the SearchIndex fields that have been "registered"
        in `view.fields`.

        Default behavior is to `OR` terms for the same parameters, and `AND`
        between parameters.

        Any querystring parameters that are not registered in
        `view.fields` will be ignored.
        """

        terms = []
        exclude_terms = []

        if filters is None:
            filters = {}  # pragma: no cover

        for param, value in filters.items():
            # Skip if the parameter is not listed in the serializer's `fields`
            # or if it's in the `exclude` list.
            if param == 'q':
                param = 'q__startswith'
            excluding_term = False
            param_parts = param.split("__")
            base_param = param_parts[0]  # only test against field without lookup
            negation_keyword = getattr(settings, "DRF_HAYSTACK_NEGATION_KEYWORD", "not")
            if len(param_parts) > 1 and param_parts[1] == negation_keyword:
                excluding_term = True
                param = param.replace("__%s" % negation_keyword, "")  # haystack wouldn't understand our negation

            serializer_class = view.get_serializer_class()
            if serializer_class:
                try:
                    if hasattr(serializer_class.Meta, "field_aliases"):
                        old_base = base_param
                        base_param = serializer_class.Meta.field_aliases.get(base_param, base_param)
                        param = param.replace(old_base, base_param)  # need to replace the alias

                    fields = getattr(serializer_class.Meta, "fields", [])
                    exclude = getattr(serializer_class.Meta, "exclude", [])
                    search_fields = getattr(serializer_class.Meta, "search_fields", [])

                    if ((fields or search_fields) and base_param not in chain(fields, search_fields)) or base_param in exclude or not value:
                        continue

                except AttributeError:
                    raise ImproperlyConfigured("%s must implement a Meta class." %
                                               serializer_class.__class__.__name__)

            tokens = [token.strip() for token in value.split(view.lookup_sep)]
            field_queries = []

            for token in tokens:
                if token:
                    field_queries.append(view.query_object((param, token)))

            term = six.moves.reduce(operator.or_, filter(lambda x: x, field_queries))
            if excluding_term:
                exclude_terms.append(term)
            else:
                terms.append(term)

        terms = six.moves.reduce(operator.and_, filter(lambda x: x, terms)) if terms else []
        exclude_terms = six.moves.reduce(operator.and_, filter(lambda x: x, exclude_terms)) if exclude_terms else []
        return terms, exclude_terms


class HaystackSerializer(serializers.HaystackSerializer):
    def get_fields(self):
        """
        Get the required fields for serializing the result.
        """
        field_mapping = super(HaystackSerializer, self).get_fields()
        ignore_fields = getattr(self.Meta, "ignore_fields", [])

        field_names = list(field_mapping.keys())
        for field_name in field_names:
            if field_name in ignore_fields:
                field_mapping.pop(field_name)

        return field_mapping
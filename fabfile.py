from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from time import sleep
import json
import sys
import time
import os
import imp
from fabric.api import settings, env, cd, shell_env, lcd, task
from fabric.api import local
from fabric.operations import prompt
from fabric.contrib.project import rsync_project
from fabric.api import put
from fabric.api import run
from fabric.api import sudo
from fabric.utils import puts
import getpass
from datetime import datetime
import pickle
from fabric.contrib import django
import boto
from boto import cloudfront
from fabric.decorators import roles
import requests
import re
from shutil import move
import digitalocean
from gevent.pool import Pool


reload(sys)
sys.setdefaultencoding('utf8')

PROJECT_ROOT = os.path.dirname(__file__)

if env.is_production in ['true', 'True']:
    from deploy.fab.production import env as production_env
    env.update(production_env)
else:
    from deploy.fab.development import env as development_env
    env.update(development_env)


# from fabric.api import put as fabric_put
# from fabric.api import run as fabric_run
# from fabric.api import sudo as fabric_sudo


# def rsync_project(host, user, key_filename, **kwargs):
#     with settings(host_string=host, user=user, key_filename=key_filename):
#         fabric_rsync_project(**kwargs)
#
#
# def put(host, user, key_filename, **kwargs):
#     with settings(host_string=host, user=user, key_filename=key_filename):
#         output = fabric_put(**kwargs)
#     return output
#
#
# def run(host, user, key_filename, **kwargs):
#     with settings(host_string=host, user=user, key_filename=key_filename):
#         output = fabric_run(**kwargs)
#     return output
#
#
# def sudo(host, user, key_filename, **kwargs):
#     with settings(host_string=host, user=user, key_filename=key_filename):
#         output = fabric_sudo(**kwargs)
#     return output


def clean_docker():
    # Cleanup
    with shell_env(**env.docker_kwargs):
        with settings(warn_only=True):
            sudo('docker ps -a | grep Exited | grep run | awk \'{}{print $1}\' | xargs docker rm')
            sudo('docker images | awk \'{}{print $3}\' | xargs docker rmi')


def get_droplet_by_name(name):
    manager = digitalocean.Manager(token=env.digital_ocean_api_token)
    droplets = manager.get_all_droplets()
    for droplet in droplets:
        if droplet.name == name:
            return droplet
    return None


@task
def remove_droplet(name):
    droplet = get_droplet_by_name(name)
    if not droplet:
        raise Exception('Droplet not found!')
    if prompt('Are you sure you want to destroy this droplet [%s]?' % droplet.name, default='No', validate=validate_polar_question):
        droplet.destroy()


@task
def create_droplet(name, size_slug='512mb'):
    manager = digitalocean.Manager(token=env.digital_ocean_api_token)
    ssh_keys = manager.get_all_sshkeys()

    if get_droplet_by_name(name):
        raise Exception('A droplet with this name already exists!')

    droplet = digitalocean.Droplet(
        token=env.digital_ocean_api_token,
        name=name,
        region='nyc2',
        image='ubuntu-15-10-x64',
        size_slug=size_slug,
        backups=False,
        ssh_keys=ssh_keys
    )
    droplet.create()

    actions = droplet.get_actions()
    for action in actions:
        action.load()
        print(action.status)


@task
def provision_droplet(name):
    droplet = get_droplet_by_name(name)
    env.host_string = droplet.ip_address
    env.user = 'root'

    run('adduser %s' % env.app_name)

    # Update stuff and install docker
    run('apt-get update')
    run('apt-get install -y --force-yes htop')
    run('apt-get install -y --force-yes python-pip')
    run('apt-get install -y --force-yes curl')
    run('apt-get install -y --force-yes fail2ban')
    run('curl -sSL https://get.docker.com/ | sh')
    run('pip install -U docker-compose')
    run('mkdir -p %s' % env.app_root)

    # Create user
    run('adduser %s sudo' % env.app_name)
    run('usermod -aG docker %s' % env.app_name)
    run('chown %s %s' % (env.app_name, env.app_root))
    run('find %s | xargs chown %s' % (env.app_root, env.app_name))

    # Firewall
    run('ufw allow ssh')
    run('ufw allow 80/tcp')
    run('ufw allow 443/tcp')
    run('ufw --force enable')

    # Change docker storage driver
    run('systemctl stop docker')
    run('rm /var/lib/docker -rf')
    run('mkdir -p /etc/systemd/system/docker.service.d')
    put(local_path=os.path.join(PROJECT_ROOT, 'deploy/systemd/aufs.production.conf'), remote_path='/etc/systemd/system/docker.service.d/aufs.conf')
    run('systemctl daemon-reload')
    run('systemctl start docker')

    # Create fightnerd service
    put(local_path=os.path.join(PROJECT_ROOT, 'deploy/systemd/fightnerd.production.service'), remote_path='/etc/systemd/system/fightnerd.service')
    run('systemctl daemon-reload')

    # Copy ssh key
    with open(env.public_key_filename) as key_file:
        home_directory = '/home/%s' % env.app_name
        ssh_directory = os.path.join(home_directory, '.ssh')
        run('mkdir -p %s' % ssh_directory)
        run('echo "%s" > %s' % (key_file.readlines()[0], os.path.join(ssh_directory, 'authorized_keys')))
        run('chown %s %s' % (env.app_name, home_directory))
        run('find %s | xargs chown %s' % (home_directory, env.app_name))

    # Add docker login credentials
    run('mkdir -p /home/%s/.docker/' % env.app_name)
    put(remote_path='/home/%s/.docker/config.json' % env.app_name, local_path=env.docker_auth_filename)

    # Disable root login
    run('sed -i.bak "s/^PermitRootLogin.*/PermitRootLogin no/g" /etc/ssh/sshd_config')
    run('systemctl restart ssh')


@task()
def deploy_droplet(name, branch='master', do_build_images=True, do_build_statics=True, do_backup_database=True):
    do_build_images = do_build_images in ['True', 'true', True]
    do_build_statics = do_build_statics in ['True', 'true', True]
    do_backup_database = do_backup_database in ['True', 'true', True]

    droplet = get_droplet_by_name(name)
    env.host_string = droplet.ip_address
    print('user: %s' % env.user)
    env.password = getpass.getpass('password:')

    # Backup database
    if do_backup_database:
        with cd(env.app_root):
            with settings(docker_kwargs=env.remote_docker_kwargs):
                # docker_compose('run worker ./manage.py backup_json')
                docker_compose('run worker ./manage.py backup_postgres')

    # Do ubild statics
    if do_build_statics:
        build_statics()

    # Rsync to vagrant host
    with settings(host_string=env.vagrant_host, user=env.vagrant_user, key_filename=env.vagrant_key_filename, docker_kwargs=env.local_docker_kwargs):
        # Rsync to vagrant host
        deploy_repository(branch=branch)

    # Build images on Vagrant
    with settings(host_string=env.vagrant_host, user=env.vagrant_user, key_filename=env.vagrant_key_filename, docker_kwargs=env.local_docker_kwargs):
        if do_build_images:
            build_images()

    # Update docker compose on DO
    put(local_path=env.docker_compose_file_path, remote_path=os.path.join(env.app_root, 'docker-compose.yml'))

    # Create database in DO
    # if True:
    #     with settings(docker_kwargs=env.remote_docker_kwargs):
    #         create_database()

    # Pull new images
    with cd(env.app_root):
        with settings(docker_kwargs=env.remote_docker_kwargs):
            docker_compose(command='pull')

    # Restart daemons
    put(use_sudo=True, local_path=os.path.join(PROJECT_ROOT, 'deploy/systemd/fightnerd.production.service'), remote_path='/etc/systemd/system/fightnerd.service')
    sudo(command='systemctl daemon-reload')
    sudo(command='systemctl restart %s' % env.app_name)
    sudo(command='systemctl enable %s' % env.app_name)
    sudo(command='systemctl status %s' % env.app_name)

    with settings(docker_kwargs=env.remote_docker_kwargs):
        with cd(env.app_root):
            docker_compose('scale worker=2')
            docker_compose('ps')

    with settings(docker_kwargs=env.remote_docker_kwargs):
        clean_docker()


def validate_polar_question(answer):
    if answer.lower() == 'yes':
        return True
    elif answer.lower() == 'no':
        return False
    else:
        raise Exception("Answer must be 'Yes' or 'No'")


def str_to_bool(text):
    if isinstance(text, basestring):
        if text.lower() in ['true', '1']:
            return True
        elif text.lower() in ['false', '0']:
            return False
        else:
            raise ValueError("Unable to convert \'%s\' to a bool." % text)
    else:
        return text


def add_escape_characters(text):
    return text.replace('/', '\/')


def test(condition):
    return str_to_bool(run("%s && echo true || echo false" % condition))


def docker(command, do_capture=False):
    output = None
    with shell_env(**env.docker_kwargs):
        output = run(command='docker %s' % (command,))
    return output


def docker_compose(command):
    output = None
    with shell_env(**env.docker_kwargs):
        output = run(command='docker-compose -p %s %s' % (env.app_name, command))
    return output


def download_geoip_databases():
    geoip_dir = os.path.join(PROJECT_ROOT, 'deploy/docker/python/geoip/')
    last_download_time_path = os.path.join(geoip_dir, '.last_download_time')
    if os.path.exists(last_download_time_path):
        with open(last_download_time_path, 'rb') as last_download_time_file:
            last_download_time = pickle.load(last_download_time_file)
            now_time = datetime.utcnow()
            time_delta = now_time - last_download_time
            if time_delta.days < 30:
                puts('Skipping GeoIP download. Last download was %d days ago.' % time_delta.days)
                return

    with lcd(geoip_dir):
        local('rm -rfv *.gz *.dat')
        local('wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz')
        local('wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz')
        local('wget http://geolite.maxmind.com/download/geoip/database/GeoIPv6.dat.gz')
        local('gunzip -f *.gz')
    with open(last_download_time_path, 'w') as last_download_time_file:
        pickle.dump(datetime.utcnow(), last_download_time_file)


def deploy_repository(branch):
    with settings(warn_only=False):
        sudo('mkdir -p %s' % env.app_root)
        sudo('chown %s %s' % (env.user, env.app_root))

    # Rsync repository
    ignore_patterns = [
        r'build',
        r'bin',
        r'include',
        r'python2.7',
        r'logs$',
        r'media',
        r'opt',
        r'tmp',
        r'.DS_Store',
        r'*.sqlite3',
        r'node_modules',
        r'whoosh_index',
        r'.idea',
        r'.sass-cache'
        # r'fightnerd-web/static',
    ]
    rsync_project(remote_dir=env.app_root, local_dir='./', exclude=ignore_patterns, delete=True)

    # Switch the branch
    with cd(env.app_root):
        run(command='git checkout %s' % branch)


@task
def provision():
    # Add docker login credentials
    run('mkdir -p /home/%s/.docker/' % env.user)
    put(remote_path='/home/%s/.docker/config.json' % env.user, local_path=env.docker_auth_filename)


@task
def deploy_vagrant(branch='develop', do_create_database=False, do_build_statics=False, do_build_images=True):
    puts('is_production = %s' % env.is_production)

    # Is this a full rebuild?
    do_create_database = do_create_database in ['True', 'true', True]
    do_build_images = do_build_images in ['True', 'true', True]

    # Get credentials
    do_prompt_password = 'password' not in env or not env.password
    env.user = prompt('user:') if do_prompt_password else env.user
    env.password = getpass.getpass('password:') if do_prompt_password else env.password

    # # Handle statics
    # do_build_statics = do_build_statics in ['True', 'true']
    # if do_build_statics:
    #     build_statics()

    # Download GeoIP
    download_geoip_databases()

    # Upload repository
    deploy_repository(branch=branch)

    # Build images
    if do_build_images:
        build_images()

    # Copy over docker-compose.yml
    put(local_path=env.docker_compose_file_path, remote_path=os.path.join(env.app_root, 'docker-compose.yml'))

    if do_create_database:
        create_database()

    # Restart the server
    put(use_sudo=True, local_path=os.path.join(PROJECT_ROOT, 'deploy/systemd/fightnerd.development.service'), remote_path='/etc/systemd/system/fightnerd.service')
    sudo(command='systemctl daemon-reload')
    sudo(command='systemctl enable %s' % env.app_name)
    sudo(command='systemctl restart %s' % env.app_name)

    with cd(env.app_root):
        docker_compose(command='ps')

    clean_docker()


def create_database():
    with settings(warn_only=True):
        sudo(command='systemctl stop %s' % env.app_name)
    with cd(env.app_root):
        with settings(warn_only=True):
            docker_compose(command='rm -f')
        sudo('rm -rfv /var/lib/postgresql/data')

        # Bring up postgres
        docker_compose(command='up -d postgres')

        # Initialize database
        puts('Waiting for database to start...')
        sleep(30)
        docker_compose(command='run postgres psql -c \"CREATE DATABASE %s;\" -h postgres -U %s' % (env.postgres_db_name, env.postgres_login))
        docker_compose(command='run postgres psql -c \"CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;\" -h postgres -U %s' % (env.postgres_login,))
        docker_compose(command='run postgres psql -c \"ALTER USER %s WITH ENCRYPTED PASSWORD \'%s\';\" -h postgres -U %s' % (env.postgres_login, env.postgres_password, env.postgres_login))

        # The big sleep
        sleep(10)


@task
def build_images():
    service_names = sorted(env.services.keys(), key=lambda x: 0 if x == 'python' else 1)
    print(service_names)
    args = list()
    for service_name in service_names:
        service = env.services.get(service_name)
        dockerfile_path = service.get('docker').get('dockerfile_path')
        build_context_path = service.get('docker').get('build_context_path')
        registry_image_name = service.get('docker').get('registry_image_name')
        build_image(build_context_path, dockerfile_path, registry_image_name)


def build_image(build_context_path, dockerfile_path, registry_image_name):
    output = unicode(docker(command='build -f %s %s' % (dockerfile_path, build_context_path), do_capture=True))
    image_id = output.split('\n')[-1].split('Successfully built ')[1]
    docker(command='tag -f %s %s' % (image_id, registry_image_name))
    docker(command='-D push %s' % registry_image_name)


def build_image_star(args):
    return build_image(*args)


@task
def build_statics():
    # # Delete statics
    # local('rm -rf %s/%s/static' % (PROJECT_PATH, APP_NAME))
    commands = list()

    # Run Gulp
    commands.append('gulp dist --httpPath /static')

    # Collect statics
    commands.append('%s %s collectstatic --settings %s --noinput -i *.less -i *.scss -i *.sass -i *.map -i *.md' % ('python', './manage.py', env.deployment_settings_module))

    # Compress statics
    commands.append('%s %s compress --force --settings %s' % ('python', './manage.py', env.deployment_settings_module))

    with settings(host_string=env.vagrant_host, user=env.vagrant_user, key_filename=env.vagrant_key_filename, docker_kwargs=env.local_docker_kwargs):
        put(local_path=os.path.join(PROJECT_ROOT, 'deploy/docker/compose/development.yml'), remote_path=os.path.join(env.app_root, 'docker-compose.yml'))
        with cd(env.app_root):
            docker_compose('run gulp /bin/sh -c \'%s\'' % ' && '.join(commands))

    # Invalidate CloudFront
    cloudfront_connection = cloudfront.CloudFrontConnection(env.aws_access_key_id, env.aws_secret_access_key)
    cloudfront_connection.create_invalidation_request(env.aws_cloudfront_distribution_id, ['/static/'])

---
# Install Python 2 on Droplet
- hosts: '{{ hosts }}'
  gather_facts: no
  tasks:
    - name: Install Python2
      raw: sudo apt-get -y install python-simplejson

# Install app
- hosts: '{{ hosts }}'
  vars:
    working_directory: "{{ lookup('env','CI_PROJECT_DIR') }}"
    version: "{{ lookup('env','VERSION') }}"
    staticfiles_version: "{{ lookup('env','STATICFILES_VERSION') }}"
  pre_tasks:
    - include_vars: './vault.yml'
  roles:
    - common
    - { role: docker, user: 'root'}
  post_tasks:
    - name: Add user
      user:
        name: "{{ user.username }}"
        password: "{{ user.password }}"
        groups: "sudo,docker"
    - name: Setup firewall
      ufw:
        rule: allow
        port: "{{ item }}"
        proto: tcp
        state: enabled
        direction: in
      with_items:
        - ssh
        - 80
        - 443
    - name: Prevent root from logging in with a password
      notify:
        - "restart sshd service"
      lineinfile:
        dest: /etc/ssh/sshd_config
        regexp: ^PermitRootLogin
        state: present
        line: PermitRootLogin without-password
    - name: Log into private registry and force re-authorization
      docker_login:
        registry: https://registry.gitlab.com
        username: "{{ registry.username }}"
        password: "{{ registry.password }}"
        reauthorize: yes
    - name: Create app directory
      file:
        path: "{{ app_directory }}"
        state: directory
        owner: "{{ ansible_user }}"
    - name: Install compose files
      notify:
        - "restart fightnerd service"
      synchronize:
        src: "{{ working_directory }}/deploy/compose/"
        dest: "{{ app_directory }}/"
    - name: Update environment file
      template:
        src: "{{ working_directory }}/deploy/compose/env/common.env.j2"
        dest: "{{ app_directory }}/.env"
      notify:
        - "restart fightnerd service"
    - name: Pull images
      shell: docker-compose -p {{ app_name }}
        -f {{ app_directory }}/common.yml
        -f {{ app_directory }}/production.yml
        pull
      args:
        chdir: "{{ app_directory }}"
    - name: Install newest version of fightnerd service
      template:
        src: "{{ working_directory }}/deploy/systemd/{{ app_name }}.service.j2"
        dest: /etc/systemd/system/{{ app_name }}-{{ version }}.service
      notify:
        - "reload systemctl daemon"
        - "restart fightnerd service"
    - name: Link fightnerd service
      file:
        src: /etc/systemd/system/{{ app_name }}-{{ version }}.service
        dest: /etc/systemd/system/{{ app_name }}.service
        state: link
        force: yes
      notify:
        - "reload systemctl daemon"
        - "restart fightnerd service"
  handlers:
    - name: Restart SSH
      listen: "restart sshd service"
      service:
        name: "sshd"
        state: restarted
        enabled: yes
    - name: Reload systemctl daemon
      shell: systemctl daemon-reload
      listen: "reload systemctl daemon"
    - name: Restart docker
      service:
        name: "docker"
        state: restarted
        enabled: yes
      listen: "restart docker service"
    - name: Restart fightnerd
      service:
        name: "{{ app_name }}"
        state: restarted
        enabled: yes
      listen: "restart fightnerd service"

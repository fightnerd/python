from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import os
import re
import socket

from jinja2 import Template

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser(description='Create HAProxy cfg')
    argument_parser.add_argument('template_file_path', action='store')
    argument_parser.add_argument('output_file_path', action='store')

    args = argument_parser.parse_args()

    web_nodes = list()
    for i in range(20):
        hostname = 'web_%d' % i
        try:
            socket.gethostbyname(hostname)
            web_nodes.append({
                'name': 'web%d' % i,
                'hostname': hostname,
                'port': 80,
            })
        except socket.error:
            continue

    # Render
    with open(args.template_file_path) as template_file:
        template = Template(''.join(template_file.readlines()))
        with open(args.output_file_path, 'w') as output_file:
            output_file.writelines(template.render(web_nodes=web_nodes))

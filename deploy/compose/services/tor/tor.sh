#!/bin/bash

# Configure Tor
echo "Waiting for IP...?"
sleep 5
echo "Tor password is $TOR_PASSWORD"
hashed_control_password=`tor --hash-password $TOR_PASSWORD | tail -n 1`
ip_address=`/sbin/ip route | awk '/link/ { print $9 }'`
echo "Tor password is $hashed_control_password"

echo "ControlPort 9051" >> /etc/tor/torrc
echo "ControlListenAddress $ip_address" >> /etc/tor/torrc
echo "HashedControlPassword $hashed_control_password" >> /etc/tor/torrc
echo "forward-socks5 / localhost:9050 ." >> /etc/privoxy/config
echo "listen-address $ip_address:8118" >> /etc/privoxy/config
echo "use_proxy=yes" >> /root/.wgetrc
echo "http_proxy=$ip_address:8118" >> /root/.wgetrc
#/etc/init.d/tor restart
#/etc/init.d/privoxy restart
#/bin/true
privoxy --no-daemon /etc/privoxy/config &
tor
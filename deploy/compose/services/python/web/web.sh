#!/bin/bash
mkdir -p /var/log/supervisor/
mkdir -p /var/log/gunicorn/
mkdir -p /var/log/nginx/
mkdir -p /var/run/gunicorn/

rm -rfv /etc/nginx/sites-enabled/*
cp -rfv $NGINX_CONFIG_FILE_PATH /etc/nginx/sites-enabled/thecageapp.com.conf

supervisord -n
bind = 'unix:///var/run/gunicorn/fightnerd.sock'
workers = 1
max_requests = 1000
max_requests_jitter = 25
accesslog = '/var/log/gunicorn/access.log'
errorlog = '/var/log/gunicorn/error.log'

reload = False

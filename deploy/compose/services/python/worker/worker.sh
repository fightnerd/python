#!/bin/bash
mkdir -p /var/log/celery/
touch /var/log/celery/worker.log
celery worker -A fightnerd -c 1 -l info

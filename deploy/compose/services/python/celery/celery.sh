#!/bin/bash
mkdir -p /var/log/celery/
touch /var/log/celery/beat.log
celery -A fightnerd --logfile=/var/log/celery/beat.log -l info beat
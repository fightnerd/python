// Include gulp
var gulp = require('gulp');
// Include plugins
var compass = require('gulp-compass');
var argv = require('yargs').argv;
var runSequence = require('run-sequence');
var path = require('path');
var src = path.join(__dirname, 'fightnerd/fightnerd/');
var dest = path.join(__dirname, 'fightnerd/fightnerd/static/');
var localStaticURL = '/static';
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');

gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch(src + '*/static/*/sass/*/*.scss', function(){
        runSequence('sass');
    });
    gulp.watch(src + 'static/sass/*/*.scss', function(){
        runSequence('sass');
    });
});

//function registerCompassTask(inputDirectoryPath, appName) {
//    var taskName = 'compass__' + inputDirectoryPath;
//    gulp.task(taskName, function() {
//        exec('rm -rfv ' + inputDirectoryPath + 'stylesheets/*.css');
//        //console.log('rm -rfv ' + inputDirectoryPath + 'stylesheets/**/*.css');
//        var httpPath = '';
//        if (typeof argv.httpPath != "undefined") {
//            httpPath = argv.httpPath + '/' + appName;
//        } else {
//            httpPath = localStaticURL + '/' + appName;
//        }
//        exec('compass compile --import-path "' + '/Users/chukwuemeka/Documents/Projects/python/jawa-web/jawa/jawa/static/sass' + '" ' + '--http-path "' + httpPath + '" ' + inputDirectoryPath)
//    });
//    return taskName
//}

function registerCompassTask(projectPath, appName) {
    var taskName = 'compass__' + projectPath;
    gulp.task(taskName, function() {
        var httpPath = '';
        if (typeof argv.httpPath != "undefined") {
            httpPath = argv.httpPath + '/' + appName;
        } else {
            httpPath = localStaticURL + '/' + appName;
        }
        gulp.src(projectPath + '*.scss')
            .pipe(plumber({
                errorHandler: function (error) {
                    gutil.log(error);
                    gutil.log(error.message);
                    this.emit('end');
                }}))
            .pipe(compass({
                project: projectPath,
                //config_file: path.join(projectPath, 'config.rb'),
                css: 'css',
                sass: 'sass',
                http_path: httpPath
            }))
    }).on('error', function(error) {
      //  gutil.log("WATH!!!");
      //gutil.log(error);
      this.emit('end');
    });
    return taskName;
}

gulp.task('sass', [
    registerCompassTask(src + 'static/', '')
]);

var isRunning = false;
gulp.task('collectstatic', function(){
    function callback(error, stdout, stderr) {
        console.log(stdout);
        isRunning = false;
    }
    if (isRunning) {
        return;
    }
    isRunning = true;
    exec('/Users/chukwuemeka/Documents/Projects/python/jawa-web/bin/python /Users/chukwuemeka/Documents/Projects/python/jawa-web/jawa/manage.py collectstatic --settings jawa.settings.default --noinput',
        {
            'PYTHONPATH': '/Users/chukwuemeka/Documents/Projects/python/jawa-web/jawa/'
        }, callback);
});

// Default Task
gulp.task('default', function() {
        runSequence(['watch', 'sass']);
    }
);

// For deployment
gulp.task('dist', ['sass']);
